#!/bin/bash

set -Eeuo pipefail

if [ "$#" != "1" ]; then
	echo "Usage: $0 <mht file>"
	exit 1
fi

MHTFILE="$(readlink -f "${1}")"
MHTBASENAME="$(basename "${MHTFILE}" | sed 's,\.mht$,,g')"

mkdir -p "${MHTBASENAME}"
pushd "${MHTBASENAME}"

rm -f tmpfile ; touch tmpfile

MAX="$(cat "${MHTFILE}"  | grep '^Content-Location' | wc -l)"
COUNTER=0

CURLOC=related
CURISENC=f
MODE=0
cat "${MHTFILE}" <(echo -ne "\n") | tr -d '\r' | while read line ; do

	if echo "${line}" | grep -q  '^--=_NextPart_SMP' ; then
		#echo "FOUND BOUNDARY: ${line}"

		if [ "${CURISENC}" = "t" ]; then
			# base64 encoded file
			cat tmpfile  | base64 -d > "${CURLOC}"
		else
			mv tmpfile "${CURLOC}"
		fi

		rm -f tmpfile ; touch tmpfile

		MODE=1
		if echo "${line}" | grep -q -- '--$' ; then
			echo "END OF FILE REACHED"
			exit 0
		fi
	elif [ "x${line}y" = "xy" ] && [ "$MODE" = "1" ]; then
		MODE=2
		#echo "Found beginning of file."
		#echo "---"
		#cat tmpfile
		#echo "/---"
		CURLOC="$(cat tmpfile | grep '^Content-Location:' | cut -d ':' -f 2 | sed 's,^ ,,')"
		COUNTER=$(( $COUNTER + 1 ))
		echo "(${COUNTER}/${MAX}) FILE: '${CURLOC}'"
		if cat tmpfile | grep -q '^Content-Transfer-Encoding: base64' ; then
			CURISENC=t
		else
			CURISENC=f
		fi
		rm -f tmpfile ; touch tmpfile
	else
		#echo "line: 'x${line}y'"
		echo "${line}" >>  tmpfile
	fi

done

popd
