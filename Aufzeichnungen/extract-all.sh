#!/bin/bash

set -e

echo ""
echo ""
echo ""
date
echo ""
echo ""
echo ""

./extract.sh Recording_20221031_2253_versuch1.mht
./extract.sh Recording_20221031_2306_Virtualbox_installieren.mht
./extract.sh Recording_20221101_0124_Lifi_installieren.mht
./extract.sh Recording_20221101_0127_VT-x_im_Bios_deaktiviert.mht
./extract.sh Recording_20221101_0140_Guest_Additions_installieren.mht
./extract.sh Recording_20221101_0142_3D_Beschleunigung.mht

echo ""
echo ""
echo ""
echo "[$(date)] Done."
echo ""
echo ""
echo ""
