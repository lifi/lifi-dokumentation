# FAQ

[TOC]

## Muss ich Lifi verwenden um das Studium zu bestehen?

Nein.

Es ist möglich dein Informatik Studium komplett ohne Lifi aufm Laptop/Rechner zu bestehen.
Du brauchst sogar theoretisch nicht mal einen Laptop/Rechner.
Alles was du für dein Studium tun musst, kannst du ohne Probleme auch auf den [CIP-Pool Rechnern im Ifi](https://fg.informatik.uni-goettingen.de/de/raeume/cip.html) tun. Dafür sind die schließlich mitunter da.

Selbstverständlich macht aber ein Laptop Sachen einfacher, wenn man z.B. nicht andauernd zum Ifi fahren möchte/kann.

Lifi macht Sachen dann einfacher, wenn man z.B. Windows auf dem Laptop hat. (Einige Veranstaltungen gehen davon aus, dass man Linux verwendet. Lifi ist ein Linux.)

## Was sind die Systemanforderungen für Lifi?

- Freier Festplattenspeicher: min. 15GB (empfohlen 20GB)
- RAM: min. 4GB
- CPU: (halbwegs) moderne x86 Intel oder AMD CPU. Mindestens zwei Kerne.
- OS: Ein [von VirtualBox unterstütztes Betriebssystem](https://www.virtualbox.org/manual/UserManual.html#hostossupport).

## Wie verbinde ich Lifi mit den CIP-Pools?

Siehe '[Mit CIP-Pool Verbinden](cipconnect.md)'.

## Was für ein Linux ist Lifi?

Lifi ist einfach nur ein [Ubuntu 22.04.1 LTS](https://www.releases.ubuntu.com/22.04/) mit ein paar Veränderungen, vorinstalliert in einer VM.

- Es sind einige Sachen umkonfiguriert.
- Es ist viel fürs Informatik Studium an nützlicher Software vorinstalliert.
- Es sind einige unnötige Pakete von Ubuntu deinstalliert.
- Es sind Sachen für VMs optimiert.

(Für die technisch Interessierten: Alle Änderungen sind über [diese Ansible Role](https://gitlab.gwdg.de/lifi/ansible-role-lifi) definiert.)

## Muss ich Lifi bei einem neuen Release neuinstallieren?

Nein.

Lifi hat einen [integrierten Updater](update.md), wodurch Lifi automatisch auf dem aktuellesten Stand gehalten wird.

Die einzigen Sachen die der integrierte Updater nicht automatisch aktualisiert, sind Sachen wie z.B. Firefox Bookmarks. Also nichts wofür es sich lohnen würde Lifi komplett neu zu installieren.

## Welche Texteditoren gibt es in Lifi?

> Das Wichtigste beim Programmieren ist ein guter Texteditor.<br>
> ~ Alle Programmierer

Zum Glück hat Lifi eine gute Auswahl an Texteditoren vorinstalliert:

- [VSCodium](https://vscodium.com/) (empfohlen)
- [Emacs](https://www.gnu.org/software/emacs/)
- [GEdit](https://de.wikipedia.org/wiki/Gedit)
- [Nano](https://www.nano-editor.org/)
- [NeoVim](https://neovim.io/)
- [Vim](https://www.vim.org/)
- [Ed](https://de.wikipedia.org/wiki/Ed_(Texteditor))

## Wie installiere ich Software in Lifi?

Bei Lifi fehlt leider das 'Ubuntu Software Center' womit man üblicherweise in Ubuntu neue Software installieren würde.

Es gibt aber dennoch Wege Software zu installieren.

**Möglichkeit 1: Issue**

Erstelle ein Issue im [Lifi Issue Tracker](https://gitlab.gwdg.de/lifi/lifi-bugs/-/issues) wo du fragst, dass die bestimmte Software installiert werden soll.

Sobald dein Issue angenommen wird, kannst du einfach Lifi Aktualisiern und schon sollte die Software installiert sein.

Diese Möglichkeit ist besonders sinnvoll wenn du glaubst, dass diese Software grundsätzlich auch für andere Studis nützlich sein könnte.

**Möglichkeit 2: Manuell**

Lifi ist intern immer noch ein Ubuntu.

Du kannst also im Terminal mit
```bash
apt search <Paketname>
```
nach einem bestimmten Paket suchen und dieses dann mit
```bash
sudo apt install <Paketname>
```
installieren.

!!! warning
    Es gibt ein paar Pakete die beim Aktualisieren von Lifi automatisch deinstalliert werden, falls vorhanden.
    Solltest du auf dieses Problem stoßen, bitte ein [Issue erstellen](https://gitlab.gwdg.de/lifi/lifi-bugs/-/issues).


## Wie aktualisiere ich Lifi?

Siehe '[Lifi Aktualisieren](update.md)'.

## Warum sind da Kühe beim Aktualisieren?

```
 ______________
< Warum nicht? >
 --------------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||

```
(Der Lifi Updater verwendet intern [Ansible](https://www.ansible.com/) und Ansible hat standardmäßig Kühe.)

## Wie wird "Lifi" ausgesprochen?

Lifi wird wie das englische Wort "leafy" ausgesprochen.

## Was sind die Nutzerdaten für den "lifi" Account?

- Nutzername: `lifi`
- Passwort: `lifi`

## Wo werden die Dateien, die ich in Lifi erstelle, gespeichert?

Bei der Installation hattest du einen Ordner ausgesucht, wo du die Lifi.zip Datei hin extrahiert hast.

Dieser Ordner sollte (mindestens) zwei Dateien beinhalten.
Eine mit blauem Icon (diese hatten wir fürs Installieren verwendet) und eine mit orangenem Icon.

Die orangene Datei ist eine "Virtual Disk Image" Datei (*.vdi*). Das ist quasi eine virtuelle Festplatte.
Dort ist Lifi drin gespeichert und dort sind entsprechend auch alle Dateien, die du in Lifi erstellt hast, gespeichert.

Es ist nicht (einfach) möglich diese Datei ohne VirtualBox zu öffnen.

!!! danger "Nicht verschieben!"
    Auf keinen Fall sollte diese *.vdi* Datei verschoben werden!
    VirtualBox erwartet, dass die Datei immer am gleichen Ort bleibt.
    Falls die Datei verschoben wird, kann VirtualBox Lifi nicht mehr starten.

![Lifi Ordner](img/screenshots/install_lifi_linux.png)

## Warum ist Quartz bei mir nicht installiert?

Wenn Quartz noch nicht bei dir in Lifi installiert ist, musst du einfach nur [Lifi aktualisieren](update.md).  
Quartz wird dann automatisch installiert.


