---
lang: de
---

# Quartz verwenden

Für die Technische Informatik Vorlesung ist in Lifi auch [Quartz](https://lifi.pages.gwdg.de/quartz/) installiert.

Weitere [Informationen zum Verwenden von Quartz gibt es in der Quartz Dokumentation](https://lifi.pages.gwdg.de/quartz/usage/).

!!! warning "Command 'quartz' not found"
    ![Fehlermeldung](./img/screenshots/quartz_not_found.png)  
    Wenn man diese Fehlermeldung bekommt, muss man einfach nur **[Lifi aktualisieren](update.md)**.  
    Quartz wird dann automatisch installiert.


