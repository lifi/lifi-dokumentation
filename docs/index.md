# Linux fürs Institut für Informatik (Lifi)

Lifi ist eine Virtuelle Maschine (VM) für Studis am Institut für Informatik an der Georg-August-Universität Göttingen.
Die VM ist insbesondere für den Einsatz in den folgenden Veranstaltungen gedacht:

- Informatik 1
- Grundlagen der C-Programmierung
- Allgemeines Programmierpraktikum (APP)
- Technische Informatik


---

<div class="text-center" >
<a href="install/windows/"          class="btn btn-primary attention-btn"  style="--animation-delay: 0.2s;" role="button">Lifi auf Windows installieren</a>
<a href="install/linux/"            class="btn btn-primary              "  style="--animation-delay: 0.3s;" role="button">Lifi auf Linux installieren</a>
<a href="install/mac_applesilicon/" class="btn btn-primary attention-btn"  style="--animation-delay: 0.4s;" role="button">Lifi auf MacOS (<span style="font-weight: bold;">M1 & M2</span>) installieren</a>
<a href="install/mac/"              class="btn btn-primary              "  style="--animation-delay: 0.6s;" role="button">Lifi auf MacOS (<span style="font-weight: bold;">Intel</span>) installieren</a>
</div>

---

!!! info
    Lifi probiert möglichst ähnlich zu den [Linux Rechnern in den CIP-Pools](https://fg.informatik.uni-goettingen.de/de/raeume/cip.html) zu sein, hat aber auch einige Quality-of-Life Verbesserungen.
    Mit Lifi kannst du also ohne Probleme alle Übungen auf deinem privaten Rechner/Laptop bearbeiten und musst nicht zum Arbeiten andauernd zum Institut fahren.
    Auch wenn das Ifi natürlich trotzdem ein schöner Ort ist.

---

<div class="text-center" >
<a href="downloads/" class="btn btn-secondary"  style="--animation-delay: 0.2s;" role="button">Lifi Herunterladen</a>
</div>

---

<div class="text-center" >
<a href="faq/" class="btn btn-tertiary"  style="--animation-delay: 0.2s;" role="button">FAQ</a>
<a href="https://gitlab.gwdg.de/lifi/lifi-bugs/-/issues" class="btn btn-tertiary"  style="--animation-delay: 0.2s;" role="button">Bug & Issue Tracker</a>
</div>

---

Es ist übrigens geplant, dass Lifi auch im E-Prüfungsraum bei den Klausuren eingesetzt wird. Ein Blick kann also nicht schaden. :wink:

