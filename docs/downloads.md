# Downloads

Hier kannst du die aktuelle *Lifi* Version herunterladen.


<div class="text-center big-margin-bottom attention-btn" >
<a href="https://owncloud.gwdg.de/index.php/s/gLaleluJFjpR38F/download?path=%2F&files=Lifi-20221121.zip" class="btn btn-secondary " role="button">Aktuelle Lifi.zip (für Windows, Linux & Intel MacBooks) Herunterladen</a>
<a href="https://owncloud.gwdg.de/index.php/s/gLaleluJFjpR38F/download?path=%2F&files=Lifi-20221121.tgz" class="btn btn-secondary " role="button">Aktuelle Lifi.tgz (für M1 & M2 MacBooks) Herunterladen</a>
</div>

<!--
!!! warning
    Der Download funktioniert nur im s.g. [GÖNET](https://docs.gwdg.de/doku.php?id=de:services:network_services:goenet:start).

    D.h. um die Datei herunterladen zu können musst du entweder

    - im WLAN (Eduroam) der Uni,
    - an einem Computer in der Uni, oder
    - über den [GWDG VPN Dienst](https://docs.gwdg.de/doku.php?id=de:services:network_services:vpn:start) angemeldet sein.
-->




!!! warning
    Das Archiv ist beim Herunterladen ca. 2,6GB (bzw. 4,5GB) groß, entpackt ca. 8,2GB (bzw. 11,3GB).

---

## Alle Releases :rocket:

### 20221121

- Changelog:
    - Support für M1 & M2 MacBooks (.tgz Datei)
    - Problem mit fehlendem USB 2.0 Support bei der Installation von Lifi auf Windows gefixt ([lifi-bugs#2](https://gitlab.gwdg.de/lifi/lifi-bugs/-/issues/2))
- Version: 20221121
- Releasedatum: 21.11.2022
- Dateien:
    - [Lifi-20221121.zip      ](https://owncloud.gwdg.de/index.php/s/gLaleluJFjpR38F/download?path=%2F&files=Lifi-20221121.zip      )
    - [Lifi-20221121.tgz      ](https://owncloud.gwdg.de/index.php/s/gLaleluJFjpR38F/download?path=%2F&files=Lifi-20221121.tgz      )
    - [Lifi-20221121.sha256sum](https://owncloud.gwdg.de/index.php/s/gLaleluJFjpR38F/download?path=%2F&files=Lifi-20221121.sha256sum)
    - [Lifi-20221121.zipinfo  ](https://owncloud.gwdg.de/index.php/s/gLaleluJFjpR38F/download?path=%2F&files=Lifi-20221121.zipinfo  )
    - Lifi-20221121.buildlog
- Weitere Links:
    - [Lifi Ansible Role Release 20221121](https://gitlab.gwdg.de/lifi/ansible-role-lifi/-/releases/20221121)
    - [Lifi Builder      Release 20221121](https://gitlab.gwdg.de/lifi/lifi-builder/-/releases/20221121)


### 20221108

- Changelog:
    - Chromium installiert
    - intern aufgeräumt
    - Bugs gefixt
- Version: 20221108
- Releasedatum: 08.11.2022
- Dateien:
    - [Lifi-20221108.zip      ](https://owncloud.gwdg.de/index.php/s/gLaleluJFjpR38F/download?path=%2F&files=Lifi-20221108.zip      )
    - [Lifi-20221108.sha256sum](https://owncloud.gwdg.de/index.php/s/gLaleluJFjpR38F/download?path=%2F&files=Lifi-20221108.sha256sum)
    - [Lifi-20221108.zipinfo  ](https://owncloud.gwdg.de/index.php/s/gLaleluJFjpR38F/download?path=%2F&files=Lifi-20221108.zipinfo  )
    - Lifi-20221108.buildlog
- Weitere Links:
    - [Lifi Ansible Role Release 20221108](https://gitlab.gwdg.de/lifi/ansible-role-lifi/-/releases/20221108)
    - [Lifi Builder      Release 20221108](https://gitlab.gwdg.de/lifi/lifi-builder/-/releases/20221108)

### 20221105

- Changelog:
    - "Mit CIP-Pool Verbinden" hinzugefügt
    - MacOS Support verbessert
- Version: 20221105
- Releasedatum: 05.11.2022
- Dateien:
    - Lifi-20221105.zip
    - Lifi-20221105.sha256sum
    - Lifi-20221105.zipinfo
    - Lifi-20221105.buildlog
- Weitere Links:
    - [Lifi Ansible Role Release 20221105](https://gitlab.gwdg.de/lifi/ansible-role-lifi/-/releases/20221105)
    - [Lifi Builder      Release 20221105](https://gitlab.gwdg.de/lifi/lifi-builder/-/releases/20221105)

### 20221102

- Changelog:
    - VirtualBox Gasterweiterungen sind nun halbautomatisch installierbar.
- Version: 20221102
- Releasedatum: 02.11.2022
- Dateien:
    - Lifi-20221102.zip
    - Lifi-20221102.sha256sum
    - Lifi-20221102.zipinfo
    - Lifi-20221102.buildlog
- Weitere Links:
    - [Lifi Ansible Role Release 20221102](https://gitlab.gwdg.de/lifi/ansible-role-lifi/-/releases/20221102)
    - [Lifi Builder      Release 20221102](https://gitlab.gwdg.de/lifi/lifi-builder/-/releases/20221102)

### 20221101

- Changelog:
    - Erster Lifi Release :tada:
- Version: 20221101
- Releasedatum: 01.11.2022
- Dateien:
    - Lifi-20221101.zip
    - Lifi-20221101.sha256sum
    - Lifi-20221101.zipinfo
    - Lifi-20221101.buildlog
- Weitere Links:
    - [Lifi Ansible Role Release 20221101](https://gitlab.gwdg.de/lifi/ansible-role-lifi/-/releases/20221101)
    - [Lifi Builder      Release 20221101](https://gitlab.gwdg.de/lifi/lifi-builder/-/releases/20221101)
