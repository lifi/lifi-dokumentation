# Mit CIP-Pool Verbinden

Du kannst von Lifi aus sehr einfach auf die [CIP-Pool Rechner im Ifi](https://fg.informatik.uni-goettingen.de/de/raeume/cip.html) zugreifen.
Hierzu hat Lifi das Programm "Mit CIP-Pool Verbinden".


![Mit CIP-Pool Verbinden](img/screenshots/cipconnect.png)

!!! warning "Abhängigkeit: Zwei-Faktor-Authentifizierung"
    Damit du von Lifi aus auf die CIP-Pools zugreifen kannst, musst du vorher auf den CIP-Pools einmal die Zwei-Faktor-Authentifizierung (2FA) eingerichtet haben, da Lifi für die Verbindung SSH verwendet.

    Falls du die Zwei-Faktor-Authentifizierung noch nicht eingerichtet hast, folge bitte zunächst den [Schritten zum Einrichten von 2FA aus dem Ifi Wiki (englisch)](https://doc.informatik.uni-goettingen.de/wiki/index.php/Shell#2FA). Prof. Damm hat die [Anleitung zum Einrichten von 2FA auch auf deutsch](https://user.informatik.uni-goettingen.de/~damm/info1/aktuell/2FA.html) übersetzt.

Sobald du in "Mit CIP-Pool Verbinden" deine studenische E-Mailadresse angegeben hast und auf *Verbinden* geklickt hast, wird Lifi dich nach dem aktuellen *Verification Code* fragen.

!!! info
    Den aktuellen *Verification Code* bekommst du von deiner Zwei-Faktor-Authentifizierungs-App (wahrscheinlich *Google Authenticator*).
    Der Code sollte aus sechs Ziffern bestehen und sich alle ca. 30 Sekunden ändern.

![2FA](img/screenshots/cipconnect_verificationcode.png)

Nachdem du den *Verification Code* eingegeben hast, wirst du nach deinem Passwort gefragt.
Das ist dein normales Passwort, welches du auch auf den CIP-Pools, in E-Campus, in Stud.IP, etc... verwendest.

![Passworteingabe](img/screenshots/cipconnect_password.png)

Sofern du dein Passwort korrekt eingegeben hast, sollte sich nun der Dateimanager geöffnet haben, wo du direkten Zugriff auf deine Dateien und Ordner vom CIP-Pool hast.

!!! tip
    Es wurde auch automatisch ein "CIP-Pool" Bookmark in deinem Dateimanager erstellt, den du zukünftig einfach direkt verwenden kannst um dich mit dem CIP-Pool zu verbinden.

    ![Bookmark](img/screenshots/cipconnect_bookmark.png)


<!--
## Befehle auf CIP-Pool ausführen

Falls du, aus welchen Gründen auch immer, Befehle auf den CIP-Pools ausführen möchtest, kannst du einfach in einem beliebigen Ordner in der CIP-Pool Verbindung **Rechtsklick** machen und **Open in Remote Terminal** auswählen.


![Terminal öffnen](img/screenshots/cipconnect_remoteterminal.png)

-->
