---
title: "Lifi auf Linux installieren"
---

# Lifi auf Linux installieren

!!! tip
    Wenn du schon ein Linux auf deinem Laptop/Rechner hast, brauchst du vielleicht Lifi gar nicht.
    Lifi ist auch nur eine Linux VM (Ubuntu 22.04.1 LTS).
    Du kannst ohne Probleme einfach auf deinem Linux die notwendige Software für die jeweiligen Veranstaltungen installieren und direkt verwenden.
    Welche Software du genau brauchst erfährst du meistens in der entsprechenden Vorlesung.

!!! info
    Die Installation von Lifi auf Linux ist nahezu identisch zu der [Installation von Lifi auf Windows](windows.md).
    Entsprechend nicht wundern, dass viele Screenshots hier von Windows sind.

[TOC]

## Schritt 1: VirtualBox installieren

Da Lifi eine VirtualBox VM ist, musst du zunächst VirtualBox installieren.

**Möglichkeit 1:**

Bei den meisten Linux Distros kannst du VirtualBox einfach über die Paketverwaltung installieren.

Auf Ubuntu/Debian kannst du einfach den folgenden Befehl ausführen:
```bash
sudo apt-get install virtualbox
```

Auf Manjaro/Arch kannst du einfach den folgenden Befehl ausführen:
```bash
sudo pacman -S virtualbox
```

**Möglichkeit 2:**

Den aktuellen VirtualBox Release kannst du [auf der VirtualBox Webseite herunterladen](https://www.virtualbox.org/wiki/Downloads).

Explizite Installationsinstruktionen gibt es [in der VirtualBox Dokumentation Kapitel 2.3](https://www.virtualbox.org/manual/UserManual.html#install-linux-host).


## Schritt 2: Lifi herunterladen, extrahieren und installieren

Die aktuelle Version von Lifi kannst du [auf der Downloadseite herunterladen](../../downloads/).

Sobald die Datei heruntergeladen ist, kannst du das Archiv an den Ort wo du Lifi installieren möchtest, z.B. auf den Desktop, extrahieren.

![Rechtsklick zu 'Alle extrahieren...'](../img/screenshots/install_lifi_right_click_linux.png)
<!-- ![Auf Desktop extrahieren](../img/screenshots/install_lifi_extract_linux.png) -->

Es sollten zwei Dateien in dem extrahierten Ordner sein.

!!! tip "Lifi installieren"
    Nun kannst du Lifi mit einem **Doppelklick** auf die Datei mit dem **blauen Icon** installieren. Diese Datei ist vom Typ eine "VirtualBox Machine Definition" und sollte nur wenige KB groß sein. Die Dateiendung ist `.vbox`.

(Die andere Datei ist die virtuelle Festplatte von Lifi. Dort ist die VM drin gespeichert.)

![Lifi installieren](../img/screenshots/install_lifi_linux.png)

Es sollte sich nun VirtualBox geöffnet haben und die "Lifi" VM ausgewählt sein.

![Lifi installiert](../img/screenshots/install_lifi_virtualbox_opened.png)

**Herzlichen Glückwunsch!** Du hast Lifi installiert, **aber** hier bist du leider noch nicht fertig.

Du kannst nun versuchen die VM zu starten, in dem du auf "Starten" klickst.

![Lifi installieren](../img/screenshots/vtx_start_lifi.png)

## Schritt 3: Virtualisierung aktivieren

!!! warning "Das Problem"
    Du wirst wahrscheinlich die Fehlermeldung "VT-x is disabled in the BIOS for all CPU modes." o.ä. sehen, wenn du versuchst die VM zu starten.

    Keine Sorge, das ist normal.

    ![Fehlermeldung](../img/screenshots/vtx_error.png)

    In dem [BIOS/UEFI](https://de.wikipedia.org/wiki/Unified_Extensible_Firmware_Interface) von deinem Laptop/Rechner ist das Virtualisierungsfeature (VT-x bei Intel, AMD-V bei AMD) von deiner CPU gerade deaktiviert.
    Du musst dieses Feature in deinem BIOS/UEFI aktivieren.

!!! info
    Falls du die Fehlermeldung nicht hast und die VM einfach direkt startet, kannst du [zu Schritt 4 springen](#schritt-4-gasterweiterungen-installieren).

!!! tip "Die Lösung"
    Zum Aktivieren des Virtualisierungsfeatures musst du zunächst in dein BIOS/UEFI kommen. Die Art und Weise wie das geht ist leider von Hersteller zu Hersteller unterschiedlich.
    Häufig funktionieren aber die folgenden Schritte:

    1. Du fährst deinen Laptop/Rechner herunter.
    2. Du startest deinen Laptop/Rechner und fängst direkt an
    3. die Tasten Esc, F2, F8, F10, F12, und Entf zu spammen. (Häufig ist F2 die richtige Taste.)
    4. Du solltest nun im BIOS/UEFI sein. Falls nicht, beginne wieder beim ersten Schritt.

    Sobald du in deinem BIOS/UEFI bist, musst du ein bisschen Suchen.
    Du suchst eine Option oder eine Checkbox zum Aktivieren von Virtualisierung.
    Wie diese Option genau heißt und in welchem Untermenü diese ist, ist wieder von Hersteller zu Hersteller unterschiedlich.
    Wahrscheinlich hat diese Option aber irgendwas mit "VT-x", "AMD-V", oder "Virtualisierung" im Namen.

    Sobald du die Option gefunden und aktiviert hast, kannst du deine Änderungen speichern und das System neustarten.
    (Im BIOS/UEFI heißt neustarten auch häufig "Save & Reset" oder einfach nur "Save & Exit".)

In Linux kannst du nun VirtualBox wieder starten und die Lifi VM wieder versuchen zu starten. Nun sollte sie auch wirklich starten.

## Schritt 4: Gasterweiterungen installieren

!!! warning "Das Problem"
    Theoretisch ist Lifi jetzt schon nutzbar, aber wenn du probierst das Fenster zu vergrößern wird dir auffallen, dass der Lifi Desktop sich nicht vergrößert und stattdessen das Fenster graue Ränder hat.
    Das wollen wir natürlich nicht.
    (Es gibt auch noch ein paar weitere Quality-of-Life Features, die ohne die Gasterweiterungen nicht funktioniern.)

    ![Gasterweiterungen fehlen](../img/screenshots/guest_additions_missing.png)

!!! tip "Die Lösung"
    Um das Problem zu beheben musst du die "VirtualBox Gasterweiterungen" installieren.<br>
    Klicke dafür einfach bei der VM oben in der Menüleiste auf "Geräte" und dann auf "Gasterweiterungen einlegen..." (Englisch: "Devices" -> "Insert Guest Additions CD image...").

    ![Gasterweiterungen einlegen](../img/screenshots/guest_additions_insert.PNG)

<!--
 (Scheinbar tritt der Fehler nicht mehr auf.)
Nach einiger Zeit sollte rechts der folgende Fehler auftauchen:

![Gasterweiterungen Fehler](../img/screenshots/guest_additions_error.PNG)

Keine Sorge, das ist normal.-->

Nach kurzer Zeit (paar Sekunden bis zwei Minuten) sollte Lifi automatisch ein Fenster öffnen, wo es dann die Gasterweiterungen automatisch installiert.

![Gasterweiterungen fertig installiert](../img/screenshots/guest_additions_shutdown.PNG)

Sobald die Gasterweiterungen fertig installiert sind, wird die VM sich automatisch herunterfahren.

Sobald die VM heruntergefahren ist, kannst du sie auch schon direkt wieder starten.
Alles sollte jetzt ohne Probleme funktionieren.

## Fertig

Hurra! Du hast nun eine VM die du für dein Studium benutzen kannst! :tada:

Lifi kannst du nun fortan einfach über VirtualBox starten.

![Lifi starten](../img/screenshots/vtx_start_lifi.png)

Viel Spaß beim Studieren!













