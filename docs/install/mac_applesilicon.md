---
title: "Lifi auf MacOS (M1 & M2) installieren"
---

# Lifi auf MacOS (M1 & M2) installieren

!!! info
    Diese Schritte sind für **M1 & M2** MacBooks.
    Falls du ein Intel MacBook hast, bitte nutze ['Lifi auf MacOS (Intel) installieren'](./mac.md).

!!! warning "Parität"
    Lifi auf M1 und M2 MacBooks ist leider nicht zu 100% identisch zum normalen Lifi.
    Die folgenden Dinge funktionieren nicht:

    - Chromium
    - `htmljs2pdf`

[TOC]

## Schritt 1: UTM installieren


Lifi für M1 & M2 MacBooks ist eine UTM virtuelle Maschine.
Entsprechend musst du zunächst UTM installieren.

Die aktuelle UTM Version kannst du [auf der UTM Webseite herunterladen](https://mac.getutm.app/).

UTM ist kostenlos. Die Version von UTM im Mac App Store (die Geld kostet) ist identisch zur Version die du auf der Webseite kostenlos herunterlädst.

## Schritt 2: Lifi herunterladen, extrahieren und installieren

Die aktuelle Version von Lifi kannst du [auf der Downloadseite herunterladen](../../downloads/).
**Du benötigst die "für M1 & M2" Version.** Die Dateiendung ist `.tgz`.

Sobald die Datei heruntergeladen ist, wird MacOS automatisch die Datei zu einer `.tar` Datei umwandeln, welche du durch einen einfachen Doppelklick entpacken kannst.

Es sollte nun eine `Lifi-2022MMDD.utm` Datei in dem Ordner liegen.

!!! tip "Lifi installieren"
    Nun kannst du Lifi mit einem **Doppelklick** auf die `.utm` Datei installieren.


![UTM](../img/screenshots/utm.png)

Es sollte sich nun UTM geöffnet haben und die "Lifi" VM ausgewählt sein.

**Herzlichen Glückwunsch!** Du hast Lifi installiert.

Du kannst nun die VM starten, in dem du auf den Play Button klickst.

!!! info
    Wenn du die VM herunterfährst, wird  UTM nicht automatisch geschlossen.
    Du musst das Fenster dann manuell schließen.
    ![UTM](../img/screenshots/utm_poweroff.png)


## Fertig

Hurra! Du hast nun eine VM die du für dein Studium benutzen kannst! :tada:

Lifi kannst du nun fortan einfach über UTM starten.

Viel Spaß beim Studieren!











