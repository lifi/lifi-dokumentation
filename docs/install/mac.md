---
title: "Lifi auf MacOS (Intel) installieren"
---

# Lifi auf MacOS (Intel) installieren

!!! info
    Diese Schritte sind für **Intel** MacBooks.
    Falls du ein M1 oder M2 MacBook hast, bitte nutze ['Lifi auf MacOS (M1 & M2) installieren'](./mac_applesilicon.md).

!!! warning "Screenshots"
    Nicht wundern, einige Screenshots auf dieser Seite sind von Windows und Linux.

[TOC]

## Schritt 1: VirtualBox installieren

Da Lifi eine VirtualBox VM ist, musst du zunächst VirtualBox installieren.

Den aktuellen VirtualBox Release kannst du [auf der VirtualBox Webseite herunterladen](https://www.virtualbox.org/wiki/Downloads).

Explizite Installationsinstruktionen gibt es [in der VirtualBox Dokumentation Kapitel 2.2](https://www.virtualbox.org/manual/UserManual.html#installation-mac).

## Schritt 2: Lifi herunterladen, extrahieren und installieren

Die aktuelle Version von Lifi kannst du [auf der Downloadseite herunterladen](../../downloads/).

Sobald die Datei heruntergeladen ist, kannst du das Archiv an den Ort wo du Lifi installieren möchtest, z.B. auf den Desktop, extrahieren.

!!! info
    Scheinbar scheint MacOS manchmal(?) ZIP Archive automatisch zu extrahieren.
    In dem Fall kannst du den extrahierten Ordner einfach dahin verschieben, wo du Lifi installiert haben möchtest.

<!-- ![Rechtsklick zu 'Alle extrahieren...'](../img/screenshots/install_lifi_right_click.png)
![Auf Desktop extrahieren](../img/screenshots/install_lifi_extract.png) -->

Es sollten zwei Dateien in dem extrahierten Ordner sein.

!!! tip "Lifi installieren"
    Nun kannst du Lifi mit einem **Doppelklick** auf die Datei mit dem **blauen Icon** installieren. Diese Datei ist vom Typ eine "VirtualBox Machine Definition" und sollte nur wenige KB groß sein. Die Dateiendung ist `.vbox`.

(Die andere Datei ist die virtuelle Festplatte von Lifi. Dort ist die VM drin gespeichert.)

![Lifi installieren](../img/screenshots/install_lifi_linux.png)
![Lifi installiert](../img/screenshots/install_lifi_virtualbox_opened.png)

Es sollte sich nun VirtualBox geöffnet haben und die "Lifi" VM ausgewählt sein.

**Herzlichen Glückwunsch!** Du hast Lifi installiert, **aber** hier bist du leider noch nicht fertig.

Du kannst nun die VM starten, in dem du auf "Starten" klickst.

![Lifi installieren](../img/screenshots/vtx_start_lifi.png)

## Schritt 3: Tastaturlayout setzen

Standardmäßig geht Lifi von einem Windows-typischen Tastaturlayout aus.

Du kannst das Tastaturlayout in Lifi ändern, in dem du oben rechts auf "de<sub>1</sub>" klickst und "German (Macintosh)" auswählst.

![Tastaturlayout setzen](../img/screenshots/keyboardlayout.png)

!!! warning "Windows-typische Shortcuts"
    Obwohl du das Tastaturlayout zwar geändert hast, verwenden Programme trotzdem Windows-typische Shortcuts.

    Heißt z.B. zum Speichern einer Datei musst du **Control + s** statt **Command + s** drücken.

    Die **Command** Taste funktioniert in Lifi wie die **Windows** Taste.

## Schritt 4: Gasterweiterungen installieren

!!! warning "Das Problem"
    Theoretisch ist Lifi jetzt schon nutzbar, aber wenn du probierst das Fenster zu vergrößern wird dir auffallen, dass der Lifi Desktop sich nicht vergrößert und stattdessen das Fenster graue Ränder hat.
    Das wollen wir natürlich nicht.
    (Es gibt auch noch ein paar weitere Quality-of-Life Features, die ohne die Gasterweiterungen nicht funktioniern.)

    ![Gasterweiterungen fehlen](../img/screenshots/guest_additions_missing.png)

!!! tip "Die Lösung"
    Um das Problem zu beheben musst du die "VirtualBox Gasterweiterungen" installieren.<br>
    Klicke dafür einfach bei der VM oben in der Menüleiste auf "Geräte" und dann auf "Gasterweiterungen einlegen..." (Englisch: "Devices" -> "Insert Guest Additions CD image...").

    ![Gasterweiterungen einlegen](../img/screenshots/guest_additions_insert.PNG)

<!--
 (Scheinbar tritt der Fehler nicht mehr auf.)
Nach einiger Zeit sollte rechts der folgende Fehler auftauchen:

![Gasterweiterungen Fehler](../img/screenshots/guest_additions_error.PNG)

Keine Sorge, das ist normal.-->

Nach kurzer Zeit (paar Sekunden bis zwei Minuten) sollte Lifi automatisch ein Fenster öffnen, wo es dann die Gasterweiterungen automatisch installiert.

![Gasterweiterungen fertig installiert](../img/screenshots/guest_additions_shutdown.PNG)

Sobald die Gasterweiterungen fertig installiert sind, wird die VM sich automatisch herunterfahren.

Sobald die VM heruntergefahren ist, kannst du sie auch schon direkt wieder starten.
Alles sollte jetzt ohne Probleme funktionieren.

## Fertig

Hurra! Du hast nun eine VM die du für dein Studium benutzen kannst! :tada:

Lifi kannst du nun fortan einfach über VirtualBox starten.

![Lifi starten](../img/screenshots/vtx_start_lifi.png)

Viel Spaß beim Studieren!














