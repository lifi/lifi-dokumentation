# Lifi Aktualisieren


Lifi hat einen integrierten Updater.

Du kannst entweder den 
```bash
update
```
Befehl im Terminal ausführen oder das 'System Aktualisieren'-Programm starten.

![Updater](img/screenshots/update.png)


Du bekommst wöchentlich auch regelmäßig eine Benachrichtigung, dass es neue Updates gibt.
Wenn du auf diese Benachrichtigung klickst wird automatisch 'System Aktualisieren' gestartet.
