# Softwarelizenzen

Alle Angaben auf dieser Seite sind ohne Gewähr.

## Ubuntu

> Ubuntu is freely available to all users for personal, or in the case of organisations, internal use. It is provided for this use without warranty. All implied warranties are disclaimed to the fullest extent permitted at law.
> [...]
> You can modify Ubuntu for personal or internal use.
> [...]
> You can make changes to Ubuntu for your own personal use or for your organisation’s own internal use.
>
> ~ [Ubuntu Intellectual property rights policy](https://ubuntu.com/legal/intellectual-property-policy)

Lifi ist eine virtuelle Maschine (VM) für Studierende am Institut für Informatik an der Georg-August-Universität Göttingen.

Lifi ist ausschließlich für den institusinternen Gebrauch an der Universität.

Lifi basiert auf [Ubuntu 22.04.1 LTS](https://www.releases.ubuntu.com/22.04/).

## Lifi

Lifi wird über die [Lifi Ansible Role](https://gitlab.gwdg.de/lifi/ansible-role-lifi) konfiguriert.
Lifi Ansible Role ist unter der [CC0 1.0 Universal Lizenz](https://gitlab.gwdg.de/lifi/ansible-role-lifi/-/blob/main/LICENSE) veröffentlicht.

Diese Dokumentation ist ebenfalls unter der [CC0 1.0 Universal Lizenz](https://gitlab.gwdg.de/lifi/lifi-dokumentation/-/blob/main/LICENSE) veröffentlicht.

## Einzelne Softwarepakete

Die folgende Tabelle listet die Lizenzen der verschiedenen vorinstallierten Softwarepakete in Lifi auf.
Die Tabelle wurde mit dem Tool [dpkg-licenses](https://github.com/daald/dpkg-licenses) generiert.

Die genauen Lizenztexte für die meisten Pakete sind in Lifi unter dem Pfad `/usr/share/doc/<PAKETNAME>/copyright` zu finden.

Alle Pakete die hier nur `unknown` als Lizenz stehen haben, haben einen komplett eigenen Lizenztext. Dieser Lizenztext ist dann auch immer unter `/usr/share/doc/<PAKETNAME>/copyright` in Lifi zu finden.

| Name                            | Lizenzen | Beschreibung                                                   | Version                         |
| ----                            | -------- | -----------                                                   | -------                         |
| accountsservice                 | GPL-2+ GPL-3+ | query and manipulate user account information                 | 22.07.5-2ubuntu1.3              |
| acl                             | GPL-2+ LGPL-2+ | access control list - utilities                               | 2.3.1-1                         |
| acpi-support                    | GPL-2+ | scripts for handling many ACPI events                         | 0.144                           |
| acpid                           | GPL-2 | Advanced Configuration and Power Interface event daemon       | 1:2.0.33-1ubuntu1               |
| adduser                         | GPL-2 | add and remove users and groups                               | 3.118ubuntu5                    |
| adwaita-icon-theme              | GFDL-1.2 GPL LGPL-3 | default icon theme of GNOME (small subset)                    | 41.0-1ubuntu1                   |
| alsa-base                       | GPL-2 | ALSA driver configuration files                               | 1.0.25+dfsg-0ubuntu7            |
| alsa-topology-conf              | BSD-3-clause | ALSA topology configuration files                             | 1.2.5.1-2                       |
| alsa-ucm-conf                   | BSD-3-clause | ALSA Use Case Manager configuration files                     | 1.2.6.3-1ubuntu1.1              |
| alsa-utils                      | GPL-2 LGPL-2+ | Utilities for configuring and using ALSA                      | 1.2.6-1ubuntu1                  |
| anacron                         | GPL-2 | cron-like program that doesn't go by time                     | 2.3-31ubuntu2                   |
| ansible                         | Apache-2 BSD-2-clause BSD-2-Clause BSD-2-Clause or Python-2 Expat GPL-3+ Python-2 | Configuration management, deployment, and task execution sys  | 2.10.7+merged+base+2.10.8+dfsg  |
| ant                             | Apache-2.0 | Java based build tool like make                               | 1.10.12-1                       |
| ant-doc                         | Apache-2.0 | Java based build tool like make - API documentation and manu  | 1.10.12-1                       |
| ant-optional                    | Apache-2.0 | Java based build tool like make - optional libraries          | 1.10.12-1                       |
| apg                             | BSD-3-Clause | Automated Password Generator - Standalone version             | 2.2.3.dfsg.1-5build2            |
| apparmor                        | BSD-3-clause BSD-3-clause or GPL-2+ GPL-2 GPL-2+ LGPL-2.1+ | user-space parser utility for AppArmor                        | 3.0.4-2ubuntu2.1                |
| apport                          | GPL-2+ | automatically generate crash reports for debugging            | 2.20.11-0ubuntu82.1             |
| apport-gtk                      | GPL-2+ | GTK+ frontend for the apport crash report system              | 2.20.11-0ubuntu82.1             |
| apport-symptoms                 | GPL-2+ | symptom scripts for apport                                    | 0.24                            |
| appstream                       | FSFAP GPL-2+ LGPL-2.1+ SIL-1.1 | Software component metadata management                        | 0.15.2-2                        |
| apt                             | GPL-2 | commandline package manager                                   | 2.4.8                           |
| apt-config-icons                | FSFAP GPL-2+ LGPL-2.1+ SIL-1.1 | APT configuration snippet to enable icon downloads            | 0.15.2-2                        |
| apt-config-icons-hidpi          | FSFAP GPL-2+ LGPL-2.1+ SIL-1.1 | APT configuration snippet to enable HiDPI icon downloads      | 0.15.2-2                        |
| apt-utils                       | GPL-2 | package management related utility programs                   | 2.4.8                           |
| aptdaemon                       | GPL-2 | transaction based package management service                  | 1.1.1+bzr982-0ubuntu39          |
| aptdaemon-data                  | GPL-2 | data files for clients                                        | 1.1.1+bzr982-0ubuntu39          |
| aspell                          | GFDL-1.2+ LGPL-2.1+ | GNU Aspell spell-checker                                      | 0.60.8-4build1                  |
| aspell-de                       | BSD-4-clause GPL-2+ | German dictionary for aspell                                  | 20161207-9                      |
| aspell-en                       | public domain Public Domain | English dictionary for GNU Aspell                             | 2018.04.16-0-1                  |
| at-spi2-core                    | AFL-2.1 AFL-2.1 or GPL-2+ GPL-2 GPL-2+ LGPL-2+ public-domain | Assistive Technology Service Provider Interface (dbus core)   | 2.44.0-3                        |
| avahi-autoipd                   | GPL GPL-2 LGPL-2.1 | Avahi IPv4LL network address configuration daemon             | 0.8-5ubuntu5                    |
| avahi-daemon                    | GPL GPL-2 LGPL-2.1 | Avahi mDNS/DNS-SD daemon                                      | 0.8-5ubuntu5                    |
| avahi-utils                     | GPL GPL-2 LGPL-2.1 | Avahi browsing, publishing and discovery utilities            | 0.8-5ubuntu5                    |
| base-files                      | GPL | Debian base system miscellaneous files                        | 12ubuntu4.2                     |
| base-passwd                     | GPL-2 public-domain | Debian base system master password and group files            | 3.5.52build1                    |
| bash                            | GPL-3 | GNU Bourne Again SHell                                        | 5.1-6ubuntu1                    |
| bash-completion                 | GPL-2+ | programmable completion for the bash shell                    | 1:2.11-5ubuntu1                 |
| bc                              | GPL-2.0+ GPL-2.0+ with Texinfo exception permissive permissive' public-domain X11 | GNU bc arbitrary precision calculator language                | 1.07.1-3build1                  |
| bind9-dnsutils                  | BSD-2-clause BSD-3-clause CC0-1.0 ISC ISC or MPL-2.0 MPL-2.0 public-domain | Clients provided with BIND 9                                  | 1:9.18.1-1ubuntu1.2             |
| bind9-host                      | BSD-2-clause BSD-3-clause CC0-1.0 ISC ISC or MPL-2.0 MPL-2.0 public-domain | DNS Lookup Utility                                            | 1:9.18.1-1ubuntu1.2             |
| bind9-libs:amd64                | BSD-2-clause BSD-3-clause CC0-1.0 ISC ISC or MPL-2.0 MPL-2.0 public-domain | Shared Libraries used by BIND 9                               | 1:9.18.1-1ubuntu1.2             |
| binutils                        | GFDL GPL LGPL | GNU assembler, linker and binary utilities                    | 2.38-4ubuntu2                   |
| binutils-common:amd64           | GFDL GPL LGPL | Common files for the GNU assembler, linker and binary utilit  | 2.38-4ubuntu2                   |
| binutils-x86-64-linux-gnu       | GFDL GPL LGPL | GNU binary utilities, for x86-64-linux-gnu target             | 2.38-4ubuntu2                   |
| bluez                           | GFDL GPL-2 LGPL-2.1 | Bluetooth tools and daemons                                   | 5.64-0ubuntu1                   |
| bolt                            | LGPL-2.1+ | system daemon to manage thunderbolt 3 devices                 | 0.9.2-1                         |
| branding-ubuntu                 | GPL-2 | Replacement artwork with Ubuntu branding                      | 0.10                            |
| brltty                          | LGPL-2.1 | Access software for a blind person using a braille display    | 6.4-4ubuntu3                    |
| bsdextrautils                   | BSD-2-clause BSD-3-clause BSD-4-clause GPL-2 GPL-2+ GPL-3+ LGPL LGPL-2+ LGPL-2.1+ LGPL-3+ MIT public-domain | extra utilities from 4.4BSD-Lite                              | 2.37.2-4ubuntu3                 |
| bsdutils                        | BSD-2-clause BSD-3-clause BSD-4-clause GPL-2 GPL-2+ GPL-3+ LGPL LGPL-2+ LGPL-2.1+ LGPL-3+ MIT public-domain | basic utilities from 4.4BSD-Lite                              | 1:2.37.2-4ubuntu3               |
| bubblewrap                      | GPL-2+ with Autoconf exception LGPL-2+ | utility for unprivileged chroot and namespace manipulation    | 0.6.1-1                         |
| build-essential                 | GPL | Informational list of build-essential packages                | 12.9ubuntu3                     |
| busybox-initramfs               | GPL-2 | Standalone shell setup for initramfs                          | 1:1.30.1-7ubuntu3               |
| busybox-static                  | GPL-2 | Standalone rescue shell with tons of builtin utilities        | 1:1.30.1-7ubuntu3               |
| bzip2                           | BSD-variant GPL-2 | high-quality block-sorting file compressor - utilities        | 1.0.8-5build1                   |
| ca-certificates                 | GPL-2+ MPL-2.0 | Common CA certificates                                        | 20211016                        |
| ca-certificates-java            | GPL | Common CA certificates (JKS keystore)                         | 20190909                        |
| caca-utils                      | LGPL | text mode graphics utilities                                  | 0.99.beta19-2.2ubuntu4          |
| chafa                           | Expat LGPL-3.0+ MIT | Image-to-text converter supporting a wide range of symbols,   | 1.8.0-1                         |
| cheese-common                   | CC-BY-SA-3.0 GPL-2+ | Common files for the Cheese tool to take pictures and videos  | 41.1-1build1                    |
| codium                          | MIT | Code editing. Redefined.                                      | 1.73.0.22306                    |
| colord                          | CC0 GFDL-NIV GPL-2+ LGPL-2.1+ | system service to manage device colour profiles -- system da  | 1.4.6-1                         |
| colord-data                     | CC0 GFDL-NIV GPL-2+ LGPL-2.1+ | system service to manage device colour profiles -- data file  | 1.4.6-1                         |
| command-not-found               | GPL GPL-3 | Suggest installation of packages in interactive bash session  | 22.04.0                         |
| console-setup                   | GPL-2 | console font and keymap setup program                         | 1.205ubuntu3                    |
| console-setup-linux             | GPL-2 | Linux specific part of console-setup                          | 1.205ubuntu3                    |
| coreutils                       | GPL-3 | GNU core utilities                                            | 8.32-4.1ubuntu1                 |
| cowsay                          | COWSAY GPL GPL-2+ WTFPL-2 | configurable talking cow                                      | 3.03+dfsg2-8                    |
| cpio                            | GPL-3 | GNU cpio -- a program to manage archives of files             | 2.13+dfsg-7                     |
| cpp                             | GPL-2 | GNU C preprocessor (cpp)                                      | 4:11.2.0-1ubuntu1               |
| cpp-11                          | Artistic GFDL-1.2 GPL GPL-2 GPL-3 LGPL | GNU C preprocessor                                            | 11.3.0-1ubuntu1~22.04           |
| cracklib-runtime                | LGPL-2.1 | runtime support for password checker library cracklib2        | 2.9.6-3.4build4                 |
| cron                            | Artistic GPL-2+ ISC Paul-Vixie's-license | process scheduling daemon                                     | 3.0pl1-137ubuntu3               |
| cups-pk-helper                  | GPL-2 | PolicyKit helper to configure cups with fine-grained privile  | 0.2.6-1ubuntu5                  |
| curl                            | BSD-3-Clause BSD-4-Clause curl ISC other public-domain | command line tool for transferring data with URL syntax       | 7.81.0-1ubuntu1.6               |
| dash                            | BSD-3-clause BSD-3-Clause Expat FSFUL FSFULLR GPL-2+ GPL-2+ or BSD-3-clause public-domain | POSIX-compliant shell                                         | 0.5.11+git20210903+057cd650a4e  |
| dbus                            | AFL-2.1 BSD-3-clause BSD-3-clause-generic Expat g10-permissive GPL-2+ GPL-2+ or AFL-2.1 GPL-2+ or AFL-2.1, Tcl-BSDish | simple interprocess messaging system (daemon and utilities)   | 1.12.20-2ubuntu4.1              |
| dbus-user-session               | AFL-2.1 BSD-3-clause BSD-3-clause-generic Expat g10-permissive GPL-2+ GPL-2+ or AFL-2.1 GPL-2+ or AFL-2.1, Tcl-BSDish | simple interprocess messaging system (systemd --user integra  | 1.12.20-2ubuntu4.1              |
| dconf-cli                       | GPL-3 LGPL-2+ | simple configuration storage system - utilities               | 0.40.0-3                        |
| dconf-gsettings-backend:amd64   | GPL-3 LGPL-2+ | simple configuration storage system - GSettings back-end      | 0.40.0-3                        |
| dconf-service                   | GPL-3 LGPL-2+ | simple configuration storage system - D-Bus service           | 0.40.0-3                        |
| dctrl-tools                     | BSD-2-clause GPL-2+ | Command-line tools to process Debian package information      | 2.24-3build2                    |
| debconf                         | BSD-2-clause | Debian configuration management system                        | 1.5.79ubuntu1                   |
| debconf-i18n                    | BSD-2-clause | full internationalization support for debconf                 | 1.5.79ubuntu1                   |
| debianutils                     | GPL-2 | Miscellaneous utilities specific to Debian                    | 5.5-1ubuntu2                    |
| default-jdk                     | GPL-2+ | Standard Java or Java compatible Development Kit              | 2:1.11-72build2                 |
| default-jdk-headless            | GPL-2+ | Standard Java or Java compatible Development Kit (headless)   | 2:1.11-72build2                 |
| default-jre                     | GPL-2+ | Standard Java or Java compatible Runtime                      | 2:1.11-72build2                 |
| default-jre-headless            | GPL-2+ | Standard Java or Java compatible Runtime (headless)           | 2:1.11-72build2                 |
| desktop-file-utils              | GPL-2+ | Utilities for .desktop files                                  | 0.26-1ubuntu3                   |
| dex                             | GPL-3+ MIT | generate and execute Application type .desktop files          | 0.9.0-1                         |
| dictionaries-common             | GPL-2+ GPL-3+ | spelling dictionaries - common utilities                      | 1.28.14                         |
| diffutils                       | GFDL GPL | File comparison utilities                                     | 1:3.8-0ubuntu2                  |
| dirmngr                         | BSD-3-clause CC0-1.0 Expat GPL-3+ GPL-3+ or BSD-3-clause LGPL-2.1+ LGPL-3+ permissive RFC-Reference TinySCHEME | GNU privacy guard - network certificate management service    | 2.2.27-3ubuntu2.1               |
| distro-info                     | ISC | provides information about the distributions' releases        | 1.1build1                       |
| distro-info-data                | ISC | information about the distributions' releases (data files)    | 0.52ubuntu0.1                   |
| dkms                            | GPL-2+ | Dynamic Kernel Module Support Framework                       | 2.8.7-2ubuntu2                  |
| dmidecode                       | GPL-2+ | SMBIOS/DMI table decoder                                      | 3.3-3ubuntu0.1                  |
| dmsetup                         | BSD-2-Clause GPL-2.0 GPL-2.0+ LGPL-2.1 | Linux Kernel Device Mapper userspace library                  | 2:1.02.175-2.1ubuntu4           |
| dmz-cursor-theme                | GPL-2 | Style neutral, scalable cursor theme                          | 0.4.5ubuntu1                    |
| dns-root-data                   | Expat ICANN-Public | DNS root data including root zone and DNSSEC key              | 2021011101                      |
| dnsmasq-base                    | GPL-2 GPL-3 | Small caching DNS proxy and DHCP/TFTP server                  | 2.86-1.1ubuntu0.1               |
| docbook-xml                     | Docbook GPL-2+ | standard XML documentation system for software and systems    | 4.5-11                          |
| dosfstools                      | GPL-3+ public-domain | utilities for making and checking MS-DOS FAT filesystems      | 4.2-1build3                     |
| dpkg                            | BSD-2-clause GPL-2 GPL-2+ public-domain-md5 public-domain-s-s-d | Debian package management system                              | 1.21.1ubuntu2.1                 |
| dpkg-dev                        | BSD-2-clause GPL-2 GPL-2+ public-domain-md5 public-domain-s-s-d | Debian package development tools                              | 1.21.1ubuntu2.1                 |
| dunst                           | BSD-3-clause ISC | dmenu-ish notification-daemon                                 | 1.5.0-1build1                   |
| e2fsprogs                       | GPL-2 LGPL-2 | ext2/ext3/ext4 file system utilities                          | 1.46.5-2ubuntu1.1               |
| ed                              | BSD-2-Clause FCONF FDOC FLOG GFDL-NIV-1.3+ GPL-2+ GPL-3+ | classic UNIX line editor                                      | 1.18-1                          |
| efibootmgr                      | GPL-2.0+ | Interact with the EFI Boot Manager                            | 17-1ubuntu2                     |
| eject                           | BSD-2-clause BSD-3-clause BSD-4-clause GPL-2 GPL-2+ GPL-3+ LGPL LGPL-2+ LGPL-2.1+ LGPL-3+ MIT public-domain | ejects CDs and operates CD-Changers under Linux               | 2.37.2-4ubuntu3                 |
| emacs                           | efaq.texi license efaq-w32.texi license gnulib-comp.m4 license GPL-2+ GPL-3+ GPL plus Ian LGPL-2+ m4 license manpage license meese.el license MPL-2.0 nt/inc/dirent.h license pkg.m4 license same as corresponding file in /etc/images sedadmin.inp license | GNU Emacs editor (metapackage)                                | 1:27.1+1-3ubuntu5               |
| emacs-bin-common                | efaq.texi license efaq-w32.texi license gnulib-comp.m4 license GPL-2+ GPL-3+ GPL plus Ian LGPL-2+ m4 license manpage license meese.el license MPL-2.0 nt/inc/dirent.h license pkg.m4 license same as corresponding file in /etc/images sedadmin.inp license | GNU Emacs editor's shared, architecture dependent files       | 1:27.1+1-3ubuntu5               |
| emacs-common                    | efaq.texi license efaq-w32.texi license gnulib-comp.m4 license GPL-2+ GPL-3+ GPL plus Ian LGPL-2+ m4 license manpage license meese.el license MPL-2.0 nt/inc/dirent.h license pkg.m4 license same as corresponding file in /etc/images sedadmin.inp license | GNU Emacs editor's shared, architecture independent infrastr  | 1:27.1+1-3ubuntu5               |
| emacs-el                        | efaq.texi license efaq-w32.texi license gnulib-comp.m4 license GPL-2+ GPL-3+ GPL plus Ian LGPL-2+ m4 license manpage license meese.el license MPL-2.0 nt/inc/dirent.h license pkg.m4 license same as corresponding file in /etc/images sedadmin.inp license | GNU Emacs LISP (.el) files                                    | 1:27.1+1-3ubuntu5               |
| emacs-gtk                       | efaq.texi license efaq-w32.texi license gnulib-comp.m4 license GPL-2+ GPL-3+ GPL plus Ian LGPL-2+ m4 license manpage license meese.el license MPL-2.0 nt/inc/dirent.h license pkg.m4 license same as corresponding file in /etc/images sedadmin.inp license | GNU Emacs editor (with GTK+ GUI support)                      | 1:27.1+1-3ubuntu5               |
| emacsen-common                  | GPL | Common facilities for all emacsen                             | 3.0.4                           |
| enchant-2                       | Expat FSFAP GPL-2.0+ GPL-3.0+ LGPL-2.0+ LGPL-2.1+ LGPL-3.0+ | Wrapper for various spell checker engines (binary programs)   | 2.3.2-1ubuntu2                  |
| eog                             | GPL-2 LGPL-2 | Eye of GNOME graphics viewer program                          | 42.0-1                          |
| espeak-ng-data:amd64            | GPL-3 | Multi-lingual software speech synthesizer: speech data files  | 1.50+dfsg-10                    |
| evince                          | GFDL GPL GPL-2 LGPL-2 | Document (PostScript, PDF) viewer                             | 42.3-0ubuntu2                   |
| evince-common                   | GFDL GPL GPL-2 LGPL-2 | Document (PostScript, PDF) viewer - common files              | 42.3-0ubuntu2                   |
| evolution-data-server           | LGPL-2 LGPL-2.1 | evolution database backend server                             | 3.44.4-0ubuntu1                 |
| evolution-data-server-common    | LGPL-2 LGPL-2.1 | architecture independent files for Evolution Data Server      | 3.44.4-0ubuntu1                 |
| fakeroot                        | Artistic GPL-3 | tool for simulating superuser privileges                      | 1.28-1ubuntu1                   |
| fdisk                           | BSD-2-clause BSD-3-clause BSD-4-clause GPL-2 GPL-2+ GPL-3+ LGPL LGPL-2+ LGPL-2.1+ LGPL-3+ MIT public-domain | collection of partitioning utilities                          | 2.37.2-4ubuntu3                 |
| file                            | BSD-2-Clause-alike BSD-2-Clause-netbsd BSD-2-Clause-regents MIT-Old-Style-with-legal-disclaimer-2 public-domain | Recognize the type of data in a file using "magic" numbers    | 1:5.41-3                        |
| file-roller                     | GPL-2+ LGPL-2+ | archive manager for GNOME                                     | 3.42.0-1                        |
| findutils                       | GFDL-1.3 GPL-3 | utilities for finding files--find, xargs                      | 4.8.0-1ubuntu3                  |
| firefox                         | BSD-2-clause BSD-3 BSD-3-clause BSD-4-clause GPL-2 GPL-2 or GPL-3 GPL-2 or MIT GPL-3 LGPL-2.1 MIT MPL-1.1 MPL-1.1 or GPL-2 or LGPL-2.1 MPL-1.1 or GPL-2 or LGPL-2.1, MPL-1.1 or LGPL-2.1, MPL-2.0 other public-domain | Safe and easy web browser from Mozilla                        | 106.0.2+build1-0ubuntu0.22.04.  |
| fish                            | GPL-2 GPL-2+ ISC LGPL-2.1+ | friendly interactive shell                                    | 3.3.1+ds-3                      |
| fish-common                     | GPL-2 GPL-2+ ISC LGPL-2.1+ | friendly interactive shell (architecture-independent files)   | 3.3.1+ds-3                      |
| fontconfig                      | unknown | generic font configuration library - support binaries         | 2.13.1-4.2ubuntu5               |
| fontconfig-config               | unknown | generic font configuration library - configuration            | 2.13.1-4.2ubuntu5               |
| fonts-beng                      | ISC | Metapackage to install Bengali and Assamese fonts             | 2:1.3                           |
| fonts-beng-extra                | GPL-2.0+ GPL2+-with-font-exception GPL3+-with-font-exception | TrueType fonts for Bengali language                           | 3.2.1-1                         |
| fonts-dejavu-core               | bitstream-vera GPL-2+ | Vera font family derivate with additional characters          | 2.37-2build1                    |
| fonts-dejavu-extra              | bitstream-vera GPL-2+ | Vera font family derivate with additional characters (extra   | 2.37-2build1                    |
| fonts-deva                      | ISC | Meta package to install all Devanagari fonts                  | 2:1.3                           |
| fonts-deva-extra                | GPL-2.0+ | Free fonts for Devanagari script                              | 3.0-5                           |
| fonts-droid-fallback            | Apache-2 | handheld device font with extensive style and language suppo  | 1:6.0.1r16-1.1build1            |
| fonts-freefont-ttf              | GPL-3+ GPL-3+ with Special Font Exception | Freefont Serif, Sans and Mono Truetype fonts                  | 20120503-10build1               |
| fonts-gargi                     | GPL-2+ | OpenType Devanagari font                                      | 2.0-5                           |
| fonts-gubbi                     | GPL-2+ GPL-2+ with font exception | Gubbi free font for Kannada script                            | 1.3-5build1                     |
| fonts-gujr                      | ISC | Meta package to install all Gujarati fonts                    | 2:1.4                           |
| fonts-gujr-extra                | GPL-2.0+ | Free fonts for Gujarati script                                | 1.0.1-1                         |
| fonts-guru                      | ISC | Meta package to install all Punjabi fonts                     | 2:1.3                           |
| fonts-guru-extra                | GPL-2.0+ GPL-2.0+ with Font exception | Free fonts for Punjabi language                               | 2.0-5                           |
| fonts-indic                     | ISC | Meta package to install all Indian language fonts             | 2:1.4                           |
| fonts-kacst                     | GPL-2 | KACST free TrueType Arabic fonts                              | 2.01+mry-15                     |
| fonts-kacst-one                 | GPL-2 | TrueType font designed for Arabic language                    | 5.0+svn11846-10                 |
| fonts-kalapi                    | GPL-2+ OFL-1.1 | Kalapi Gujarati Unicode font                                  | 1.0-4                           |
| fonts-khmeros-core              | LGPL-2.1+ | KhmerOS Unicode fonts for the Khmer language of Cambodia      | 5.0-9ubuntu1                    |
| fonts-knda                      | ISC | Meta package for Kannada fonts                                | 2:1.3                           |
| fonts-lao                       | GPL-2 LGPL-2 | TrueType font for Lao language                                | 0.0.20060226-10ubuntu2          |
| fonts-liberation                | GPL-2 | Fonts with the same metrics as Times, Arial and Courier       | 1:1.07.4-11                     |
| fonts-liberation2               | GPL-2+ SIL-OFL-1.1 | Fonts with the same metrics as Times, Arial and Courier (v2)  | 2.1.5-1                         |
| fonts-lklug-sinhala             | GPL-2 | Unicode Sinhala font by Lanka Linux User Group                | 0.6-4                           |
| fonts-lohit-beng-assamese       | CC0-1.0 ISC OFL-1.1 | Lohit TrueType font for Assamese Language                     | 2.91.5-2                        |
| fonts-lohit-beng-bengali        | CC0-1.0 ISC OFL-1.1 | Lohit TrueType font for Bengali Language                      | 2.91.5-2                        |
| fonts-lohit-deva                | CC0-1.0 ISC OFL-1.1 | Lohit TrueType font for Devanagari script                     | 2.95.4-4                        |
| fonts-lohit-gujr                | CC0-1.0 ISC OFL-1.1 | Lohit TrueType font for Gujarati Language                     | 2.92.4-4                        |
| fonts-lohit-guru                | CC0-1.0 ISC OFL-1.1 | Lohit TrueType font for Punjabi Language                      | 2.91.2-2build1                  |
| fonts-lohit-knda                | CC0-1.0 ISC OFL-1.1 | Lohit TrueType font for Kannada Language                      | 2.5.4-3                         |
| fonts-lohit-mlym                | CC0-1.0 ISC OFL-1.1 | Lohit TrueType font for Malayalam Language                    | 2.92.2-2                        |
| fonts-lohit-orya                | CC0-1.0 ISC OFL-1.1 | Lohit TrueType font for Oriya Language                        | 2.91.2-2                        |
| fonts-lohit-taml                | CC0-1.0 GPL-2.0+ OFL-1.1 | Lohit TrueType font for Tamil Language                        | 2.91.3-2                        |
| fonts-lohit-taml-classical      | CC0-1.0 GPL-2+ OFL-1.1 | Lohit Tamil TrueType fonts for Tamil script                   | 2.5.4-2                         |
| fonts-lohit-telu                | CC0-1.0 GPL-2.0+ OFL-1.1 | Lohit TrueType font for Telugu Language                       | 2.5.5-2build1                   |
| fonts-mlym                      | ISC | Meta package to install all Malayalam fonts                   | 2:1.3                           |
| fonts-nakula                    | GPL-2.0+ | Free Unicode compliant Devanagari font                        | 1.0-4                           |
| fonts-navilu                    | GPL-2+ OFL-1.1 | Handwriting font for Kannada                                  | 1.2-3                           |
| fonts-noto-cjk                  | GPL-3+ SIL-1.1 | "No Tofu" font families with large Unicode coverage (CJK reg  | 1:20220127+repack1-1            |
| fonts-noto-color-emoji          | Apache-2.0 SIL-1.1 | color emoji font from Google                                  | 2.034-1                         |
| fonts-noto-mono                 | GPL-3+ OFL-1.1 | "No Tofu" monospaced font family with large Unicode coverage  | 20201225-1build1                |
| fonts-opensymbol                | Apache-2.0 BSD-3-clause CC0-1.0 CC-BY-SA-3.0 Expat GPL-2 GPL-2+ GPL-3+ LGPL-2 LGPL-3 LGPL-3+ MPL-1.1 MPL-1.1 or GPL-2 or LGPL-2 MPL-2.0 MPL_2.0 other | OpenSymbol TrueType font                                      | 2:102.12+LibO7.3.6-0ubuntu0.22  |
| fonts-orya                      | ISC | Meta package to install all Odia fonts                        | 2:1.3                           |
| fonts-orya-extra                | GPL-2.0+ GPL-3.0+ | Free fonts for Odia script                                    | 2.0-6                           |
| fonts-pagul                     | GPL-3.0+ GPL-3.0+ with Font exception | Free TrueType font for the Sourashtra language                | 1.0-8                           |
| fonts-sahadeva                  | GPL-2.0+ | Free Unicode compliant Devanagari font                        | 1.0-5                           |
| fonts-samyak-deva               | GPL-3.0+ GPL-3.0+ with font exception | Samyak TrueType font for Devanagari script                    | 1.2.2-5build1                   |
| fonts-samyak-gujr               | GPL-3.0+ GPL-3.0+ with font exception | Samyak TrueType font for Gujarati language                    | 1.2.2-5build1                   |
| fonts-samyak-mlym               | GPL-3.0+ GPL-3.0+ with font exception | Samyak TrueType font for Malayalam language                   | 1.2.2-5build1                   |
| fonts-samyak-taml               | GPL-3.0+ GPL-3.0+ with font exception | Samyak TrueType font for Tamil language                       | 1.2.2-5build1                   |
| fonts-sarai                     | GPL-2.0 | truetype font for devanagari script                           | 1.0-3                           |
| fonts-sil-abyssinica            | OFL-1.1-RFN | Ethiopic script font designed in a calligraphic style         | 2.100-3                         |
| fonts-sil-padauk                | OFL-1.1 | Burmese Unicode TrueType font with OpenType and Graphite sup  | 5.000-3                         |
| fonts-smc                       | GPL-3.0+ | Metapackage for various TrueType fonts for Malayalam Languag  | 1:7.2                           |
| fonts-smc-anjalioldlipi         | CC0-1.0 GPL-3.0+ OFL-1.1 | AnjaliOldLipi malayalam font                                  | 7.1.2-2                         |
| fonts-smc-chilanka              | CC0-1.0 GPL-3.0+ OFL-1.1 | Chilanka malayalam font                                       | 1.540-1                         |
| fonts-smc-dyuthi                | CC0-1.0 GPL-3.0+ GPL-3.0+ with Font Exception GPL-3.0+ with Font Exception or OFL-1.1 OFL-1.1 | Dyuthi malayalam font                                         | 3.0.2-2                         |
| fonts-smc-gayathri              | CC0-1.0 Expat GPL-3.0+ OFL-1.1 | Gayathri Malayalam font                                       | 1.110-2-1                       |
| fonts-smc-karumbi               | CC0-1.0 GPL-3.0+ OFL-1.1 | Karumbi malayalam font                                        | 1.1.2-2                         |
| fonts-smc-keraleeyam            | CC0-1.0 GPL-3.0+ OFL-1.1 | Keraleeyam malayalam font                                     | 3.0.2-2                         |
| fonts-smc-manjari               | CC0-1.0 GPL-3.0+ OFL-1.1 | Manjari malayalam font                                        | 2.000-3                         |
| fonts-smc-meera                 | CC0-1.0 GPL-3.0+ GPL-3.0+ with Font Exception GPL-3.0+ with Font Exception or OFL-1.1 OFL-1.1 | Meera malayalam font                                          | 7.0.3-1                         |
| fonts-smc-rachana               | CC0-1.0 GPL-3.0+ GPL-3.0+ with Font Exception GPL-3.0+ with Font Exception or OFL-1.1 OFL-1.1 | Rachana malayalam font                                        | 7.0.2-1build1                   |
| fonts-smc-raghumalayalamsans    | CC0-1.0 GPL-2.0 GPL-3.0+ | RaghuMalayalamSans malayalam font                             | 2.2.1-1                         |
| fonts-smc-suruma                | CC0-1.0 GPL-3.0+ GPL-3.0+ with Font Exception | Suruma malayalam font                                         | 3.2.3-1                         |
| fonts-smc-uroob                 | CC0-1.0 GPL-3.0+ OFL-1.1 | Uroob malayalam font                                          | 2.0.2-1                         |
| fonts-taml                      | ISC | Meta package to install all Tamil fonts                       | 2:1.4                           |
| fonts-telu                      | ISC | Meta package to install all Telugu fonts                      | 2:1.3                           |
| fonts-telu-extra                | GPL-2.0+ | Free fonts for Telugu script                                  | 2.0-5                           |
| fonts-teluguvijayam             | GPL-2+ OFL-1.1 | TrueType fonts for Telugu script (te)                         | 2.1-1                           |
| fonts-thai-tlwg                 | GPL-2+ GPL-2+ with Font exception LPPL other | Thai fonts maintained by TLWG (metapackage)                   | 1:0.7.3-1                       |
| fonts-tibetan-machine           | GPL-1+ | font for Tibetan, Dzongkha and Ladakhi (OpenType Unicode)     | 1.901b-6                        |
| fonts-tlwg-garuda               | GPL-2+ GPL-2+ with Font exception LPPL other | Thai Garuda font (dependency package)                         | 1:0.7.3-1                       |
| fonts-tlwg-garuda-ttf           | GPL-2+ GPL-2+ with Font exception LPPL other | Thai Garuda TrueType font                                     | 1:0.7.3-1                       |
| fonts-tlwg-kinnari              | GPL-2+ GPL-2+ with Font exception LPPL other | Thai Kinnari font (dependency package)                        | 1:0.7.3-1                       |
| fonts-tlwg-kinnari-ttf          | GPL-2+ GPL-2+ with Font exception LPPL other | Thai Kinnari TrueType font                                    | 1:0.7.3-1                       |
| fonts-tlwg-laksaman             | GPL-2+ GPL-2+ with Font exception LPPL other | Thai Laksaman font (dependency package)                       | 1:0.7.3-1                       |
| fonts-tlwg-laksaman-ttf         | GPL-2+ GPL-2+ with Font exception LPPL other | Thai Laksaman TrueType font                                   | 1:0.7.3-1                       |
| fonts-tlwg-loma                 | GPL-2+ GPL-2+ with Font exception LPPL other | Thai Loma font (dependency package)                           | 1:0.7.3-1                       |
| fonts-tlwg-loma-ttf             | GPL-2+ GPL-2+ with Font exception LPPL other | Thai Loma TrueType font                                       | 1:0.7.3-1                       |
| fonts-tlwg-mono                 | GPL-2+ GPL-2+ with Font exception LPPL other | Thai TlwgMono font (dependency package)                       | 1:0.7.3-1                       |
| fonts-tlwg-mono-ttf             | GPL-2+ GPL-2+ with Font exception LPPL other | Thai TlwgMono TrueType font                                   | 1:0.7.3-1                       |
| fonts-tlwg-norasi               | GPL-2+ GPL-2+ with Font exception LPPL other | Thai Norasi font (dependency package)                         | 1:0.7.3-1                       |
| fonts-tlwg-norasi-ttf           | GPL-2+ GPL-2+ with Font exception LPPL other | Thai Norasi TrueType font                                     | 1:0.7.3-1                       |
| fonts-tlwg-purisa               | GPL-2+ GPL-2+ with Font exception LPPL other | Thai Purisa font (dependency package)                         | 1:0.7.3-1                       |
| fonts-tlwg-purisa-ttf           | GPL-2+ GPL-2+ with Font exception LPPL other | Thai Purisa TrueType font                                     | 1:0.7.3-1                       |
| fonts-tlwg-sawasdee             | GPL-2+ GPL-2+ with Font exception LPPL other | Thai Sawasdee font (dependency package)                       | 1:0.7.3-1                       |
| fonts-tlwg-sawasdee-ttf         | GPL-2+ GPL-2+ with Font exception LPPL other | Thai Sawasdee TrueType font                                   | 1:0.7.3-1                       |
| fonts-tlwg-typewriter           | GPL-2+ GPL-2+ with Font exception LPPL other | Thai TlwgTypewriter font (dependency package)                 | 1:0.7.3-1                       |
| fonts-tlwg-typewriter-ttf       | GPL-2+ GPL-2+ with Font exception LPPL other | Thai TlwgTypewriter TrueType font                             | 1:0.7.3-1                       |
| fonts-tlwg-typist               | GPL-2+ GPL-2+ with Font exception LPPL other | Thai TlwgTypist font (dependency package)                     | 1:0.7.3-1                       |
| fonts-tlwg-typist-ttf           | GPL-2+ GPL-2+ with Font exception LPPL other | Thai TlwgTypist TrueType font                                 | 1:0.7.3-1                       |
| fonts-tlwg-typo                 | GPL-2+ GPL-2+ with Font exception LPPL other | Thai TlwgTypo font (dependency package)                       | 1:0.7.3-1                       |
| fonts-tlwg-typo-ttf             | GPL-2+ GPL-2+ with Font exception LPPL other | Thai TlwgTypo TrueType font                                   | 1:0.7.3-1                       |
| fonts-tlwg-umpush               | GPL-2+ GPL-2+ with Font exception LPPL other | Thai Umpush font (dependency package)                         | 1:0.7.3-1                       |
| fonts-tlwg-umpush-ttf           | GPL-2+ GPL-2+ with Font exception LPPL other | Thai Umpush TrueType font                                     | 1:0.7.3-1                       |
| fonts-tlwg-waree                | GPL-2+ GPL-2+ with Font exception LPPL other | Thai Waree font (dependency package)                          | 1:0.7.3-1                       |
| fonts-tlwg-waree-ttf            | GPL-2+ GPL-2+ with Font exception LPPL other | Thai Waree TrueType font                                      | 1:0.7.3-1                       |
| fonts-ubuntu                    | CC-BY-SA-3.0 GPL-3 Ubuntu-Font-License-1.0 | sans-serif font set from Ubuntu                               | 0.83-6ubuntu1                   |
| fonts-urw-base35                | AGPL-3 AGPL-3 with Font exception GPL-2+ | font set metric-compatible with the 35 PostScript Level 2 Ba  | 20200910-1                      |
| fonts-yrsa-rasa                 | GPL-2.0+ sil-1.1 | Open-source, libre fonts for Latin + Gujarati                 | 2.005-1                         |
| foomatic-db-compressed-ppds     | BSD~unspecified Expat FSFUL GPL-2 GPL-2+ GPL-2.0+OKI | OpenPrinting printer support - Compressed PPDs derived from   | 20220223-0ubuntu1               |
| fprintd                         | GPL-2+ | D-Bus daemon for fingerprint reader access                    | 1.94.2-1ubuntu0.22.04.1         |
| friendly-recovery               | GPL-2+ | Make recovery boot mode more user-friendly                    | 0.2.42                          |
| ftp                             | BSD-2-clause BSD-2-Clause BSD-3-clause BSD-3-Clause BSD-4-Clause Expat FSFAP FSFULLR GPL-2+ GPL-2+ with Autoconf-data exception GPL-2+ with libtool exception GPL-3+ with Autoconf-data exception ISC Unlicense | dummy transitional package for tnftp                          | 20210827-4build1                |
| fuse3                           | GPL-2 GPL-2+ LGPL-2.1 | Filesystem in Userspace (3.x version)                         | 3.10.5-1build1                  |
| fwupd                           | CC0-1.0 LGPL-2.1+ LGPL-2.1+ OR MIT MIT | Firmware update daemon                                        | 1.7.9-1~22.04.1                 |
| fwupd-signed                    | GPL-2.0+ GPL-3+ | Linux Firmware Updater EFI signed binary                      | 1.44+1.2-3                      |
| g++                             | GPL-2 | GNU C++ compiler                                              | 4:11.2.0-1ubuntu1               |
| g++-11                          | Artistic GFDL-1.2 GPL GPL-2 GPL-3 LGPL | GNU C++ compiler                                              | 11.3.0-1ubuntu1~22.04           |
| gamemode                        | BSD-3-clause FSFAP | Optimise Linux system performance on demand                   | 1.6.1-1build2                   |
| gamemode-daemon                 | BSD-3-clause FSFAP | Optimise Linux system performance on demand (daemon)          | 1.6.1-1build2                   |
| gcc                             | GPL-2 | GNU C compiler                                                | 4:11.2.0-1ubuntu1               |
| gcc-11                          | Artistic GFDL-1.2 GPL GPL-2 GPL-3 LGPL | GNU C compiler                                                | 11.3.0-1ubuntu1~22.04           |
| gcc-11-base:amd64               | Artistic GFDL-1.2 GPL GPL-2 GPL-3 LGPL | GCC, the GNU Compiler Collection (base package)               | 11.3.0-1ubuntu1~22.04           |
| gcc-12-base:amd64               | Artistic GFDL-1.2 GPL GPL-2 GPL-3 LGPL | GCC, the GNU Compiler Collection (base package)               | 12.1.0-2ubuntu1~22.04           |
| gcr                             | bzip2-1.0.5 LGPL-2.1+ | GNOME crypto services (daemon and tools)                      | 3.40.0-4                        |
| gdb                             | BSD-3-clause GPL-2+ GPL-3+ GPL-3+ with GCC-Runtime-3.1 exception LGPL-2+ LGPL-2.1+ | GNU Debugger                                                  | 12.1-0ubuntu1~22.04             |
| gdisk                           | LGPL-2.0+ | GPT fdisk text-mode partitioning tool                         | 1.0.8-4build1                   |
| gdm3                            | GPL-2 | GNOME Display Manager                                         | 42.0-1ubuntu7                   |
| gedit                           | GPL-2 | official text editor of the GNOME desktop environment         | 41.0-3                          |
| gedit-common                    | GPL-2 | official text editor of the GNOME desktop environment (suppo  | 41.0-3                          |
| geoclue-2.0                     | GFDL-NIV-1.1+ GPL-2+ LGPL-2+ | geoinformation service                                        | 2.5.7-3ubuntu3                  |
| gettext-base                    | GFDL GPL LGPL | GNU Internationalization utilities for the base system        | 0.21-4ubuntu4                   |
| ghostscript                     | Adobe-2006 AGPL-3 AGPL-3+ AGPL-3+ with font exception Apache-2.0 BSD-3-Clause BSD-3-Clause~Adobe custom Expat Expat~Ghostgum Expat~SunSoft Expat~SunSoft with SunSoft exception FTL GAP~configure GPL GPL-2+ GPL-3+ GPL-3+ with Autoconf exception ISC LGPL-2.1 MIT-Open-Group none NTP~Lucent NTP~WSU public-domain X11 ZLIB | interpreter for the PostScript language and for PDF           | 9.55.0~dfsg1-0ubuntu5.1         |
| ghostscript-x                   | Adobe-2006 AGPL-3 AGPL-3+ AGPL-3+ with font exception Apache-2.0 BSD-3-Clause BSD-3-Clause~Adobe custom Expat Expat~Ghostgum Expat~SunSoft Expat~SunSoft with SunSoft exception FTL GAP~configure GPL GPL-2+ GPL-3+ GPL-3+ with Autoconf exception ISC LGPL-2.1 MIT-Open-Group none NTP~Lucent NTP~WSU public-domain X11 ZLIB | interpreter for the PostScript language and for PDF - X11 su  | 9.55.0~dfsg1-0ubuntu5.1         |
| gir1.2-accountsservice-1.0:amd  | GPL-2+ GPL-3+ | GObject introspection data for AccountService                 | 22.07.5-2ubuntu1.3              |
| gir1.2-adw-1:amd64              | Apache-2.0 Apache-2.0 or GPL-3+ CC0-1.0 CC-BY-SA-4.0 GPL-3+ LGPL-2.1+ | GObject introspection files for libadwaita                    | 1.1.0-1ubuntu2                  |
| gir1.2-atk-1.0:amd64            | LGPL-2 | ATK accessibility toolkit (GObject introspection)             | 2.36.0-3build1                  |
| gir1.2-atspi-2.0:amd64          | AFL-2.1 AFL-2.1 or GPL-2+ GPL-2 GPL-2+ LGPL-2+ public-domain | Assistive Technology Service Provider (GObject introspection  | 2.44.0-3                        |
| gir1.2-dbusmenu-glib-0.4:amd64  | GPL-3 LGPL-2.1 LGPL-3 | typelib file for libdbusmenu-glib4                            | 16.04.1+18.10.20180917-0ubuntu  |
| gir1.2-dee-1.0                  | GPL-3 LGPL-3 | GObject introspection data for the Dee library                | 1.2.7+17.10.20170616-6ubuntu4   |
| gir1.2-freedesktop:amd64        | BSD-2-clause GPL-2+ LGPL-2+ MIT | Introspection data for some FreeDesktop components            | 1.72.0-1                        |
| gir1.2-gck-1:amd64              | bzip2-1.0.5 LGPL-2.1+ | GObject introspection data for the GCK library                | 3.40.0-4                        |
| gir1.2-gcr-3:amd64              | bzip2-1.0.5 LGPL-2.1+ | GObject introspection data for the GCR library                | 3.40.0-4                        |
| gir1.2-gdesktopenums-3.0:amd64  | LGPL-2.1+ | GObject introspection for GSettings desktop-wide schemas      | 42.0-1ubuntu1                   |
| gir1.2-gdkpixbuf-2.0:amd64      | CC0-1.0 GPL-2+ LGPL-2+ LGPL-2.1+ | GDK Pixbuf library - GObject-Introspection                    | 2.42.8+dfsg-1ubuntu0.1          |
| gir1.2-gdm-1.0:amd64            | GPL-2 | GObject introspection data for the GNOME Display Manager      | 42.0-1ubuntu7                   |
| gir1.2-geoclue-2.0:amd64        | GFDL-NIV-1.1+ GPL-2+ LGPL-2+ | convenience library to interact with geoinformation service   | 2.5.7-3ubuntu3                  |
| gir1.2-glib-2.0:amd64           | BSD-2-clause GPL-2+ LGPL-2+ MIT | Introspection data for GLib, GObject, Gio and GModule         | 1.72.0-1                        |
| gir1.2-gmenu-3.0:amd64          | GPL-2 | GObject introspection data for the GNOME menu library         | 3.36.0-1ubuntu3                 |
| gir1.2-gnomebluetooth-3.0:amd6  | GPL-2+ LGPL-2.1+ | Introspection data for GnomeBluetooth                         | 42.0-5                          |
| gir1.2-gnomedesktop-3.0:amd64   | Expat GPL-2+ LGPL-2+ LGPL-3+ | Introspection data for GnomeDesktop (GTK 3)                   | 42.4-0ubuntu1                   |
| gir1.2-graphene-1.0:amd64       | Expat | library of graphic data types (introspection files)           | 1.10.8-1                        |
| gir1.2-gstreamer-1.0:amd64      | LGPL | GObject introspection data for the GStreamer library          | 1.20.3-0ubuntu1                 |
| gir1.2-gtk-3.0:amd64            | Apache-2.0 check-gdk-cairo-permissive Expat LGPL-2+ LGPL-2.1+ LGPL-2+ or SWL other SWL X11R5-permissive ZPL-2.1 | GTK graphical user interface library -- gir bindings          | 3.24.33-1ubuntu2                |
| gir1.2-gtk-4.0:amd64            | Apache-2.0 Apache-2.0 or GPL-3.0+, Apache-2.0 with LLVM exception BSD-3-clause-Google CC0-1.0 CC0-1.0, CC-BY-SA-3.0 CC-BY-SA-3.0, Expat Expat or unlicense GPL-2.0+ GPL-2.0+, GPL-3.0+ lcs-telegraphics-permissive LGPL-2+ LGPL-2.1+ LGPL-2+ or MPL-1.1 MPL-1.1 sun-permissive Unicode-DFS-2016 unlicense X11R5-permissive ZPL-2.1 | GTK graphical user interface library -- gir bindings          | 4.6.6+ds-0ubuntu1               |
| gir1.2-gtksource-4:amd64        | LGPL-2.1 | gir files for the GTK+ syntax highlighting widget             | 4.8.3-1                         |
| gir1.2-gweather-3.0:amd64       | GPL-2+ LGPL-2.1+ | GObject introspection data for the GWeather library           | 40.0-5build1                    |
| gir1.2-handy-1:amd64            | LGPL-2.1+ | GObject introspection files for libhandy                      | 1.6.1-1                         |
| gir1.2-harfbuzz-0.0:amd64       | MIT | OpenType text shaping engine (GObject introspection data)     | 2.7.4-1ubuntu3.1                |
| gir1.2-ibus-1.0:amd64           | GPL-2.0+ with autoconf exception GPL-3.0+ with autoconf exception ISC-Fujitsu ISC-Intel ISC-NCR ISC-Sun LGPL-2.0+ LGPL-2.1+ MIT permissive permissive-author-grant-attribution permissive-autoconf-m4 permissive-fsf-grant permissive-fsf-grant-attribution permissive-makefile-in | Intelligent Input Bus - introspection data                    | 1.5.26-4                        |
| gir1.2-javascriptcoregtk-4.0:a  | AFL-2.0 AFL-2.0 or LGPL-2+ Apache-2.0 BSD-2-clause BSD-2-clause or BSL-1.0 BSD-2-clause or Expat BSD-3-clause-adam-barth BSD-3-clause-apple BSD-3-clause-canon BSD-3-clause-code-aurora BSD-3-clause-copyright-holder BSD-3-clause-ericsson BSD-3-clause-google BSD-3-clause-jochen-kalmbach BSD-3-clause-microsoft BSD-3-clause-motorola BSD-3-clause-opera BSD-valgrind BSL-1.0 Expat GPL-2+ GPL-2+ or LGPL-2.1+ or MPL-1.1 GPL-2+ with Bison exception GPL-3+ GPL-3+ with Bison exception ISC LGPL-2 LGPL-2+ LGPL-2.1 LGPL-2.1+ LGPL-2.1+ or MPL-1.1 LGPL-2+ or MPL-1.1 MPL-1.1 MPL-2.0 public-domain | JavaScript engine library from WebKitGTK - GObject introspec  | 2.36.8-0ubuntu0.22.04.1         |
| gir1.2-json-1.0:amd64           | LGPL-2.1+ | GLib JSON manipulation library (introspection data)           | 1.6.6-1build1                   |
| gir1.2-mutter-10:amd64          | DEC-BSD-variant Expat GPL-2+ GPL-3+ LGPL-2+ LGPL-2.1+ NTP-BSD-variant OpenGroup-BSD-variant SGI-B-2.0 WRF-BSD-variant | GObject introspection data for Mutter                         | 42.5-0ubuntu1                   |
| gir1.2-nm-1.0:amd64             | GFDL-NIV-1.1+ GPL-2+ LGPL-2.1+ | GObject introspection data for the libnm library              | 1.36.6-0ubuntu2                 |
| gir1.2-nma-1.0:amd64            | GPL-2+ LGPL-2+ LGPL-2.1+ | GObject introspection data for libnma                         | 1.8.34-1ubuntu1                 |
| gir1.2-notify-0.7:amd64         | LGPL-2.1 | sends desktop notifications to a notification daemon (Intros  | 0.7.9-3ubuntu5.22.04.1          |
| gir1.2-packagekitglib-1.0       | GPL-2+ LGPL-2.1+ | GObject introspection data for the PackageKit GLib library    | 1.2.5-2ubuntu2                  |
| gir1.2-pango-1.0:amd64          | Apache-2 Apache-2.0 Apache-2.0 or GPL-3.0+, Bitstream-Vera CC0-1.0 CC0-1.0, CC-BY-SA-3.0 CC-BY-SA-3.0, Chromium-BSD-style Example Expat GPL-2+ GPL-2+, GPL-3.0+ ICU LGPL-2+ LGPL-2+, LGPL-2.1+ LGPL-2+ or MPL-1.1 MPL-1.1 OFL-1.1 TCL Unicode | Layout and rendering of internationalized text - gir binding  | 1.50.6+ds-2                     |
| gir1.2-peas-1.0:amd64           | GPL-3 LGPL-2 | Application plugin library (introspection files)              | 1.32.0-1                        |
| gir1.2-polkit-1.0               | Apache-2.0 LGPL-2.0+ | GObject introspection data for PolicyKit                      | 0.105-33                        |
| gir1.2-rsvg-2.0:amd64           | 0BSD Apache-2.0 Apache-2.0, Apache-2.0 or Boost-1.0, Apache-2.0 or Expat, Apache-2.0 or Expat or 0BSD, Boost-1.0 BSD-2-clause BSD-2-clause, BSD-3-clause BSD-3-clause, CC-BY-3.0 CC-zero-waive-1.0-us Expat Expat, Expat or Unlicense, FSFAP LGPL-2+ MPL-2.0 MPL-2.0, OFL-1.1 Sun-permissive Sun-permissive, Unlicense zlib | gir files for renderer library for SVG files                  | 2.52.5+dfsg-3                   |
| gir1.2-secret-1:amd64           | Apache-2.0 BSD-3-clause GPL-2+ LGPL-2.1+ | Secret store (GObject-Introspection)                          | 0.20.5-2                        |
| gir1.2-soup-2.4:amd64           | Expat LGPL-2+ LGPL-2.1+ | GObject introspection data for the libsoup HTTP library       | 2.74.2-3                        |
| gir1.2-unity-7.0:amd64          | GPL-3 LGPL-3 | GObject introspection data for the Unity library              | 7.1.4+19.04.20190319-6build1    |
| gir1.2-upowerglib-1.0:amd64     | GFDL-1.1+ GPL-2+ | GObject introspection data for upower                         | 0.99.17-1                       |
| gir1.2-vte-2.91:amd64           | Expat GPL-3+ LGPL-3+ | GObject introspection data for the VTE library                | 0.68.0-1                        |
| gir1.2-webkit2-4.0:amd64        | AFL-2.0 AFL-2.0 or LGPL-2+ Apache-2.0 BSD-2-clause BSD-2-clause or BSL-1.0 BSD-2-clause or Expat BSD-3-clause-adam-barth BSD-3-clause-apple BSD-3-clause-canon BSD-3-clause-code-aurora BSD-3-clause-copyright-holder BSD-3-clause-ericsson BSD-3-clause-google BSD-3-clause-jochen-kalmbach BSD-3-clause-microsoft BSD-3-clause-motorola BSD-3-clause-opera BSD-valgrind BSL-1.0 Expat GPL-2+ GPL-2+ or LGPL-2.1+ or MPL-1.1 GPL-2+ with Bison exception GPL-3+ GPL-3+ with Bison exception ISC LGPL-2 LGPL-2+ LGPL-2.1 LGPL-2.1+ LGPL-2.1+ or MPL-1.1 LGPL-2+ or MPL-1.1 MPL-1.1 MPL-2.0 public-domain | Web content engine library for GTK - GObject introspection d  | 2.36.8-0ubuntu0.22.04.1         |
| gir1.2-wnck-3.0:amd64           | LGPL-2+ | GObject introspection data for the WNCK library               | 40.1-1                          |
| git                             | Apache-2.0 Boost dlmalloc EDL-1.0 Expat GPL-1+ or Artistic-1 GPL-2 GPL-2+ ISC LGPL-2+ LGPL-2.1+ mingw-runtime | fast, scalable, distributed revision control system           | 1:2.34.1-1ubuntu1.5             |
| git-man                         | Apache-2.0 Boost dlmalloc EDL-1.0 Expat GPL-1+ or Artistic-1 GPL-2 GPL-2+ ISC LGPL-2+ LGPL-2.1+ mingw-runtime | fast, scalable, distributed revision control system (manual   | 1:2.34.1-1ubuntu1.5             |
| gjs                             | BSD-3-clause CC0-1.0 Expat Expat or LGPL-2.0+ LGPL-2.0+ LGPL-2.1+ MPL-1.1 or GPL-2.0+ or LGPL-2.1+ MPL-2.0 | Mozilla-based javascript bindings for the GNOME platform (cl  | 1.72.2-0ubuntu1                 |
| gkbd-capplet                    | LGPL-2+ | GNOME control center tools for libgnomekbd                    | 3.26.1-2                        |
| glib-networking:amd64           | LGPL-2+ LGPL-2.1+ | network-related giomodules for GLib                           | 2.72.0-1                        |
| glib-networking-common          | LGPL-2+ LGPL-2.1+ | network-related giomodules for GLib - data files              | 2.72.0-1                        |
| glib-networking-services        | LGPL-2+ LGPL-2.1+ | network-related giomodules for GLib - D-Bus services          | 2.72.0-1                        |
| gnome-accessibility-themes      | LGPL-2.1+ | High Contrast GTK+ 2 theme and icons                          | 3.28-1ubuntu3                   |
| gnome-bluetooth-3-common        | GPL-2+ LGPL-2.1+ | GNOME Bluetooth 3 common files                                | 42.0-5                          |
| gnome-bluetooth-common          | GPL-2+ LGPL-2.1+ | GNOME Bluetooth common files                                  | 3.34.5-8                        |
| gnome-calculator                | CC-BY-SA-3.0 GPL-2+ | GNOME desktop calculator                                      | 1:41.1-2ubuntu2                 |
| gnome-characters                | BSD-3-clause GPL-2+ | character map application                                     | 41.0-4                          |
| gnome-control-center            | CC0-1.0 CC-BY-SA-4.0 Expat GPL-2+ GPL-3+ LGPL-2+ LGPL-2.1+ tzdb-public-domain | utilities to configure the GNOME desktop                      | 1:41.7-0ubuntu0.22.04.5         |
| gnome-control-center-data       | CC0-1.0 CC-BY-SA-4.0 Expat GPL-2+ GPL-3+ LGPL-2+ LGPL-2.1+ tzdb-public-domain | configuration applets for GNOME - data files                  | 1:41.7-0ubuntu0.22.04.5         |
| gnome-control-center-faces      | CC0-1.0 CC-BY-SA-4.0 Expat GPL-2+ GPL-3+ LGPL-2+ LGPL-2.1+ tzdb-public-domain | utilities to configure the GNOME desktop - faces images       | 1:41.7-0ubuntu0.22.04.5         |
| gnome-desktop3-data             | Expat GPL-2+ LGPL-2+ LGPL-3+ | Common files for GNOME desktop apps                           | 42.4-0ubuntu1                   |
| gnome-disk-utility              | CC-BY-SA-3.0 CC-BY-SA-3.0 or LGPL-3 GPL-2+ LGPL-3 | manage and configure disk drives and media                    | 42.0-1ubuntu1                   |
| gnome-font-viewer               | GPL-2+ | font viewer for GNOME                                         | 41.0-2                          |
| gnome-keyring                   | BSD-3-clause custom-license Expat FSFAP FSFUL FSFULLR FSFULLR or GPL-2+ with AutoConf exception FSFULLR or GPL-2+ with LibTool exception GPL-2+ GPL-2+ or LGPL-3+ GPL-2+ with AutoConf exception GPL-2+ with LibTool exception GPL-3+ with AutoConf exception LGPL-2+ LGPL-2.1+ LGPL-3+ MPL-1.1 MPL-1.1 or GPL-2+ or LGPL-2.1+ | GNOME keyring services (daemon and tools)                     | 40.0-3ubuntu3                   |
| gnome-keyring-pkcs11:amd64      | BSD-3-clause custom-license Expat FSFAP FSFUL FSFULLR FSFULLR or GPL-2+ with AutoConf exception FSFULLR or GPL-2+ with LibTool exception GPL-2+ GPL-2+ or LGPL-3+ GPL-2+ with AutoConf exception GPL-2+ with LibTool exception GPL-3+ with AutoConf exception LGPL-2+ LGPL-2.1+ LGPL-3+ MPL-1.1 MPL-1.1 or GPL-2+ or LGPL-2.1+ | GNOME keyring module for the PKCS#11 module loading library   | 40.0-3ubuntu3                   |
| gnome-logs                      | CC0 CC-BY-SA GPL-2+ GPL-2+ or CC-BY-SA GPL-3+ | viewer for the systemd journal                                | 42.0-1                          |
| gnome-menus                     | GPL-2 | GNOME implementation of the freedesktop menu specification    | 3.36.0-1ubuntu3                 |
| gnome-online-accounts           | LGPL-2.1+ | service to manage online accounts for the GNOME desktop       | 3.44.0-1ubuntu1                 |
| gnome-remote-desktop            | GPL-2+ | Remote desktop daemon for GNOME using PipeWire                | 42.4-0ubuntu1                   |
| gnome-session-bin               | GPL-2 | GNOME Session Manager - Minimal runtime                       | 42.0-1ubuntu2                   |
| gnome-session-canberra          | LGPL-2 LGPL-2.1 | GNOME session log in and log out sound events                 | 0.30-10ubuntu1                  |
| gnome-session-common            | GPL-2 | GNOME Session Manager - common files                          | 42.0-1ubuntu2                   |
| gnome-settings-daemon           | GPL LGPL | daemon handling the GNOME session settings                    | 42.1-1ubuntu2.1                 |
| gnome-settings-daemon-common    | GPL LGPL | daemon handling the GNOME session settings - common files     | 42.1-1ubuntu2.1                 |
| gnome-shell                     | GPL-2 GPL-3 LGPL-2 LGPL-2.1 | graphical shell for the GNOME desktop                         | 42.2-0ubuntu0.2                 |
| gnome-shell-common              | GPL-2 GPL-3 LGPL-2 LGPL-2.1 | common files for the GNOME graphical shell                    | 42.2-0ubuntu0.2                 |
| gnome-shell-extension-appindic  | GPL-2+ | AppIndicator, KStatusNotifierItem and tray support for GNOME  | 42-2~fakesync1                  |
| gnome-shell-extension-desktop-  | GPL-3 | desktop icon support for GNOME Shell                          | 43-2ubuntu1                     |
| gnome-shell-extension-ubuntu-d  | GPL-2+ | Ubuntu Dock for GNOME Shell                                   | 72~ubuntu5.22.04.1              |
| gnome-startup-applications      | GPL-2 | Startup Applications manager for GNOME                        | 42.0-1ubuntu2                   |
| gnome-system-monitor            | CC-BY-SA-4.0 GPL-2+ LGPL-2+ | Process viewer and system resource monitor for GNOME          | 42.0-1                          |
| gnome-terminal                  | GFDL-NIV-1.3 GFDL-NIV-1.3 or GPL-3+ GPL-3+ LGPL-3+ | GNOME terminal emulator application                           | 3.44.0-1ubuntu1                 |
| gnome-terminal-data             | GFDL-NIV-1.3 GFDL-NIV-1.3 or GPL-3+ GPL-3+ LGPL-3+ | Data files for the GNOME terminal emulator                    | 3.44.0-1ubuntu1                 |
| gnome-themes-extra:amd64        | PL-2.1+ | Adwaita GTK+ 2 theme — engine                              LG | 3.28-1ubuntu3                   |
| gnome-themes-extra-data         | PL-2.1+ | Adwaita GTK+ 2 theme — common files                        LG | 3.28-1ubuntu3                   |
| gnome-user-docs                 | CC-BY-SA-3.0 | GNOME Help                                                    | 41.5-1ubuntu2                   |
| gnupg                           | BSD-3-clause CC0-1.0 Expat GPL-3+ GPL-3+ or BSD-3-clause LGPL-2.1+ LGPL-3+ permissive RFC-Reference TinySCHEME | GNU privacy guard - a free PGP replacement                    | 2.2.27-3ubuntu2.1               |
| gnupg-l10n                      | BSD-3-clause CC0-1.0 Expat GPL-3+ GPL-3+ or BSD-3-clause LGPL-2.1+ LGPL-3+ permissive RFC-Reference TinySCHEME | GNU privacy guard - localization files                        | 2.2.27-3ubuntu2.1               |
| gnupg-utils                     | BSD-3-clause CC0-1.0 Expat GPL-3+ GPL-3+ or BSD-3-clause LGPL-2.1+ LGPL-3+ permissive RFC-Reference TinySCHEME | GNU privacy guard - utility programs                          | 2.2.27-3ubuntu2.1               |
| gpg                             | BSD-3-clause CC0-1.0 Expat GPL-3+ GPL-3+ or BSD-3-clause LGPL-2.1+ LGPL-3+ permissive RFC-Reference TinySCHEME | GNU Privacy Guard -- minimalist public key operations         | 2.2.27-3ubuntu2.1               |
| gpg-agent                       | BSD-3-clause CC0-1.0 Expat GPL-3+ GPL-3+ or BSD-3-clause LGPL-2.1+ LGPL-3+ permissive RFC-Reference TinySCHEME | GNU privacy guard - cryptographic agent                       | 2.2.27-3ubuntu2.1               |
| gpg-wks-client                  | BSD-3-clause CC0-1.0 Expat GPL-3+ GPL-3+ or BSD-3-clause LGPL-2.1+ LGPL-3+ permissive RFC-Reference TinySCHEME | GNU privacy guard - Web Key Service client                    | 2.2.27-3ubuntu2.1               |
| gpg-wks-server                  | BSD-3-clause CC0-1.0 Expat GPL-3+ GPL-3+ or BSD-3-clause LGPL-2.1+ LGPL-3+ permissive RFC-Reference TinySCHEME | GNU privacy guard - Web Key Service server                    | 2.2.27-3ubuntu2.1               |
| gpgconf                         | BSD-3-clause CC0-1.0 Expat GPL-3+ GPL-3+ or BSD-3-clause LGPL-2.1+ LGPL-3+ permissive RFC-Reference TinySCHEME | GNU privacy guard - core configuration utilities              | 2.2.27-3ubuntu2.1               |
| gpgsm                           | BSD-3-clause CC0-1.0 Expat GPL-3+ GPL-3+ or BSD-3-clause LGPL-2.1+ LGPL-3+ permissive RFC-Reference TinySCHEME | GNU privacy guard - S/MIME version                            | 2.2.27-3ubuntu2.1               |
| gpgv                            | BSD-3-clause CC0-1.0 Expat GPL-3+ GPL-3+ or BSD-3-clause LGPL-2.1+ LGPL-3+ permissive RFC-Reference TinySCHEME | GNU privacy guard - signature verification tool               | 2.2.27-3ubuntu2.1               |
| grep                            | GPL-3+ | GNU grep, egrep and fgrep                                     | 3.7-1build1                     |
| groff-base                      | GFDL-1.3 GPL-3 | GNU troff text-formatting system (base system components)     | 1.22.4-8build1                  |
| grub-common                     | CC-BY-SA-3.0 Expat GPL-3+ | GRand Unified Bootloader (common files)                       | 2.06-2ubuntu7                   |
| grub-efi-amd64-bin              | CC-BY-SA-3.0 Expat GPL-3+ | GRand Unified Bootloader, version 2 (EFI-AMD64 modules)       | 2.06-2ubuntu10                  |
| grub-efi-amd64-signed           | GPL-3+ | GRand Unified Bootloader, version 2 (EFI-AMD64 version, sign  | 1.182~22.04.1+2.06-2ubuntu10    |
| grub-gfxpayload-lists           | GPL-3 | GRUB gfxpayload blacklist                                     | 0.7                             |
| grub-pc                         | CC-BY-SA-3.0 Expat GPL-3+ | GRand Unified Bootloader, version 2 (PC/BIOS version)         | 2.06-2ubuntu7                   |
| grub-pc-bin                     | CC-BY-SA-3.0 Expat GPL-3+ | GRand Unified Bootloader, version 2 (PC/BIOS modules)         | 2.06-2ubuntu7                   |
| grub2-common                    | CC-BY-SA-3.0 Expat GPL-3+ | GRand Unified Bootloader (common files for version 2)         | 2.06-2ubuntu7                   |
| gsettings-desktop-schemas       | LGPL-2.1+ | GSettings desktop-wide schemas                                | 42.0-1ubuntu1                   |
| gsettings-ubuntu-schemas        | LGPL-2.1+ | GSettings deskop-wide schemas for Ubuntu                      | 0.0.7+21.10.20210712-0ubuntu2   |
| gsfonts                         | GPL | Fonts for the Ghostscript interpreter(s)                      | 1:8.11+urwcyr1.0.7~pre44-4.5    |
| gstreamer1.0-alsa:amd64         | LGPL | GStreamer plugin for ALSA                                     | 1.20.1-1                        |
| gstreamer1.0-clutter-3.0:amd64  | LGPL-2 | Clutter PLugin for GStreamer 1.0                              | 3.0.27-2ubuntu1                 |
| gstreamer1.0-gl:amd64           | LGPL | GStreamer plugins for GL                                      | 1.20.1-1                        |
| gstreamer1.0-packagekit         | GPL-2+ LGPL-2.1+ | GStreamer plugin to install codecs using PackageKit           | 1.2.5-2ubuntu2                  |
| gstreamer1.0-pipewire:amd64     | Expat LGPL-2+ LGPL-2.1+ | GStreamer 1.0 plugin for the PipeWire multimedia server       | 0.3.48-1ubuntu2                 |
| gstreamer1.0-plugins-base:amd6  | LGPL | GStreamer plugins from the "base" set                         | 1.20.1-1                        |
| gstreamer1.0-plugins-base-apps  | LGPL | GStreamer helper programs from the "base" set                 | 1.20.1-1                        |
| gstreamer1.0-plugins-good:amd6  | LGPL | GStreamer plugins from the "good" set                         | 1.20.3-0ubuntu1                 |
| gstreamer1.0-pulseaudio:amd64   | LGPL | GStreamer plugin for PulseAudio (transitional package)        | 1.20.3-0ubuntu1                 |
| gstreamer1.0-tools              | LGPL | Tools for use with GStreamer                                  | 1.20.3-0ubuntu1                 |
| gstreamer1.0-x:amd64            | LGPL | GStreamer plugins for X11 and Pango                           | 1.20.1-1                        |
| gtk-update-icon-cache           | Apache-2.0 check-gdk-cairo-permissive Expat LGPL-2+ LGPL-2.1+ LGPL-2+ or SWL other SWL X11R5-permissive ZPL-2.1 | icon theme caching utility                                    | 3.24.33-1ubuntu2                |
| gtk2-engines-murrine:amd64      | LGPL-2+ | cairo-based gtk+-2.0 theme engine                             | 0.98.2-3build2                  |
| gtk2-engines-pixbuf:amd64       | LGPL-2 | pixbuf-based theme for GTK 2                                  | 2.24.33-2ubuntu2                |
| gvfs:amd64                      | GPL-2+ GPL-3+ LGPL-2+ | userspace virtual filesystem - GIO module                     | 1.48.2-0ubuntu1                 |
| gvfs-backends                   | GPL-2+ GPL-3+ LGPL-2+ | userspace virtual filesystem - backends                       | 1.48.2-0ubuntu1                 |
| gvfs-common                     | GPL-2+ GPL-3+ LGPL-2+ | userspace virtual filesystem - common data files              | 1.48.2-0ubuntu1                 |
| gvfs-daemons                    | GPL-2+ GPL-3+ LGPL-2+ | userspace virtual filesystem - servers                        | 1.48.2-0ubuntu1                 |
| gvfs-fuse                       | GPL-2+ GPL-3+ LGPL-2+ | userspace virtual filesystem - fuse server                    | 1.48.2-0ubuntu1                 |
| gvfs-libs:amd64                 | GPL-2+ GPL-3+ LGPL-2+ | userspace virtual filesystem - private libraries              | 1.48.2-0ubuntu1                 |
| gzip                            | FSF-manpages GFDL-1.3+-no-invariant GPL-3+ | GNU compression utilities                                     | 1.10-4ubuntu4.1                 |
| hdparm                          | BSD-2-clause GPL-2+ GPL-2+ or BSD-2-clause hdparm | tune hard disk parameters for high performance                | 9.60+ds-1build3                 |
| hicolor-icon-theme              | GPL-2+ | default fallback theme for FreeDesktop.org icon themes        | 0.17-2                          |
| hostname                        | GPL-2 | utility to set/show the host name or domain name              | 3.23ubuntu2                     |
| htop                            | GPL-2+ | interactive processes viewer                                  | 3.0.5-7build2                   |
| humanity-icon-theme             | GPL-2 GPL-3 LGPL-3 | Humanity Icon theme                                           | 0.6.16                          |
| hunspell-en-us                  | public domain Public Domain | English_american dictionary for hunspell                      | 1:2020.12.07-2                  |
| hyphen-de                       | AGPL-3+ Apache-2.0 BSD-2-clause BSD-2-clause or CC-BY-3.0 BSD-3-clause CC0-1.0 CC-BY-3.0 CC-BY-4.0 CC-BY-SA-3.0 CC-SA-1.0 custom0 custom1 custom2 custom-bsd custom-bsd-4-clauses expat GFDL-1.1+ GFDL-1.2+ GPL GPL-2 GPL-2+ GPL-2 or GPL-3 GPL-2 or LGPL-2.1 or MPL-1.1 GPL-2+ or LGPL-2.1 or MPL-1.1 GPL-2+ or LGPL-2.1+ or MPL-1.1 GPL-3 GPL-3+ GPL-3+ or LGPL-3+ GPL-3 or LGPL-3 or MPL-1.1 GPL-3+ or LGPL-3+ or MPL-1.1+ GPL-3+ or LGPL-3+ or MPL-2.0+ GPL or LGPL GPL or LGPL or MPL or Apache-2.0 or CC-SA-1.0 LGPL LGPL-2+ LGPL-2.1 LGPL-2.1+ LGPL-2.1 or GPL-2 or MPL-1.1 LGPL-3 LGPL-3+ LGPL-3+ or GPL-3+ LGPL-3 or MPL LGPL or SISSL LPPL-1.3 MPL-1.1 MPL-1.1+ MPL-1.1 or GPL-2 or LGPL-2.1 MPL-1.1 or GPL-3+ or LGPL-3+ MPL-2.0 MPL-2.0+ MPL or LGPL-3+ other SISSL | German hyphenation patterns                                   | 1:7.2.0-2                       |
| hyphen-en-us                    | GPL-2+ GPL-2+ or LGPL-2.1+ or MPL-1.1+ LGPL-2.1+ MPL-1.1+ | English (US) hyphenation patterns                             | 2.8.8-7build2                   |
| i3                              | BSD-3-clause | metapackage (i3 window manager, screen locker, menu, statusb  | 4.20.1-1                        |
| i3-wm                           | BSD-3-clause | improved dynamic tiling window manager                        | 4.20.1-1                        |
| i3lock                          | BSD-3-clause | improved screen locker                                        | 2.13-1                          |
| i3status                        | BSD-3-clause | Generates a status line for dzen2, xmobar or i3bar            | 2.13-3                          |
| ibus                            | GPL-2.0+ with autoconf exception GPL-3.0+ with autoconf exception ISC-Fujitsu ISC-Intel ISC-NCR ISC-Sun LGPL-2.0+ LGPL-2.1+ MIT permissive permissive-author-grant-attribution permissive-autoconf-m4 permissive-fsf-grant permissive-fsf-grant-attribution permissive-makefile-in | Intelligent Input Bus - core                                  | 1.5.26-4                        |
| ibus-data                       | GPL-2.0+ with autoconf exception GPL-3.0+ with autoconf exception ISC-Fujitsu ISC-Intel ISC-NCR ISC-Sun LGPL-2.0+ LGPL-2.1+ MIT permissive permissive-author-grant-attribution permissive-autoconf-m4 permissive-fsf-grant permissive-fsf-grant-attribution permissive-makefile-in | Intelligent Input Bus - data files                            | 1.5.26-4                        |
| ibus-gtk:amd64                  | GPL-2.0+ with autoconf exception GPL-3.0+ with autoconf exception ISC-Fujitsu ISC-Intel ISC-NCR ISC-Sun LGPL-2.0+ LGPL-2.1+ MIT permissive permissive-author-grant-attribution permissive-autoconf-m4 permissive-fsf-grant permissive-fsf-grant-attribution permissive-makefile-in | Intelligent Input Bus - GTK2 support                          | 1.5.26-4                        |
| ibus-gtk3:amd64                 | GPL-2.0+ with autoconf exception GPL-3.0+ with autoconf exception ISC-Fujitsu ISC-Intel ISC-NCR ISC-Sun LGPL-2.0+ LGPL-2.1+ MIT permissive permissive-author-grant-attribution permissive-autoconf-m4 permissive-fsf-grant permissive-fsf-grant-attribution permissive-makefile-in | Intelligent Input Bus - GTK3 support                          | 1.5.26-4                        |
| ibus-gtk4:amd64                 | GPL-2.0+ with autoconf exception GPL-3.0+ with autoconf exception ISC-Fujitsu ISC-Intel ISC-NCR ISC-Sun LGPL-2.0+ LGPL-2.1+ MIT permissive permissive-author-grant-attribution permissive-autoconf-m4 permissive-fsf-grant permissive-fsf-grant-attribution permissive-makefile-in | Intelligent Input Bus - GTK4 support                          | 1.5.26-4                        |
| ibus-table                      | CC0-1.0 LGPL LGPL-2.1 | table engine for IBus                                         | 1.16.7-1                        |
| ieee-data                       | GPL-2+ other WTFPL-2 | OUI and IAB listings                                          | 20210605.1                      |
| iftop                           | BSD-2-clause BSD-3-clause BSD-like GPL-2+ | displays bandwidth usage information on an network interface  | 1.0~pre4-7                      |
| iio-sensor-proxy                | GFDL-1.1-or-later GPL-2 GPL-2+ | IIO sensors to D-Bus proxy                                    | 3.3-0ubuntu6                    |
| im-config                       | GPL-2+ | Input method configuration framework                          | 0.50-2                          |
| imagemagick                     | aclocal BSD-with-FSF-change-public-domain GNU-All-Permissive-License GPL-2+ GPL2+-with-Autoconf-Macros-exception GPL3+-with-Autoconf-Macros-exception GPL3+-with-Autoconf-Macros-exception-GNU Imagemagick ImageMagick ImageMagickLicensePartEZXML ImageMagickLicensePartFIG ImageMagickLicensePartGsview ImageMagickLicensePartOpenSSH ImageMagickPartGraphicsMagick ImageMagickPartlibjpeg ImageMagickPartlibsquish LGPL-3+ Magick++ Makefile-in Perllikelicense TatcherUlrichPublicDomain | image manipulation programs -- binaries                       | 8:6.9.11.60+dfsg-1.3build2      |
| imagemagick-6-common            | aclocal BSD-with-FSF-change-public-domain GNU-All-Permissive-License GPL-2+ GPL2+-with-Autoconf-Macros-exception GPL3+-with-Autoconf-Macros-exception GPL3+-with-Autoconf-Macros-exception-GNU Imagemagick ImageMagick ImageMagickLicensePartEZXML ImageMagickLicensePartFIG ImageMagickLicensePartGsview ImageMagickLicensePartOpenSSH ImageMagickPartGraphicsMagick ImageMagickPartlibjpeg ImageMagickPartlibsquish LGPL-3+ Magick++ Makefile-in Perllikelicense TatcherUlrichPublicDomain | image manipulation programs -- infrastructure                 | 8:6.9.11.60+dfsg-1.3build2      |
| imagemagick-6.q16               | aclocal BSD-with-FSF-change-public-domain GNU-All-Permissive-License GPL-2+ GPL2+-with-Autoconf-Macros-exception GPL3+-with-Autoconf-Macros-exception GPL3+-with-Autoconf-Macros-exception-GNU Imagemagick ImageMagick ImageMagickLicensePartEZXML ImageMagickLicensePartFIG ImageMagickLicensePartGsview ImageMagickLicensePartOpenSSH ImageMagickPartGraphicsMagick ImageMagickPartlibjpeg ImageMagickPartlibsquish LGPL-3+ Magick++ Makefile-in Perllikelicense TatcherUlrichPublicDomain | image manipulation programs -- quantum depth Q16              | 8:6.9.11.60+dfsg-1.3build2      |
| info                            | GFDL-1.3 GPL-3 | Standalone GNU Info documentation browser                     | 6.8-4build1                     |
| init                            | BSD-3-clause GPL-2+ | metapackage ensuring an init system is installed              | 1.62                            |
| init-system-helpers             | BSD-3-clause GPL-2+ | helper tools for all init systems                             | 1.62                            |
| initramfs-tools                 | GPL-2 | generic modular initramfs generator (automation)              | 0.140ubuntu13                   |
| initramfs-tools-bin             | GPL-2 | binaries used by initramfs-tools                              | 0.140ubuntu13                   |
| initramfs-tools-core            | GPL-2 | generic modular initramfs generator (core tools)              | 0.140ubuntu13                   |
| inputattach                     | GPL-2+ public-domain | utility to connect serial-attached peripherals to the input   | 1:1.7.1-1build2                 |
| install-info                    | GFDL-1.3 GPL-3 | Manage installed documentation in info format                 | 6.8-4build1                     |
| ipp-usb                         | BSD-2-clause | Daemon for IPP over USB printer support                       | 0.9.20-1                        |
| iproute2                        | GPL-2 | networking and traffic control tools                          | 5.15.0-1ubuntu2                 |
| iptables                        | Artistic custom GPL-2 GPL-2+ | administration tools for packet filtering and NAT             | 1.8.7-1ubuntu5                  |
| iputils-ping                    | GPL | Tools to test the reachability of network hosts               | 3:20211215-1                    |
| iputils-tracepath               | GPL | Tools to trace the network path to a remote host              | 3:20211215-1                    |
| irqbalance                      | GPL-2 | Daemon to balance interrupts for SMP systems                  | 1.8.0-1build1                   |
| isc-dhcp-client                 | GPL-2 MPL-2.0 | DHCP client for automatically obtaining an IP address         | 4.4.1-2.3ubuntu2.3              |
| isc-dhcp-common                 | GPL-2 MPL-2.0 | common manpages relevant to all of the isc-dhcp packages      | 4.4.1-2.3ubuntu2.3              |
| iso-codes                       | LGPL-2.1+ | ISO language, territory, currency, script codes and their tr  | 4.9.0-1                         |
| java-common                     | GPL-2+ | Base package for Java runtimes                                | 0.72build2                      |
| jp2a                            | GPL-2 GPL-2+ | converts jpg and png images to ascii                          | 1.1.1-1                         |
| jq                              | CC-BY-3.0 Expat GPL-2.0+ MIT | lightweight and flexible command-line JSON processor          | 1.6-2.1ubuntu3                  |
| kbd                             | GPL-2+ GPL-2-with-exceptions GPL-any | Linux console font and keytable utilities                     | 2.3.0-3ubuntu4                  |
| kerneloops                      | GPL-2 | kernel oops tracker                                           | 0.12+git20140509-6ubuntu5       |
| keyboard-configuration          | GPL-2 | system-wide keyboard preferences                              | 1.205ubuntu3                    |
| klibc-utils                     | GPL-2 | small utilities built with klibc for early boot               | 2.0.10-4                        |
| kmod                            | GPL-2 LGPL-2.1 | tools for managing Linux kernel modules                       | 29-1ubuntu1                     |
| language-pack-de                | GPL | translation updates for language German                       | 1:22.04+20220721                |
| language-pack-de-base           | GPL | translations for language German                              | 1:22.04+20220721                |
| language-pack-en                | GPL | translation updates for language English                      | 1:22.04+20220721                |
| language-pack-en-base           | GPL | translations for language English                             | 1:22.04+20220721                |
| language-pack-gnome-de          | GPL | GNOME translation updates for language German                 | 1:22.04+20220721                |
| language-pack-gnome-de-base     | GPL | GNOME translations for language German                        | 1:22.04+20220721                |
| language-pack-gnome-en          | GPL | GNOME translation updates for language English                | 1:22.04+20220721                |
| language-pack-gnome-en-base     | GPL | GNOME translations for language English                       | 1:22.04+20220721                |
| language-selector-common        | GPL-2+ | Language selector for Ubuntu                                  | 0.219                           |
| language-selector-gnome         | GPL-2+ | Language selector frontend for Ubuntu                         | 0.219                           |
| laptop-detect                   | BSD-3-Clause | system chassis type checker                                   | 0.16                            |
| less                            | GPL-3 | pager program similar to more                                 | 590-1build1                     |
| libaa1:amd64                    | Abstyles Custom LGPL-2.0+ MIT | ASCII art library                                             | 1.4p5-50build1                  |
| libabsl20210324:amd64           | Apache-2.0 | extensions to the C++ standard library                        | 0~20210324.2-2                  |
| libaccountsservice0:amd64       | GPL-2+ GPL-3+ | query and manipulate user account information - shared libra  | 22.07.5-2ubuntu1.3              |
| libacl1:amd64                   | GPL-2+ LGPL-2+ | access control list - shared library                          | 2.3.1-1                         |
| libadwaita-1-0:amd64            | Apache-2.0 Apache-2.0 or GPL-3+ CC0-1.0 CC-BY-SA-4.0 GPL-3+ LGPL-2.1+ | Library with GTK widgets for mobile phones                    | 1.1.0-1ubuntu2                  |
| libalgorithm-diff-perl          | Artistic Artistic or GPL-1+ GPL-1+ | module to find differences between files                      | 1.201-1                         |
| libalgorithm-diff-xs-perl       | Artistic Artistic or GPL-1+ GPL-1+ | module to find differences between files (XS accelerated)     | 0.04-6build3                    |
| libalgorithm-merge-perl         | Artistic Artistic or GPL-1+ GPL-1+ | Perl module for three-way merge of textual data               | 0.08-3                          |
| libanyevent-i3-perl             | Artistic Artistic or GPL-1+ GPL-1+ | Perl module to communicate with the i3 window manager         | 0.17-1                          |
| libanyevent-perl                | Artistic Artistic or GPL-1+ GPL-1+ | event loop framework with multiple implementations            | 7.170-2build2                   |
| libao-common                    | GPL | Cross Platform Audio Output Library (Common files)            | 1.2.2+20180113-1.1ubuntu3       |
| libao4:amd64                    | GPL | Cross Platform Audio Output Library                           | 1.2.2+20180113-1.1ubuntu3       |
| libaom3:amd64                   | BSD-2-clause BSD-2-Clause BSD-3-clause Expat ISC public-domain-md5 | AV1 Video Codec Library                                       | 3.3.0-1                         |
| libapparmor1:amd64              | BSD-3-clause BSD-3-clause or GPL-2+ GPL-2 GPL-2+ LGPL-2.1+ | changehat AppArmor library                                    | 3.0.4-2ubuntu2.1                |
| libappstream4:amd64             | FSFAP GPL-2+ LGPL-2.1+ SIL-1.1 | Library to access AppStream services                          | 0.15.2-2                        |
| libapt-pkg6.0:amd64             | GPL-2 | package management runtime library                            | 2.4.8                           |
| libarchive13:amd64              | Apache-2.0 BSD-124-clause-UCB BSD-1-clause-UCB BSD-2-clause BSD-3-clause-UCB BSD-4-clause-UCB Expat PD | Multi-format archive and compression library (shared library  | 3.6.0-1ubuntu1                  |
| libargon2-1:amd64               | Apache-2.0 CC0 CC0 or Apache-2.0 | memory-hard hashing function - runtime library                | 0~20171227-0.3                  |
| libasan6:amd64                  | Artistic GFDL-1.2 GPL GPL-2 GPL-3 LGPL | AddressSanitizer -- a fast memory error detector              | 11.3.0-1ubuntu1~22.04           |
| libasound2:amd64                | LPGL-2.1+ | shared library for ALSA applications                          | 1.2.6.1-1ubuntu1                |
| libasound2-data                 | LPGL-2.1+ | Configuration files and profiles for ALSA drivers             | 1.2.6.1-1ubuntu1                |
| libasound2-plugins:amd64        | LGPL-2.1+ | ALSA library additional plugins                               | 1.2.6-1                         |
| libaspell15:amd64               | GFDL-1.2+ LGPL-2.1+ | GNU Aspell spell-checker runtime library                      | 0.60.8-4build1                  |
| libassuan0:amd64                | GAP GAP~FSF GPL-2+ GPL-2+ with libtool exception GPL-3+ LGPL-2.1+ LGPL-3+ | IPC library for the GnuPG components                          | 2.5.5-1build1                   |
| libasync-interrupt-perl         | Artistic Artistic or GPL-1+ BSD-2 BSD-2 or GPL-2+ GPL-1+ GPL-2+ | module to allow C/XS libraries to interrupt perl              | 1.26-1build2                    |
| libasyncns0:amd64               | LGPL-2.1+ | Asynchronous name service query library                       | 0.8-6build2                     |
| libatasmart4:amd64              | GPL-3 LGPL-2.1 | ATA S.M.A.R.T. reading and parsing library                    | 0.19-5build2                    |
| libatk-adaptor:amd64            | GPL-2 LGPL-2+ | AT-SPI 2 toolkit bridge                                       | 2.38.0-3                        |
| libatk-bridge2.0-0:amd64        | GPL-2 LGPL-2+ | AT-SPI 2 toolkit bridge - shared library                      | 2.38.0-3                        |
| libatk-wrapper-java             | GPL-3 LGPL-2.1+ | ATK implementation for Java using JNI                         | 0.38.0-5build1                  |
| libatk-wrapper-java-jni:amd64   | GPL-3 LGPL-2.1+ | ATK implementation for Java using JNI (JNI bindings)          | 0.38.0-5build1                  |
| libatk1.0-0:amd64               | LGPL-2 | ATK accessibility toolkit                                     | 2.36.0-3build1                  |
| libatk1.0-data                  | LGPL-2 | Common files for the ATK accessibility toolkit                | 2.36.0-3build1                  |
| libatkmm-1.6-1v5:amd64          | GPL-2+ LGPL-2+ LGPL-2.1+ | C++ wrappers for ATK accessibility toolkit (shared libraries  | 2.28.2-1build1                  |
| libatm1:amd64                   | GPL-2 | shared library for ATM (Asynchronous Transfer Mode)           | 1:2.5.1-4build2                 |
| libatomic1:amd64                | Artistic GFDL-1.2 GPL GPL-2 GPL-3 LGPL | support library providing __atomic built-in functions         | 12.1.0-2ubuntu1~22.04           |
| libatopology2:amd64             | LPGL-2.1+ | shared library for handling ALSA topology definitions         | 1.2.6.1-1ubuntu1                |
| libatspi2.0-0:amd64             | AFL-2.1 AFL-2.1 or GPL-2+ GPL-2 GPL-2+ LGPL-2+ public-domain | Assistive Technology Service Provider Interface - shared lib  | 2.44.0-3                        |
| libattr1:amd64                  | GPL-2+ LGPL-2+ | extended attribute handling - shared library                  | 1:2.5.1-1build1                 |
| libaudit-common                 | GPL-2 LGPL-2.1 | Dynamic library for security auditing - common files          | 1:3.0.7-1build1                 |
| libaudit1:amd64                 | GPL-2 LGPL-2.1 | Dynamic library for security auditing                         | 1:3.0.7-1build1                 |
| libauthen-sasl-perl             | Artistic Artistic or GPL-1+ GPL-1+ | Authen::SASL - SASL Authentication framework                  | 2.1600-1.1                      |
| libavahi-client3:amd64          | GPL GPL-2 LGPL-2.1 | Avahi client library                                          | 0.8-5ubuntu5                    |
| libavahi-common-data:amd64      | GPL GPL-2 LGPL-2.1 | Avahi common data files                                       | 0.8-5ubuntu5                    |
| libavahi-common3:amd64          | GPL GPL-2 LGPL-2.1 | Avahi common library                                          | 0.8-5ubuntu5                    |
| libavahi-core7:amd64            | GPL GPL-2 LGPL-2.1 | Avahi's embeddable mDNS/DNS-SD library                        | 0.8-5ubuntu5                    |
| libavahi-glib1:amd64            | GPL GPL-2 LGPL-2.1 | Avahi GLib integration library                                | 0.8-5ubuntu5                    |
| libavc1394-0:amd64              | GPL-2+ LGPL-2.1+ | control IEEE 1394 audio/video devices                         | 0.5.4-5build2                   |
| libayatana-appindicator3-1      | BSD-2-clause BSD-3-clause GPL-3 GPL-3 or LGPL-2.1 or LGPL-3 LGPL-2.1 LGPL-2.1 or LGPL-3 LGPL-2.1 or LGPL-3 or GPL-3 LGPL-3 public-domain | Ayatana Application Indicators (GTK-3+ version)               | 0.5.90-7ubuntu2                 |
| libayatana-ido3-0.4-0:amd64     | BSD-2-clause BSD-3-clause GPL-3 GPL-3 or LGPL-2.1 or LGPL-3 GPL-3 or LGPL-3 or LGPL-2.1 LGPL-2+ LGPL-2.1 LGPL-2.1 or LGPL-3 LGPL-3 | Widgets and other objects used for Ayatana Indicators         | 0.9.1-1                         |
| libayatana-indicator3-7:amd64   | GPL-3 | panel indicator applet - shared library (GTK-3+ variant)      | 0.9.1-1                         |
| libbabeltrace1:amd64            | GPL-2 GPL-3+ LGPL-2.1 MIT | Babeltrace conversion libraries                               | 1.5.8-2build1                   |
| libbinutils:amd64               | GFDL GPL LGPL | GNU binary utilities (private shared library)                 | 2.38-4ubuntu2                   |
| libblkid1:amd64                 | BSD-2-clause BSD-3-clause BSD-4-clause GPL-2 GPL-2+ GPL-3+ LGPL LGPL-2+ LGPL-2.1+ LGPL-3+ MIT public-domain | block device ID library                                       | 2.37.2-4ubuntu3                 |
| libblockdev-crypto2:amd64       | GPL-2+ LGPL-2.1+ | Crypto plugin for libblockdev                                 | 2.26-1                          |
| libblockdev-fs2:amd64           | GPL-2+ LGPL-2.1+ | file system plugin for libblockdev                            | 2.26-1                          |
| libblockdev-loop2:amd64         | GPL-2+ LGPL-2.1+ | Loop device plugin for libblockdev                            | 2.26-1                          |
| libblockdev-part-err2:amd64     | GPL-2+ LGPL-2.1+ | Partition error utility functions for libblockdev             | 2.26-1                          |
| libblockdev-part2:amd64         | GPL-2+ LGPL-2.1+ | Partitioning plugin for libblockdev                           | 2.26-1                          |
| libblockdev-swap2:amd64         | GPL-2+ LGPL-2.1+ | Swap plugin for libblockdev                                   | 2.26-1                          |
| libblockdev-utils2:amd64        | GPL-2+ LGPL-2.1+ | Utility functions for libblockdev                             | 2.26-1                          |
| libblockdev2:amd64              | GPL-2+ LGPL-2.1+ | Library for manipulating block devices                        | 2.26-1                          |
| libbluetooth3:amd64             | GFDL GPL-2 LGPL-2.1 | Library to use the BlueZ Linux Bluetooth stack                | 5.64-0ubuntu1                   |
| libboost-regex1.74.0:amd64      | Apache-2.0 BSD2 BSD3_DEShaw BSD3_Google BSL-1.0 Caramel CrystalClear HP Jam Kempf MIT NIST OldBoost1 OldBoost2 OldBoost3 Python SGI Spencer Zlib | regular expression library for C++                            | 1.74.0-14ubuntu3                |
| libbpf0:amd64                   | BSD-2-Clause GPL-2+ GPL-2.0 GPL-2 with Linux-syscall-note exception LGPL-2.1 LGPL-2.1 or BSD-2-Clause | eBPF helper library (shared library)                          | 1:0.5.0-1                       |
| libbrlapi0.8:amd64              | LGPL-2.1 | braille display access via BRLTTY - shared library            | 6.4-4ubuntu3                    |
| libbrotli1:amd64                | MIT | library implementing brotli encoder and decoder (shared libr  | 1.0.9-2build6                   |
| libbsd0:amd64                   | Beerware BSD-2-clause BSD-2-clause-author BSD-2-clause-NetBSD BSD-2-clause-verbatim BSD-3-clause BSD-3-clause-author BSD-3-clause-John-Birrell BSD-3-clause-Regents BSD-4-clause-Christopher-G-Demetriou BSD-4-clause-Niels-Provos BSD-5-clause-Peter-Wemm Expat ISC ISC-Original public-domain | utility functions from BSD systems - shared library           | 0.11.5-1                        |
| libbz2-1.0:amd64                | BSD-variant GPL-2 | high-quality block-sorting file compressor library - runtime  | 1.0.8-5build1                   |
| libc-bin                        | GFDL-1.3 GPL-2 LGPL-2.1 | GNU C Library: Binaries                                       | 2.35-0ubuntu3.1                 |
| libc-dev-bin                    | GFDL-1.3 GPL-2 LGPL-2.1 | GNU C Library: Development binaries                           | 2.35-0ubuntu3.1                 |
| libc-devtools                   | GFDL-1.3 GPL-2 LGPL-2.1 | GNU C Library: Development tools                              | 2.35-0ubuntu3.1                 |
| libc6:amd64                     | GFDL-1.3 GPL-2 LGPL-2.1 | GNU C Library: Shared libraries                               | 2.35-0ubuntu3.1                 |
| libc6-dbg:amd64                 | GFDL-1.3 GPL-2 LGPL-2.1 | GNU C Library: detached debugging symbols                     | 2.35-0ubuntu3.1                 |
| libc6-dev:amd64                 | GFDL-1.3 GPL-2 LGPL-2.1 | GNU C Library: Development Libraries and Header Files         | 2.35-0ubuntu3.1                 |
| libcaca0:amd64                  | LGPL | colour ASCII art library                                      | 0.99.beta19-2.2ubuntu4          |
| libcairo-gobject-perl:amd64     | LGPL-2.1+ | integrate Cairo into the Glib type system in Perl             | 1.005-3build1                   |
| libcairo-gobject2:amd64         | LGPL-2.1 | Cairo 2D vector graphics library (GObject library)            | 1.16.0-5ubuntu2                 |
| libcairo-perl:amd64             | Artistic Artistic or GPL-1+ GPL-1+ LGPL-2.1+ | Perl interface to the Cairo graphics library                  | 1.109-2build1                   |
| libcairo-script-interpreter2:a  | LGPL-2.1 | Cairo 2D vector graphics library (script interpreter)         | 1.16.0-5ubuntu2                 |
| libcairo2:amd64                 | LGPL-2.1 | Cairo 2D vector graphics library                              | 1.16.0-5ubuntu2                 |
| libcairomm-1.0-1v5:amd64        | LGPL | C++ wrappers for Cairo (shared libraries)                     | 1.12.2-4build3                  |
| libcamel-1.2-63:amd64           | LGPL-2 LGPL-2.1 | Evolution MIME message handling library                       | 3.44.4-0ubuntu1                 |
| libcanberra-gtk3-0:amd64        | LGPL-2 LGPL-2.1 | GTK+ 3.0 helper for playing widget event sounds with libcanb  | 0.30-10ubuntu1                  |
| libcanberra-gtk3-module:amd64   | LGPL-2 LGPL-2.1 | translates GTK3 widgets signals to event sounds               | 0.30-10ubuntu1                  |
| libcanberra-pulse:amd64         | LGPL-2 LGPL-2.1 | PulseAudio backend for libcanberra                            | 0.30-10ubuntu1                  |
| libcanberra0:amd64              | LGPL-2 LGPL-2.1 | simple abstract interface for playing event sounds            | 0.30-10ubuntu1                  |
| libcap-ng0:amd64                | GPL-2 GPL-3 LGPL-2.1 | An alternate POSIX capabilities library                       | 0.7.9-2.2build3                 |
| libcap2:amd64                   | BSD-3-clause BSD-3-clause or GPL-2 BSD-3-clause or GPL-2+ GPL-2 GPL-2+ | POSIX 1003.1e capabilities (library)                          | 1:2.44-1build3                  |
| libcap2-bin                     | BSD-3-clause BSD-3-clause or GPL-2 BSD-3-clause or GPL-2+ GPL-2 GPL-2+ | POSIX 1003.1e capabilities (utilities)                        | 1:2.44-1build3                  |
| libcbor0.8:amd64                | Apache-2.0 Expat | library for parsing and generating CBOR (RFC 7049)            | 0.8.0-2ubuntu1                  |
| libcc1-0:amd64                  | Artistic GFDL-1.2 GPL GPL-2 GPL-3 LGPL | GCC cc1 plugin for GDB                                        | 12.1.0-2ubuntu1~22.04           |
| libcdio-cdda2:amd64             | GFDL-1.2 GPL-3 | library to read and control digital audio CDs                 | 10.2+2.0.0-1build3              |
| libcdio-paranoia2:amd64         | GFDL-1.2 GPL-3 | library to read digital audio CDs with error correction       | 10.2+2.0.0-1build3              |
| libcdio19:amd64                 | GFDL-1.2 GPL-2+ with autoconf-macro exception GPL-3 | library to read and control CD-ROM                            | 2.1.0-3build1                   |
| libcdparanoia0:amd64            | GPL-2+ LGPL-2.1+ | audio extraction tool for sampling CDs (library)              | 3.10.2+debian-14build2          |
| libchafa0:amd64                 | Expat LGPL-3.0+ MIT | library for image-to-text converter chafa                     | 1.8.0-1                         |
| libcheese-gtk25:amd64           | CC-BY-SA-3.0 GPL-2+ | tool to take pictures and videos from your webcam - widgets   | 41.1-1build1                    |
| libcheese8:amd64                | CC-BY-SA-3.0 GPL-2+ | tool to take pictures and videos from your webcam - base lib  | 41.1-1build1                    |
| libclone-perl                   | Artistic Artistic or GPL-1+ GPL-1+ | module for recursively copying Perl datatypes                 | 0.45-1build3                    |
| libclutter-1.0-0:amd64          | LGPL-2.1 | Open GL based interactive canvas library                      | 1.26.4+dfsg-4build1             |
| libclutter-1.0-common           | LGPL-2.1 | Open GL based interactive canvas library (common files)       | 1.26.4+dfsg-4build1             |
| libclutter-gst-3.0-0:amd64      | LGPL-2 | Open GL based interactive canvas library GStreamer elements   | 3.0.27-2ubuntu1                 |
| libclutter-gtk-1.0-0:amd64      | LGPL-2 | Open GL based interactive canvas library GTK+ widget          | 1.8.4-4build2                   |
| libcmark-gfm-extensions0.29.0.  | Apache BSD-2-clause CC-BY-SA-4.0 Expat Unicode | CommonMark GitHub flavor gfm extension library                | 0.29.0.gfm.3-3                  |
| libcmark-gfm0.29.0.gfm.3:amd64  | Apache BSD-2-clause CC-BY-SA-4.0 Expat Unicode | CommonMark GitHub flavor gfm library                          | 0.29.0.gfm.3-3                  |
| libcogl-common                  | Apache-2.0 BSD-3 Expat LGPL-2+ LGPL-2.1+ SGI-B-2.0 | Object oriented GL/GLES Abstraction/Utility Layer (common fi  | 1.22.8-3build1                  |
| libcogl-pango20:amd64           | Apache-2.0 BSD-3 Expat LGPL-2+ LGPL-2.1+ SGI-B-2.0 | Object oriented GL/GLES Abstraction/Utility Layer             | 1.22.8-3build1                  |
| libcogl-path20:amd64            | Apache-2.0 BSD-3 Expat LGPL-2+ LGPL-2.1+ SGI-B-2.0 | Object oriented GL/GLES Abstraction/Utility Layer             | 1.22.8-3build1                  |
| libcogl20:amd64                 | Apache-2.0 BSD-3 Expat LGPL-2+ LGPL-2.1+ SGI-B-2.0 | Object oriented GL/GLES Abstraction/Utility Layer             | 1.22.8-3build1                  |
| libcolord-gtk1:amd64            | GFDL-NIV-1.1+ GPL-2+ LGPL-2.1+ | GTK+ convenience library for interacting with colord          | 0.3.0-1                         |
| libcolord2:amd64                | CC0 GFDL-NIV GPL-2+ LGPL-2.1+ | system service to manage device colour profiles -- runtime    | 1.4.6-1                         |
| libcolorhug2:amd64              | CC0 GFDL-NIV GPL-2+ LGPL-2.1+ | library to access the ColorHug colourimeter -- runtime        | 1.4.6-1                         |
| libcom-err2:amd64               | unknown | common error description library                              | 1.46.5-2ubuntu1.1               |
| libcommon-sense-perl:amd64      | Artistic Artistic or GPL-1+ GPL-1+ | module that implements some sane defaults for Perl programs   | 3.75-2build1                    |
| libconfuse-common               | unknown | Common files for libConfuse                                   | 3.3-2                           |
| libconfuse2:amd64               | unknown | Library for parsing configuration files                       | 3.3-2                           |
| libcrack2:amd64                 | LGPL-2.1 | pro-active password checker library                           | 2.9.6-3.4build4                 |
| libcrypt-dev:amd64              | Public domain | libcrypt development files                                    | 1:4.4.27-1                      |
| libcrypt1:amd64                 | Public domain | libcrypt shared library                                       | 1:4.4.27-1                      |
| libcryptsetup12:amd64           | Apache-2.0 CC0 CC0 or Apache-2.0 GPL-2+ GPL-2+ with OpenSSL exception LGPL-2.1+ LGPL-2.1+ with OpenSSL exception public-domain | disk encryption support - shared library                      | 2:2.4.3-1ubuntu1.1              |
| libctf-nobfd0:amd64             | GFDL GPL LGPL | Compact C Type Format library (runtime, no BFD dependency)    | 2.38-4ubuntu2                   |
| libctf0:amd64                   | GFDL GPL LGPL | Compact C Type Format library (runtime, BFD dependency)       | 2.38-4ubuntu2                   |
| libcue2:amd64                   | GPL-2 GPL-3 | CUE Sheet Parser Library                                      | 2.2.1-3build3                   |
| libcups2:amd64                  | Apache-2.0 Apache-2.0-with-GPL2-LGPL2-Exception Apache-2.0-with-GPL2-LGPL2-Exception or BSD-2-clause BSD-2-Clause FSFUL Zlib | Common UNIX Printing System(tm) - Core library                | 2.4.1op1-1ubuntu4.1             |
| libcurl3-gnutls:amd64           | BSD-3-Clause BSD-4-Clause curl ISC other public-domain | easy-to-use client-side URL transfer library (GnuTLS flavour  | 7.81.0-1ubuntu1.6               |
| libcurl4:amd64                  | BSD-3-Clause BSD-4-Clause curl ISC other public-domain | easy-to-use client-side URL transfer library (OpenSSL flavou  | 7.81.0-1ubuntu1.6               |
| libdaemon0:amd64                | GPL-2 LGPL | lightweight C library for daemons - runtime library           | 0.14-7.1ubuntu3                 |
| libdata-dump-perl               | Artistic Artistic or GPL-1+ GPL-1+ | Perl module to help dump data structures                      | 1.25-1                          |
| libdatrie1:amd64                | GPL-2+ LGPL-2.1+ | Double-array trie library                                     | 0.2.13-2                        |
| libdav1d5:amd64                 | BSD-2-clause ISC public-domain | fast and small AV1 video stream decoder (shared library)      | 0.9.2-1                         |
| libdb5.3:amd64                  | unknown | Berkeley v5.3 Database Libraries [runtime]                    | 5.3.28+dfsg1-0.8ubuntu3         |
| libdbus-1-3:amd64               | AFL-2.1 BSD-3-clause BSD-3-clause-generic Expat g10-permissive GPL-2+ GPL-2+ or AFL-2.1 GPL-2+ or AFL-2.1, Tcl-BSDish | simple interprocess messaging system (library)                | 1.12.20-2ubuntu4.1              |
| libdbus-glib-1-2:amd64          | AFL-2.1 Expat GPL-2+ GPL-2+ or AFL-2.1 GPL-2+ or AFL-2.1 or Expat | deprecated library for D-Bus IPC                              | 0.112-2build1                   |
| libdbusmenu-glib4:amd64         | GPL-3 LGPL-2.1 LGPL-3 | library for passing menus over DBus                           | 16.04.1+18.10.20180917-0ubuntu  |
| libdbusmenu-gtk3-4:amd64        | GPL-3 LGPL-2.1 LGPL-3 | library for passing menus over DBus - GTK+ version            | 16.04.1+18.10.20180917-0ubuntu  |
| libdconf1:amd64                 | GPL-3 LGPL-2+ | simple configuration storage system - runtime library         | 0.40.0-3                        |
| libde265-0:amd64                | BSD-4-clause GPL-3+ LGPL-3+ other-1 public-domain-1 public-domain-2 | Open H.265 video codec implementation                         | 1.0.8-1                         |
| libdebconfclient0:amd64         | public domain | Debian Configuration Management System (C-implementation lib  | 0.261ubuntu1                    |
| libdebuginfod-common            | GPL-2 GPL-3 LGPL-3 | configuration to enable the Debian debug info server          | 0.186-1build1                   |
| libdebuginfod1:amd64            | GPL-2 GPL-3 LGPL-3 | library to interact with debuginfod (development files)       | 0.186-1build1                   |
| libdee-1.0-4:amd64              | GPL-3 LGPL-3 | Model to synchronize multiple instances over DBus - shared l  | 1.2.7+17.10.20170616-6ubuntu4   |
| libdeflate0:amd64               | Expat | fast, whole-buffer DEFLATE-based compression and decompressi  | 1.10-2                          |
| libdevmapper1.02.1:amd64        | BSD-2-Clause GPL-2.0 GPL-2.0+ LGPL-2.1 | Linux Kernel Device Mapper userspace library                  | 2:1.02.175-2.1ubuntu4           |
| libdjvulibre-text               | GPL-2 | Linguistic support files for libdjvulibre                     | 3.5.28-2build2                  |
| libdjvulibre21:amd64            | GPL-2 | Runtime support for the DjVu image format                     | 3.5.28-2build2                  |
| libdns-export1110               | BSD-2-clause BSD-3-clause ISC ISC or MPL-2.0 MPL-2.0 | Exported DNS Shared Library                                   | 1:9.11.19+dfsg-2.1ubuntu3       |
| libdotconf0:amd64               | Apache-1.1 LGPL-2.1 | Configuration file parser library - runtime files             | 1.3-0.3fakesync1build2          |
| libdpkg-perl                    | BSD-2-clause GPL-2 GPL-2+ public-domain-md5 public-domain-s-s-d | Dpkg perl modules                                             | 1.21.1ubuntu2.1                 |
| libdrm-amdgpu1:amd64            | unknown | Userspace interface to amdgpu-specific kernel DRM services -  | 2.4.110-1ubuntu1                |
| libdrm-common                   | unknown | Userspace interface to kernel DRM services -- common files    | 2.4.110-1ubuntu1                |
| libdrm-intel1:amd64             | unknown | Userspace interface to intel-specific kernel DRM services --  | 2.4.110-1ubuntu1                |
| libdrm-nouveau2:amd64           | unknown | Userspace interface to nouveau-specific kernel DRM services   | 2.4.110-1ubuntu1                |
| libdrm-radeon1:amd64            | unknown | Userspace interface to radeon-specific kernel DRM services -  | 2.4.110-1ubuntu1                |
| libdrm2:amd64                   | unknown | Userspace interface to kernel DRM services -- runtime         | 2.4.110-1ubuntu1                |
| libdv4:amd64                    | GPL-2+ LGPL-2.1+ | software library for DV format digital video (runtime lib)    | 1.0.0-14build1                  |
| libdw1:amd64                    | GPL-2 GPL-3 LGPL-3 | library that provides access to the DWARF debug information   | 0.186-1build1                   |
| libebackend-1.2-10:amd64        | LGPL-2 LGPL-2.1 | Utility library for evolution data servers                    | 3.44.4-0ubuntu1                 |
| libebook-1.2-20:amd64           | LGPL-2 LGPL-2.1 | Client library for evolution address books                    | 3.44.4-0ubuntu1                 |
| libebook-contacts-1.2-3:amd64   | LGPL-2 LGPL-2.1 | Client library for evolution contacts books                   | 3.44.4-0ubuntu1                 |
| libecal-2.0-1:amd64             | LGPL-2 LGPL-2.1 | Client library for evolution calendars                        | 3.44.4-0ubuntu1                 |
| libedata-book-1.2-26:amd64      | LGPL-2 LGPL-2.1 | Backend library for evolution address books                   | 3.44.4-0ubuntu1                 |
| libedata-cal-2.0-1:amd64        | LGPL-2 LGPL-2.1 | Backend library for evolution calendars                       | 3.44.4-0ubuntu1                 |
| libedataserver-1.2-26:amd64     | LGPL-2 LGPL-2.1 | Utility library for evolution data servers                    | 3.44.4-0ubuntu1                 |
| libedataserverui-1.2-3:amd64    | LGPL-2 LGPL-2.1 | Utility library for evolution data servers                    | 3.44.4-0ubuntu1                 |
| libedit2:amd64                  | BSD-3-clause | BSD editline and history libraries                            | 3.1-20210910-1build1            |
| libefiboot1:amd64               | GPL-2.0+ LGPL-2.0+ | Library to manage UEFI variables                              | 37-6ubuntu2                     |
| libefivar1:amd64                | GPL-2.0+ LGPL-2.0+ | Library to manage UEFI variables                              | 37-6ubuntu2                     |
| libegl-mesa0:amd64              | Apache-2.0 BSD-2-clause BSD-3-google BSL GPL Khronos MIT MLAA SGI | free implementation of the EGL API -- Mesa vendor library     | 22.0.5-0ubuntu0.1               |
| libegl1:amd64                   | 	Apache-2.0 Apache-2.0 	BSD-1-clause BSD-1-clause 	GPL GPL 	GPL-3+ GPL-3+ 	MIT MIT 	public-domain | Vendor neutral GL dispatch library -- EGL support             | 1.4.0-1                         |
| libelf1:amd64                   | GPL-2 GPL-3 LGPL-3 | library to read and write ELF files                           | 0.186-1build1                   |
| libenchant-2-2:amd64            | Expat FSFAP GPL-2.0+ GPL-3.0+ LGPL-2.0+ LGPL-2.1+ LGPL-3.0+ | Wrapper library for various spell checker engines (runtime l  | 2.3.2-1ubuntu2                  |
| libencode-locale-perl           | Artistic Artistic or GPL-1+ GPL-1+ | utility to determine the locale encoding                      | 1.05-1.1                        |
| libepoxy0:amd64                 | Expat | OpenGL function pointer management library                    | 1.5.10-1                        |
| liberror-perl                   | Artistic Artistic or GPL-1+ GPL-1+ MIT/X11 | Perl module for error/exception handling in an OO-ish way     | 0.17029-1                       |
| libespeak-ng1:amd64             | GPL-3 | Multi-lingual software speech synthesizer: shared library     | 1.50+dfsg-10                    |
| libestr0:amd64                  | LGPL-2.1 | Helper functions for handling strings (lib)                   | 0.1.10-2.1build3                |
| libev-perl                      | Artistic Artistic or GPL-2+ BSD-2-clause BSD-2-clause or GPL-2+ GPL-2+ | Perl interface to libev, the high performance event loop      | 4.33-1build2                    |
| libev4:amd64                    | BSD-2-clause BSD-2-clause or GPL-2+ GPL-2+ | high-performance event loop library modelled after libevent   | 1:4.33-1                        |
| libevdev2:amd64                 | Apache-2.0 BSD-3 GPL-2 GPL-2+ MIT | wrapper library for evdev devices                             | 1.12.1+dfsg-1                   |
| libevdocument3-4:amd64          | GFDL GPL GPL-2 LGPL-2 | Document (PostScript, PDF) rendering library                  | 42.3-0ubuntu2                   |
| libevent-core-2.1-7:amd64       | BSD-2-clause BSD-3-clause BSD-3-Clause~Kitware BSL curl Expat FSFUL FSFULLR FSFULLR-No-Warranty GPL-2+ GPL-3+ ISC | Asynchronous event notification library (core)                | 2.1.12-stable-1build3           |
| libevview3-3:amd64              | GFDL GPL GPL-2 LGPL-2 | Document (PostScript, PDF) rendering library - Gtk+ widgets   | 42.3-0ubuntu2                   |
| libexempi8:amd64                | BSD-like license | library to parse XMP metadata (Library)                       | 2.5.2-1ubuntu0.22.04.1          |
| libexif12:amd64                 | BSD-2-Clause GPL-2+ LGPL-2+ LGPL-2.1+ MIT public-domain | library to parse EXIF files                                   | 0.6.24-1build1                  |
| libexiv2-27:amd64               | BSD-3-clause Expat GPL-2+ | EXIF/IPTC/XMP metadata manipulation library                   | 0.27.5-3ubuntu1                 |
| libexpat1:amd64                 | MIT | XML parsing C library - runtime library                       | 2.4.7-1                         |
| libext2fs2:amd64                | GPL-2 LGPL-2 | ext2/ext3/ext4 file system libraries                          | 1.46.5-2ubuntu1.1               |
| libextutils-depends-perl        | Artistic Artistic or GPL-1+ GPL-1+ | Perl module for building extensions that depend on other ext  | 0.8001-1                        |
| libfakeroot:amd64               | Artistic GPL-3 | tool for simulating superuser privileges - shared libraries   | 1.28-1ubuntu1                   |
| libfastjson4:amd64              | Apache-2.0 Expat | fast json library for C                                       | 0.99.9-1build2                  |
| libfdisk1:amd64                 | BSD-2-clause BSD-3-clause BSD-4-clause GPL-2 GPL-2+ GPL-3+ LGPL LGPL-2+ LGPL-2.1+ LGPL-3+ MIT public-domain | fdisk partitioning library                                    | 2.37.2-4ubuntu3                 |
| libffi8:amd64                   | GPL | Foreign Function Interface library runtime                    | 3.4.2-4                         |
| libfftw3-double3:amd64          | GPL-2 | Library for computing Fast Fourier Transforms - Double preci  | 3.3.8-2ubuntu8                  |
| libfftw3-single3:amd64          | GPL-2 | Library for computing Fast Fourier Transforms - Single preci  | 3.3.8-2ubuntu8                  |
| libfido2-1:amd64                | BSD-2-clause ISC public-domain | library for generating and verifying FIDO 2.0 objects         | 1.10.0-1                        |
| libfile-basedir-perl            | Artistic Artistic or GPL-1+ GPL-1+ | Perl module to use the freedesktop basedir specification      | 0.09-1                          |
| libfile-desktopentry-perl       | Artistic Artistic or GPL-1+ GPL-1+ | Perl module to handle freedesktop .desktop files              | 0.22-2                          |
| libfile-fcntllock-perl          | Artistic Artistic or GPL-1+ GPL-1+ | Perl module for file locking with fcntl(2)                    | 0.22-3build7                    |
| libfile-listing-perl            | Artistic Artistic or GPL-1+ GPL-1+ | module to parse directory listings                            | 6.14-1                          |
| libfile-mimeinfo-perl           | Artistic Artistic or GPL-1+ GPL-1+ | Perl module to determine file types                           | 0.31-1                          |
| libflac8:amd64                  | BSD-3-clause GFDL-1.1+ GPL-2+ GPL-2+ or LGPL-2.1+ ISC LGPL-2+ LGPL-2.1+ Public-domain | Free Lossless Audio Codec - runtime C library                 | 1.3.3-2build2                   |
| libfont-afm-perl                | Artistic Artistic or GPL-1+ GPL-1+ | Perl interface to Adobe Font Metrics files                    | 1.20-3                          |
| libfontconfig1:amd64            | unknown | generic font configuration library - runtime                  | 2.13.1-4.2ubuntu5               |
| libfontenc1:amd64               | unknown | X11 font encoding library                                     | 1:1.1.4-1build3                 |
| libfprint-2-2:amd64             | LGPL-2.1 | async fingerprint library of fprint project, shared librarie  | 1:1.94.3+tod1-0ubuntu2~22.04.0  |
| libfreerdp-client2-2:amd64      | Apache-2.0 Apache-2.0 or BSD-2-clause or BSD-3-clause or BSL-1.0 or Expat or ISC or X11 or zlib/libpng or public-domain BSD-2-clause BSD-3-clause BSL-1.0 Expat FMILIB ISC LGPL-2.1+ NTP~legal-disclaimer public-domain X11 zlib/libpng | Free Remote Desktop Protocol library (client library)         | 2.6.1+dfsg1-3ubuntu2.2          |
| libfreerdp-server2-2:amd64      | Apache-2.0 Apache-2.0 or BSD-2-clause or BSD-3-clause or BSL-1.0 or Expat or ISC or X11 or zlib/libpng or public-domain BSD-2-clause BSD-3-clause BSL-1.0 Expat FMILIB ISC LGPL-2.1+ NTP~legal-disclaimer public-domain X11 zlib/libpng | Free Remote Desktop Protocol library (server library)         | 2.6.1+dfsg1-3ubuntu2.2          |
| libfreerdp2-2:amd64             | Apache-2.0 Apache-2.0 or BSD-2-clause or BSD-3-clause or BSL-1.0 or Expat or ISC or X11 or zlib/libpng or public-domain BSD-2-clause BSD-3-clause BSL-1.0 Expat FMILIB ISC LGPL-2.1+ NTP~legal-disclaimer public-domain X11 zlib/libpng | Free Remote Desktop Protocol library (core library)           | 2.6.1+dfsg1-3ubuntu2.2          |
| libfreetype6:amd64              | BSD-3-Clause BSL-1.0 FSFAP FTL GPL-2+ GPL-3+ MIT OpenGroup-BSD-like Public-Domain Zlib | FreeType 2 font engine, shared library files                  | 2.11.1+dfsg-1ubuntu0.1          |
| libfribidi0:amd64               | LGPL-2.1+ | Free Implementation of the Unicode BiDi algorithm             | 1.0.8-2ubuntu3.1                |
| libfuse3-3:amd64                | GPL-2 GPL-2+ LGPL-2.1 | Filesystem in Userspace (library) (3.x version)               | 3.10.5-1build1                  |
| libfwupd2:amd64                 | CC0-1.0 LGPL-2.1+ LGPL-2.1+ OR MIT MIT | Firmware update daemon library                                | 1.7.9-1~22.04.1                 |
| libfwupdplugin5:amd64           | CC0-1.0 LGPL-2.1+ LGPL-2.1+ OR MIT MIT | Firmware update daemon plugin library                         | 1.7.9-1~22.04.1                 |
| libgail-common:amd64            | LGPL-2 | GNOME Accessibility Implementation Library -- common modules  | 2.24.33-2ubuntu2                |
| libgail18:amd64                 | LGPL-2 | GNOME Accessibility Implementation Library -- shared librari  | 2.24.33-2ubuntu2                |
| libgamemode0:amd64              | BSD-3-clause FSFAP | Optimise Linux system performance on demand (host library)    | 1.6.1-1build2                   |
| libgamemodeauto0:amd64          | BSD-3-clause FSFAP | Optimise Linux system performance on demand (client library)  | 1.6.1-1build2                   |
| libgbm1:amd64                   | Apache-2.0 BSD-2-clause BSD-3-google BSL GPL Khronos MIT MLAA SGI | generic buffer management API -- runtime                      | 22.0.5-0ubuntu0.1               |
| libgc1:amd64                    | unknown | conservative garbage collector for C and C++                  | 1:8.0.6-1.1build1               |
| libgcab-1.0-0:amd64             | LGPL-2.0+ LGPL-2.1+ | Microsoft Cabinet file manipulation library                   | 1.4-3build2                     |
| libgcc-11-dev:amd64             | Artistic GFDL-1.2 GPL GPL-2 GPL-3 LGPL | GCC support library (development files)                       | 11.3.0-1ubuntu1~22.04           |
| libgcc-s1:amd64                 | Artistic GFDL-1.2 GPL GPL-2 GPL-3 LGPL | GCC support library                                           | 12.1.0-2ubuntu1~22.04           |
| libgck-1-0:amd64                | bzip2-1.0.5 LGPL-2.1+ | Glib wrapper library for PKCS#11 - runtime                    | 3.40.0-4                        |
| libgcr-base-3-1:amd64           | bzip2-1.0.5 LGPL-2.1+ | Library for Crypto related tasks                              | 3.40.0-4                        |
| libgcr-ui-3-1:amd64             | bzip2-1.0.5 LGPL-2.1+ | Library for Crypto UI related tasks                           | 3.40.0-4                        |
| libgcrypt20:amd64               | GPL-2 LGPL | LGPL Crypto library - runtime library                         | 1.9.4-3ubuntu3                  |
| libgd3:amd64                    | BSD-3-clause GAP~configure GAP~Makefile.in GD GPL-2+ GPL-2+ with Autoconf exception HPND MIT WEBP XFIG | GD Graphics Library                                           | 2.3.0-2ubuntu2                  |
| libgdata-common                 | LGPL-2.1+ | Library for accessing GData webservices - common data files   | 0.18.1-2build1                  |
| libgdata22:amd64                | LGPL-2.1+ | Library for accessing GData webservices - shared libraries    | 0.18.1-2build1                  |
| libgdbm-compat4:amd64           | GFDL-NIV-1.3+ GPL-2+ GPL-3+ | GNU dbm database routines (legacy support runtime version)    | 1.23-1                          |
| libgdbm6:amd64                  | GFDL-NIV-1.3+ GPL-2+ GPL-3+ | GNU dbm database routines (runtime version)                   | 1.23-1                          |
| libgdk-pixbuf-2.0-0:amd64       | CC0-1.0 GPL-2+ LGPL-2+ LGPL-2.1+ | GDK Pixbuf library                                            | 2.42.8+dfsg-1ubuntu0.1          |
| libgdk-pixbuf2.0-bin            | CC0-1.0 GPL-2+ LGPL-2+ LGPL-2.1+ | GDK Pixbuf library (thumbnailer)                              | 2.42.8+dfsg-1ubuntu0.1          |
| libgdk-pixbuf2.0-common         | CC0-1.0 GPL-2+ LGPL-2+ LGPL-2.1+ | GDK Pixbuf library - data files                               | 2.42.8+dfsg-1ubuntu0.1          |
| libgdm1                         | GPL-2 | GNOME Display Manager (shared library)                        | 42.0-1ubuntu7                   |
| libgee-0.8-2:amd64              | LGPL | GObject based collection and utility library                  | 0.20.5-2                        |
| libgeoclue-2-0:amd64            | GFDL-NIV-1.1+ GPL-2+ LGPL-2+ | convenience library to interact with geoinformation service   | 2.5.7-3ubuntu3                  |
| libgeocode-glib0:amd64          | LGPL-2+ | geocoding and reverse geocoding GLib library using Nominatim  | 3.26.2-2build2                  |
| libgexiv2-2:amd64               | GPL-2+ GPL-3+ LGPL-2.1 LGPL-2.1+ LGPL-2.1 or MIT MIT | GObject-based wrapper around the Exiv2 library                | 0.14.0-1build1                  |
| libgif7:amd64                   | ISC MIT | library for GIF images (library)                              | 5.1.9-2build2                   |
| libgirepository-1.0-1:amd64     | BSD-2-clause GPL-2+ LGPL-2+ MIT | Library for handling GObject introspection data (runtime lib  | 1.72.0-1                        |
| libgjs0g:amd64                  | BSD-3-clause CC0-1.0 Expat Expat or LGPL-2.0+ LGPL-2.0+ LGPL-2.1+ MPL-1.1 or GPL-2.0+ or LGPL-2.1+ MPL-2.0 | Mozilla-based javascript bindings for the GNOME platform      | 1.72.2-0ubuntu1                 |
| libgl1:amd64                    | 	Apache-2.0 Apache-2.0 	BSD-1-clause BSD-1-clause 	GPL GPL 	GPL-3+ GPL-3+ 	MIT MIT 	public-domain | Vendor neutral GL dispatch library -- legacy GL support       | 1.4.0-1                         |
| libgl1-amber-dri:amd64          | Apache-2.0 BSD-2-clause BSD-3-google BSL Khronos MIT MLAA SGI | free implementation of the OpenGL API -- DRI modules          | 21.3.7-0ubuntu1                 |
| libgl1-mesa-dri:amd64           | Apache-2.0 BSD-2-clause BSD-3-google BSL GPL Khronos MIT MLAA SGI | free implementation of the OpenGL API -- DRI modules          | 22.0.5-0ubuntu0.1               |
| libglapi-mesa:amd64             | Apache-2.0 BSD-2-clause BSD-3-google BSL GPL Khronos MIT MLAA SGI | free implementation of the GL API -- shared library           | 22.0.5-0ubuntu0.1               |
| libgles2:amd64                  | 	Apache-2.0 Apache-2.0 	BSD-1-clause BSD-1-clause 	GPL GPL 	GPL-3+ GPL-3+ 	MIT MIT 	public-domain | Vendor neutral GL dispatch library -- GLESv2 support          | 1.4.0-1                         |
| libglib-object-introspection-p  | LGPL-2.1+ | Perl bindings for gobject-introspection libraries             | 0.049-1+build2                  |
| libglib-perl:amd64              | LGPL-2.1+ | interface to the GLib and GObject libraries                   | 3:1.329.3-2build1               |
| libglib2.0-0:amd64              | LGPL | GLib library of C routines                                    | 2.72.1-1                        |
| libglib2.0-bin                  | LGPL | Programs for the GLib library                                 | 2.72.1-1                        |
| libglib2.0-data                 | LGPL | Common files for GLib library                                 | 2.72.1-1                        |
| libglibmm-2.4-1v5:amd64         | GPL-2+ LGPL-2.1+ | C++ wrapper for the GLib toolkit (shared libraries)           | 2.66.2-2                        |
| libglu1-mesa:amd64              | GPL-2 SGI-1.1 SGI-2 | Mesa OpenGL utility library (GLU)                             | 9.0.2-1                         |
| libglvnd0:amd64                 | 	Apache-2.0 Apache-2.0 	BSD-1-clause BSD-1-clause 	GPL GPL 	GPL-3+ GPL-3+ 	MIT MIT 	public-domain | Vendor neutral GL dispatch library                            | 1.4.0-1                         |
| libglx-mesa0:amd64              | Apache-2.0 BSD-2-clause BSD-3-google BSL GPL Khronos MIT MLAA SGI | free implementation of the OpenGL API -- GLX vendor library   | 22.0.5-0ubuntu0.1               |
| libglx0:amd64                   | 	Apache-2.0 Apache-2.0 	BSD-1-clause BSD-1-clause 	GPL GPL 	GPL-3+ GPL-3+ 	MIT MIT 	public-domain | Vendor neutral GL dispatch library -- GLX support             | 1.4.0-1                         |
| libgmp10:amd64                  | GPL GPL-2 GPL-3 LGPL-3 | Multiprecision arithmetic library                             | 2:6.2.1+dfsg-3ubuntu1           |
| libgnome-autoar-0-0:amd64       | LGPL-2.1+ | Archives integration support for GNOME                        | 0.4.3-1                         |
| libgnome-bg-4-1:amd64           | Expat GPL-2+ LGPL-2+ LGPL-3+ | Utility library for background images - runtime files         | 42.4-0ubuntu1                   |
| libgnome-bluetooth-3.0-13:amd6  | GPL-2+ LGPL-2.1+ | GNOME Bluetooth 3 support library                             | 42.0-5                          |
| libgnome-bluetooth13:amd64      | GPL-2+ LGPL-2.1+ | GNOME Bluetooth tools - support library                       | 3.34.5-8                        |
| libgnome-desktop-3-19:amd64     | Expat GPL-2+ LGPL-2+ LGPL-3+ | Utility library for the GNOME desktop - GTK 3 version         | 42.4-0ubuntu1                   |
| libgnome-desktop-4-1:amd64      | Expat GPL-2+ LGPL-2+ LGPL-3+ | Utility library for the GNOME desktop - runtime files         | 42.4-0ubuntu1                   |
| libgnome-menu-3-0:amd64         | GPL-2 | GNOME implementation of the freedesktop menu specification    | 3.36.0-1ubuntu3                 |
| libgnomekbd-common              | LGPL-2+ | GNOME library to manage keyboard configuration - common file  | 3.26.1-2                        |
| libgnomekbd8:amd64              | LGPL-2+ | GNOME library to manage keyboard configuration - shared libr  | 3.26.1-2                        |
| libgnutls30:amd64               | Apache-2.0 GFDL-1.3 GPL GPL-3 LGPL LGPL-3 | GNU TLS library - main runtime library                        | 3.7.3-4ubuntu1.1                |
| libgoa-1.0-0b:amd64             | LGPL-2.1+ | library for GNOME Online Accounts                             | 3.44.0-1ubuntu1                 |
| libgoa-1.0-common               | LGPL-2.1+ | library for GNOME Online Accounts - common files              | 3.44.0-1ubuntu1                 |
| libgoa-backend-1.0-1:amd64      | LGPL-2.1+ | backend library for GNOME Online Accounts                     | 3.44.0-1ubuntu1                 |
| libgomp1:amd64                  | Artistic GFDL-1.2 GPL GPL-2 GPL-3 LGPL | GCC OpenMP (GOMP) support library                             | 12.1.0-2ubuntu1~22.04           |
| libgpg-error0:amd64             | BSD-3-clause g10-permissive GPL-3+ LGPL-2.1+ LGPL-2.1+ or BSD-3-clause | GnuPG development runtime library                             | 1.43-3                          |
| libgpgme11:amd64                | GPL-2+ GPL-3+ LGPL-2+ LGPL-2.1+ LGPL-3+ LGPL-3+ or GPL-2+ | GPGME - GnuPG Made Easy (library)                             | 1.16.0-1.2ubuntu4               |
| libgphoto2-6:amd64              | BSD-3-Clause GPL-1 GPL-2 GPL-2+ GPL-3+ IJG LGPL-1.1+ LGPL-2+ LGPL-2.1+ LGPL-2+ or BSD-3-clause LGPL-3+ MIT other-2 other-3 public-domain public-domain-1 | gphoto2 digital camera library                                | 2.5.27-1build2                  |
| libgphoto2-l10n                 | BSD-3-Clause GPL-1 GPL-2 GPL-2+ GPL-3+ IJG LGPL-1.1+ LGPL-2+ LGPL-2.1+ LGPL-2+ or BSD-3-clause LGPL-3+ MIT other-2 other-3 public-domain public-domain-1 | gphoto2 digital camera library - localized messages           | 2.5.27-1build2                  |
| libgphoto2-port12:amd64         | BSD-3-Clause GPL-1 GPL-2 GPL-2+ GPL-3+ IJG LGPL-1.1+ LGPL-2+ LGPL-2.1+ LGPL-2+ or BSD-3-clause LGPL-3+ MIT other-2 other-3 public-domain public-domain-1 | gphoto2 digital camera port library                           | 2.5.27-1build2                  |
| libgpm2:amd64                   | GPL-2.0+ GPL-3.0+ | General Purpose Mouse - shared library                        | 1.20.7-10build1                 |
| libgraphene-1.0-0:amd64         | Expat | library of graphic data types                                 | 1.10.8-1                        |
| libgraphite2-3:amd64            | Artistic Artistic or GPL-1+ custom-sil-open-font-license GPL-1+ GPL-2+ LGPL-2.1+ LGPL-2.1+ or GPL-2+ or MPL-1.1 LGPL-2.1+ or MPL-1.1 or GPL-2+ MPL-1.1 public-domain | Font rendering engine for Complex Scripts -- library          | 1.3.14-1build2                  |
| libgs9:amd64                    | Adobe-2006 AGPL-3 AGPL-3+ AGPL-3+ with font exception Apache-2.0 BSD-3-Clause BSD-3-Clause~Adobe custom Expat Expat~Ghostgum Expat~SunSoft Expat~SunSoft with SunSoft exception FTL GAP~configure GPL GPL-2+ GPL-3+ GPL-3+ with Autoconf exception ISC LGPL-2.1 MIT-Open-Group none NTP~Lucent NTP~WSU public-domain X11 ZLIB | interpreter for the PostScript language and for PDF - Librar  | 9.55.0~dfsg1-0ubuntu5.1         |
| libgs9-common                   | Adobe-2006 AGPL-3 AGPL-3+ AGPL-3+ with font exception Apache-2.0 BSD-3-Clause BSD-3-Clause~Adobe custom Expat Expat~Ghostgum Expat~SunSoft Expat~SunSoft with SunSoft exception FTL GAP~configure GPL GPL-2+ GPL-3+ GPL-3+ with Autoconf exception ISC LGPL-2.1 MIT-Open-Group none NTP~Lucent NTP~WSU public-domain X11 ZLIB | interpreter for the PostScript language and for PDF - common  | 9.55.0~dfsg1-0ubuntu5.1         |
| libgsf-1-114:amd64              | exception-GPL-Autoconf FSF-UL GPL-2+ LGPL-2.1 local-m4a | Structured File Library - runtime version                     | 1.14.47-1build2                 |
| libgsf-1-common                 | exception-GPL-Autoconf FSF-UL GPL-2+ LGPL-2.1 local-m4a | Structured File Library - common files                        | 1.14.47-1build2                 |
| libgsound0:amd64                | LGPL-2.1+ | small library for playing system sounds                       | 1.0.3-2build1                   |
| libgspell-1-2:amd64             | CC0-1.0 LGPL-2.1+ | spell-checking library for GTK+ applications                  | 1.9.1-4                         |
| libgspell-1-common              | CC0-1.0 LGPL-2.1+ | libgspell architecture-independent files                      | 1.9.1-4                         |
| libgssapi-krb5-2:amd64          | GPL-2 | MIT Kerberos runtime libraries - krb5 GSS-API Mechanism       | 1.19.2-2                        |
| libgssdp-1.2-0:amd64            | Apache-2.0 Apache-2.0 or GPL-3.0+ CC0-1.0 CC0-1.0, CC-BY-SA-3.0 Expat GFDL-1.2+ GPL-3.0+ LGPL-2+ | GObject-based library for SSDP                                | 1.4.0.1-2build1                 |
| libgstreamer-gl1.0-0:amd64      | LGPL | GStreamer GL libraries                                        | 1.20.1-1                        |
| libgstreamer-plugins-base1.0-0  | LGPL | GStreamer libraries from the "base" set                       | 1.20.1-1                        |
| libgstreamer-plugins-good1.0-0  | LGPL | GStreamer development files for libraries from the "good" se  | 1.20.3-0ubuntu1                 |
| libgstreamer1.0-0:amd64         | LGPL | Core GStreamer libraries and elements                         | 1.20.3-0ubuntu1                 |
| libgtk-3-0:amd64                | Apache-2.0 check-gdk-cairo-permissive Expat LGPL-2+ LGPL-2.1+ LGPL-2+ or SWL other SWL X11R5-permissive ZPL-2.1 | GTK graphical user interface library                          | 3.24.33-1ubuntu2                |
| libgtk-3-bin                    | Apache-2.0 check-gdk-cairo-permissive Expat LGPL-2+ LGPL-2.1+ LGPL-2+ or SWL other SWL X11R5-permissive ZPL-2.1 | programs for the GTK graphical user interface library         | 3.24.33-1ubuntu2                |
| libgtk-3-common                 | Apache-2.0 check-gdk-cairo-permissive Expat LGPL-2+ LGPL-2.1+ LGPL-2+ or SWL other SWL X11R5-permissive ZPL-2.1 | common files for the GTK graphical user interface library     | 3.24.33-1ubuntu2                |
| libgtk-4-1:amd64                | Apache-2.0 Apache-2.0 or GPL-3.0+, Apache-2.0 with LLVM exception BSD-3-clause-Google CC0-1.0 CC0-1.0, CC-BY-SA-3.0 CC-BY-SA-3.0, Expat Expat or unlicense GPL-2.0+ GPL-2.0+, GPL-3.0+ lcs-telegraphics-permissive LGPL-2+ LGPL-2.1+ LGPL-2+ or MPL-1.1 MPL-1.1 sun-permissive Unicode-DFS-2016 unlicense X11R5-permissive ZPL-2.1 | GTK graphical user interface library                          | 4.6.6+ds-0ubuntu1               |
| libgtk-4-bin                    | Apache-2.0 Apache-2.0 or GPL-3.0+, Apache-2.0 with LLVM exception BSD-3-clause-Google CC0-1.0 CC0-1.0, CC-BY-SA-3.0 CC-BY-SA-3.0, Expat Expat or unlicense GPL-2.0+ GPL-2.0+, GPL-3.0+ lcs-telegraphics-permissive LGPL-2+ LGPL-2.1+ LGPL-2+ or MPL-1.1 MPL-1.1 sun-permissive Unicode-DFS-2016 unlicense X11R5-permissive ZPL-2.1 | programs for the GTK graphical user interface library         | 4.6.6+ds-0ubuntu1               |
| libgtk-4-common                 | Apache-2.0 Apache-2.0 or GPL-3.0+, Apache-2.0 with LLVM exception BSD-3-clause-Google CC0-1.0 CC0-1.0, CC-BY-SA-3.0 CC-BY-SA-3.0, Expat Expat or unlicense GPL-2.0+ GPL-2.0+, GPL-3.0+ lcs-telegraphics-permissive LGPL-2+ LGPL-2.1+ LGPL-2+ or MPL-1.1 MPL-1.1 sun-permissive Unicode-DFS-2016 unlicense X11R5-permissive ZPL-2.1 | common files for the GTK graphical user interface library     | 4.6.6+ds-0ubuntu1               |
| libgtk2.0-0:amd64               | LGPL-2 | GTK graphical user interface library - old version            | 2.24.33-2ubuntu2                |
| libgtk2.0-bin                   | LGPL-2 | programs for the GTK graphical user interface library         | 2.24.33-2ubuntu2                |
| libgtk2.0-common                | LGPL-2 | common files for the GTK graphical user interface library     | 2.24.33-2ubuntu2                |
| libgtk3-perl                    | LGPL-2.1+ | Perl bindings for the GTK+ graphical user interface library   | 0.038-1                         |
| libgtkmm-3.0-1v5:amd64          | GPL-2+ LGPL-2.1+ | C++ wrappers for GTK+ (shared libraries)                      | 3.24.5-1build1                  |
| libgtksourceview-4-0:amd64      | LGPL-2.1 | shared libraries for the GTK+ syntax highlighting widget      | 4.8.3-1                         |
| libgtksourceview-4-common       | LGPL-2.1 | common files for the GTK+ syntax highlighting widget          | 4.8.3-1                         |
| libgtop-2.0-11:amd64            | GPL-2 | gtop system monitoring library (shared)                       | 2.40.0-2build3                  |
| libgtop2-common                 | GPL-2 | gtop system monitoring library (common)                       | 2.40.0-2build3                  |
| libguard-perl                   | Artistic Artistic or GPL-1+ GPL-1+ | Perl module providing safe cleanup using guard objects        | 1.023-1build7                   |
| libgudev-1.0-0:amd64            | LGPL-2+ | GObject-based wrapper library for libudev                     | 1:237-2build1                   |
| libgupnp-1.2-1:amd64            | GFDL-1.2+ LGPL-2+ | GObject-based library for UPnP                                | 1.4.3-1                         |
| libgupnp-av-1.0-3               | GFDL-1.2+ LGPL-2.1+ | Audio/Visual utility library for GUPnP                        | 0.14.0-3                        |
| libgupnp-dlna-2.0-4             | GFDL-1.1+ LGPL-2+ | DLNA utility library for GUPnP                                | 0.12.0-3                        |
| libgusb2:amd64                  | GPL-2.0+ LGPL-2.1+ | GLib wrapper around libusb1                                   | 0.3.10-1                        |
| libgweather-3-16:amd64          | GPL-2+ LGPL-2.1+ | GWeather shared library                                       | 40.0-5build1                    |
| libgweather-common              | GPL-2+ LGPL-2.1+ | GWeather common files                                         | 40.0-5build1                    |
| libgxps2:amd64                  | LGPL-2.1+ | handling and rendering XPS documents (library)                | 0.3.2-2                         |
| libhandy-1-0:amd64              | LGPL-2.1+ | Library with GTK widgets for mobile phones                    | 1.6.1-1                         |
| libharfbuzz-icu0:amd64          | MIT | OpenType text shaping engine ICU backend                      | 2.7.4-1ubuntu3.1                |
| libharfbuzz0b:amd64             | MIT | OpenType text shaping engine (shared library)                 | 2.7.4-1ubuntu3.1                |
| libheif1:amd64                  | BOOST-1.0 BSD-3-clause BSD-4-clause GPL-3+ LGPL-3+ MIT | ISO/IEC 23008-12:2017 HEIF file format decoder - shared libr  | 1.12.0-2build1                  |
| libhogweed6:amd64               | Expat GAP GPL-2 GPL-2+ GPL-3+ GPL-3+ with Autoconf exception LGPL-2+ LGPL-3+ LGPL-3+ or GPL-2+ public-domain | low level cryptographic library (public-key cryptos)          | 3.7.3-1build2                   |
| libhtml-form-perl               | Artistic Artistic or GPL-1+ GPL-1+ | module that represents an HTML form element                   | 6.07-1                          |
| libhtml-format-perl             | Artistic Artistic or GPL-1+ GPL-1+ | module for transforming HTML into various formats             | 2.12-1.1                        |
| libhtml-parser-perl:amd64       | Artistic Artistic or GPL-1+ GPL-1+ | collection of modules that parse HTML text documents          | 3.76-1build2                    |
| libhtml-tagset-perl             | Artistic Artistic or GPL-1+ GPL-1+ | data tables pertaining to HTML                                | 3.20-4                          |
| libhtml-tree-perl               | Artistic Artistic or GPL-1+ GPL-1+ | Perl module to represent and create HTML syntax trees         | 5.07-2                          |
| libhttp-cookies-perl            | Artistic Artistic or GPL-1+ GPL-1+ | HTTP cookie jars                                              | 6.10-1                          |
| libhttp-daemon-perl             | Artistic Artistic or GPL-1+ GPL-1+ | simple http server class                                      | 6.13-1ubuntu0.1                 |
| libhttp-date-perl               | Artistic Artistic or GPL-1+ GPL-1+ | module of date conversion routines                            | 6.05-1                          |
| libhttp-message-perl            | Artistic Artistic or GPL-1+ GPL-1+ | perl interface to HTTP style messages                         | 6.36-1                          |
| libhttp-negotiate-perl          | Artistic Artistic or GPL-1+ GPL-1+ | implementation of content negotiation                         | 6.01-1                          |
| libhunspell-1.7-0:amd64         | GPL-2 LGPL-2.1 | spell checker and morphological analyzer (shared library)     | 1.7.0-4build1                   |
| libhyphen0:amd64                | GPL-2+ GPL-2+ or LGPL-2.1+ or MPL-1.1+ LGPL-2.1+ MPL-1.1+ | ALTLinux hyphenation library - shared library                 | 2.8.8-7build2                   |
| libibus-1.0-5:amd64             | GPL-2.0+ with autoconf exception GPL-3.0+ with autoconf exception ISC-Fujitsu ISC-Intel ISC-NCR ISC-Sun LGPL-2.0+ LGPL-2.1+ MIT permissive permissive-author-grant-attribution permissive-autoconf-m4 permissive-fsf-grant permissive-fsf-grant-attribution permissive-makefile-in | Intelligent Input Bus - shared library                        | 1.5.26-4                        |
| libical3:amd64                  | BSD-3-clause BSD-3-Clause GPL-1+ or Artistic-1 LGPL-2.1 LGPL-2.1 or MPL-2.0 MPL-2.0 | iCalendar library implementation in C (runtime)               | 3.0.14-1build1                  |
| libice-dev:amd64                | unknown | X11 Inter-Client Exchange library (development headers)       | 2:1.0.10-1build2                |
| libice6:amd64                   | unknown | X11 Inter-Client Exchange library                             | 2:1.0.10-1build2                |
| libicu70:amd64                  | GPL-3 MIT | International Components for Unicode                          | 70.1-2                          |
| libid3tag0:amd64                | GPL | ID3 tag reading library from the MAD project                  | 0.15.1b-14                      |
| libidn12:amd64                  | GAP GFDL-NIV-1.3+ GPL-2+ GPL-3+ LGPL-2.1+ LGPL-3+ LGPL-3+ or GPL-2+ | GNU Libidn library, implementation of IETF IDN specification  | 1.38-4build1                    |
| libidn2-0:amd64                 | GPL-2+ GPL-3+ LGPL-3+ LGPL-3+ or GPL-2+ Unicode | Internationalized domain names (IDNA2008/TR46) library        | 2.3.2-2build1                   |
| libiec61883-0:amd64             | LGPL-2.1 | partial implementation of IEC 61883 (shared lib)              | 1.2.0-4build3                   |
| libieee1284-3:amd64             | GPL | cross-platform library for parallel port access               | 0.2.11-14build2                 |
| libijs-0.35:amd64               | Expat Expat~X Expat~X with X exception GAP GAP~configure GAP~Makefile.in GPL-2+ GPL-2+ with Autoconf exception | IJS raster image transport protocol: shared library           | 0.35-15build2                   |
| libilmbase25:amd64              | boost ilmbase | several utility libraries from ILM used by OpenEXR            | 2.5.7-2                         |
| libimagequant0:amd64            | CC0 GPL-3.0+ MIT | palette quantization library                                  | 2.17.0-1                        |
| libimlib2:amd64                 | imlib2-license | image loading, rendering, saving library                      | 1.7.4-1build1                   |
| libimobiledevice6:amd64         | GPL-3+ LGPL-2.1+ | Library for communicating with iPhone and other Apple device  | 1.3.0-6build3                   |
| libinih1:amd64                  | BSD-3-Clause | simple .INI file parser                                       | 53-1ubuntu3                     |
| libinput-bin                    | Expat GPL-2 | input device management and event handling library - udev qu  | 1.20.0-1ubuntu0.1               |
| libinput10:amd64                | Expat GPL-2 | input device management and event handling library - shared   | 1.20.0-1ubuntu0.1               |
| libio-html-perl                 | Artistic Artistic or GPL-1+ GPL-1+ GPL-3+ | open an HTML file with automatic charset detection            | 1.004-2                         |
| libio-socket-ssl-perl           | Artistic Artistic or GPL-1+ GPL-1+ | Perl module implementing object oriented interface to SSL so  | 2.074-2                         |
| libio-stringy-perl              | Artistic Artistic or GPL-1+ GPL-1+ | modules for I/O on in-core objects (strings/arrays)           | 2.111-3                         |
| libip4tc2:amd64                 | Artistic custom GPL-2 GPL-2+ | netfilter libip4tc library                                    | 1.8.7-1ubuntu5                  |
| libip6tc2:amd64                 | Artistic custom GPL-2 GPL-2+ | netfilter libip6tc library                                    | 1.8.7-1ubuntu5                  |
| libipc-system-simple-perl       | Artistic Artistic or GPL-1+ GPL-1+ | Perl module to run commands simply, with detailed diagnostic  | 1.30-1                          |
| libipt2                         | Expat | Intel Processor Trace Decoder Library                         | 2.0.5-1                         |
| libisc-export1105:amd64         | BSD-2-clause BSD-3-clause ISC ISC or MPL-2.0 MPL-2.0 | Exported ISC Shared Library                                   | 1:9.11.19+dfsg-2.1ubuntu3       |
| libisl23:amd64                  | BSD-2-clause LGPL-2.1+ MIT | manipulating sets and relations of integer points bounded by  | 0.24-2build1                    |
| libitm1:amd64                   | Artistic GFDL-1.2 GPL GPL-2 GPL-3 LGPL | GNU Transactional Memory Library                              | 12.1.0-2ubuntu1~22.04           |
| libjack-jackd2-0:amd64          | BSD-2-clause BSD-3-clause Expat GPL-2 GPL-2+ GPL-2~either GPL-2~or GPL-3+ LGPL-2+ LGPL-2.1+ public-domain~Kroon | JACK Audio Connection Kit (libraries)                         | 1.9.20~dfsg-1                   |
| libjansson4:amd64               | Expat | C library for encoding, decoding and manipulating JSON data   | 2.13.1-1.1build3                |
| libjavascriptcoregtk-4.0-18:am  | AFL-2.0 AFL-2.0 or LGPL-2+ Apache-2.0 BSD-2-clause BSD-2-clause or BSL-1.0 BSD-2-clause or Expat BSD-3-clause-adam-barth BSD-3-clause-apple BSD-3-clause-canon BSD-3-clause-code-aurora BSD-3-clause-copyright-holder BSD-3-clause-ericsson BSD-3-clause-google BSD-3-clause-jochen-kalmbach BSD-3-clause-microsoft BSD-3-clause-motorola BSD-3-clause-opera BSD-valgrind BSL-1.0 Expat GPL-2+ GPL-2+ or LGPL-2.1+ or MPL-1.1 GPL-2+ with Bison exception GPL-3+ GPL-3+ with Bison exception ISC LGPL-2 LGPL-2+ LGPL-2.1 LGPL-2.1+ LGPL-2.1+ or MPL-1.1 LGPL-2+ or MPL-1.1 MPL-1.1 MPL-2.0 public-domain | JavaScript engine library from WebKitGTK                      | 2.36.8-0ubuntu0.22.04.1         |
| libjbig0:amd64                  | GPL-2+ | JBIGkit libraries                                             | 2.1-3.1build3                   |
| libjbig2dec0:amd64              | AGPL-3+ BSD-2-clause GPL-3+ LGPL-2.1+ pubic-domain public-domain | JBIG2 decoder library - shared libraries                      | 0.19-3build2                    |
| libjcat1:amd64                  | LGPL-2.1+ | JSON catalog library                                          | 0.1.9-1                         |
| libjpeg-turbo8:amd64            | LGPL-2.1 | IJG JPEG compliant runtime library.                           | 2.1.2-0ubuntu1                  |
| libjpeg8:amd64                  | LGPL-2.1 | Independent JPEG Group's JPEG runtime library (dependency pa  | 8c-2ubuntu10                    |
| libjq1:amd64                    | CC-BY-3.0 Expat GPL-2.0+ MIT | lightweight and flexible command-line JSON processor - share  | 1.6-2.1ubuntu3                  |
| libjson-c5:amd64                | Expat | JSON manipulation library - shared library                    | 0.15-3~ubuntu1.22.04.1          |
| libjson-glib-1.0-0:amd64        | LGPL-2.1+ | GLib JSON manipulation library                                | 1.6.6-1build1                   |
| libjson-glib-1.0-common         | LGPL-2.1+ | GLib JSON manipulation library (common files)                 | 1.6.6-1build1                   |
| libjson-xs-perl                 | Artistic Artistic or GPL-1+ GPL-1+ | module for manipulating JSON-formatted data (C/XS-accelerate  | 4.030-1build3                   |
| libjxr-tools                    | BSD-2-clause | JPEG-XR lib - command line apps                               | 1.2~git20170615.f752187-5       |
| libjxr0:amd64                   | BSD-2-clause | JPEG-XR lib - libraries                                       | 1.2~git20170615.f752187-5       |
| libk5crypto3:amd64              | GPL-2 | MIT Kerberos runtime libraries - Crypto Library               | 1.19.2-2                        |
| libkeyutils1:amd64              | GPL-2+ LGPL-2+ | Linux Key Management Utilities (library)                      | 1.6.1-2ubuntu3                  |
| libklibc:amd64                  | GPL-2 | minimal libc subset for use with initramfs                    | 2.0.10-4                        |
| libkmod2:amd64                  | GPL-2 LGPL-2.1 | libkmod shared library                                        | 29-1ubuntu1                     |
| libkpathsea6:amd64              | BSD license MIT license | TeX Live: path search library for TeX (runtime part)          | 2021.20210626.59705-1build1     |
| libkrb5-3:amd64                 | GPL-2 | MIT Kerberos runtime libraries                                | 1.19.2-2                        |
| libkrb5support0:amd64           | GPL-2 | MIT Kerberos runtime libraries - Support library              | 1.19.2-2                        |
| libksba8:amd64                  | GPL-3 | X.509 and CMS support library                                 | 1.6.0-2ubuntu0.1                |
| liblcms2-2:amd64                | GPL-2+ GPL-3 GPL-3 (GPL-3 for the fast_float plugin only) MIT | Little CMS 2 color management library                         | 2.12~rc1-2build2                |
| liblcms2-utils                  | GPL-2+ GPL-3 GPL-3 (GPL-3 for the fast_float plugin only) MIT | Little CMS 2 color management library (utilities)             | 2.12~rc1-2build2                |
| libldap-2.5-0:amd64             | unknown | OpenLDAP libraries                                            | 2.5.13+dfsg-0ubuntu0.22.04.1    |
| libldap-common                  | unknown | OpenLDAP common files for libraries                           | 2.5.13+dfsg-0ubuntu0.22.04.1    |
| libldb2:amd64                   | BSD-3 GPL-3.0+ ISC LGPL-3.0+ PostgreSQL | LDAP-like embedded database - shared library                  | 2:2.4.4-0ubuntu0.1              |
| libllvm13:amd64                 | APACHE-2-LLVM-EXCEPTIONS BSD-3-clause BSD-3-Clause MIT Python solar-public-domain | Modular compiler and toolchain technologies, runtime library  | 1:13.0.1-2ubuntu2.1             |
| liblmdb0:amd64                  | OpenLDAP-2.8 | Lightning Memory-Mapped Database shared library               | 0.9.24-1build2                  |
| liblocale-gettext-perl          | Artistic Artistic or GPL-1+ GPL-1+ | module using libc functions for internationalization in Perl  | 1.07-4build3                    |
| liblouis-data                   | GPL LGPL | Braille translation library - data                            | 3.20.0-2ubuntu0.1               |
| liblouis20:amd64                | GPL LGPL | Braille translation library - shared libs                     | 3.20.0-2ubuntu0.1               |
| liblqr-1-0:amd64                | GPL-3 LGPL-3 | converts plain array images into multi-size representation    | 0.4.2-2.1                       |
| liblsan0:amd64                  | Artistic GFDL-1.2 GPL GPL-2 GPL-3 LGPL | LeakSanitizer -- a memory leak detector (runtime)             | 12.1.0-2ubuntu1~22.04           |
| libltdl7:amd64                  | GFDL GPL | System independent dlopen wrapper for GNU libtool             | 2.4.6-15build2                  |
| libluajit-5.1-2:amd64           | MIT/X PD | Just in time compiler for Lua - library version               | 2.1.0~beta3+dfsg-6              |
| libluajit-5.1-common            | MIT/X PD | Just in time compiler for Lua - common files                  | 2.1.0~beta3+dfsg-6              |
| liblwp-mediatypes-perl          | Artistic Artistic or GPL-1+ GPL-1+ | module to guess media type for a file or a URL                | 6.04-1                          |
| liblwp-protocol-https-perl      | Artistic Artistic or GPL-1+ GPL-1+ | HTTPS driver for LWP::UserAgent                               | 6.10-1                          |
| liblz4-1:amd64                  | BSD-2-clause GPL-2 GPL-2+ | Fast LZ compression algorithm library - runtime               | 1.9.3-2build2                   |
| liblzma5:amd64                  | Autoconf config-h GPL-2 GPL-2+ LGPL-2.1+ noderivs none PD PD-debian permissive-fsf permissive-nowarranty probably-PD | XZ-format compression library                                 | 5.2.5-2ubuntu1                  |
| liblzo2-2:amd64                 | GPL-2+ | data compression library                                      | 2.10-2build3                    |
| libm17n-0:amd64                 | GPL-3 LGPL-2.1 | multilingual text processing library - runtime                | 1.8.0-4                         |
| libmagic-mgc                    | BSD-2-Clause-alike BSD-2-Clause-netbsd BSD-2-Clause-regents MIT-Old-Style-with-legal-disclaimer-2 public-domain | File type determination library using "magic" numbers (compi  | 1:5.41-3                        |
| libmagic1:amd64                 | BSD-2-Clause-alike BSD-2-Clause-netbsd BSD-2-Clause-regents MIT-Old-Style-with-legal-disclaimer-2 public-domain | Recognize the type of data in a file using "magic" numbers -  | 1:5.41-3                        |
| libmagickcore-6.q16-6:amd64     | aclocal BSD-with-FSF-change-public-domain GNU-All-Permissive-License GPL-2+ GPL2+-with-Autoconf-Macros-exception GPL3+-with-Autoconf-Macros-exception GPL3+-with-Autoconf-Macros-exception-GNU Imagemagick ImageMagick ImageMagickLicensePartEZXML ImageMagickLicensePartFIG ImageMagickLicensePartGsview ImageMagickLicensePartOpenSSH ImageMagickPartGraphicsMagick ImageMagickPartlibjpeg ImageMagickPartlibsquish LGPL-3+ Magick++ Makefile-in Perllikelicense TatcherUlrichPublicDomain | low-level image manipulation library -- quantum depth Q16     | 8:6.9.11.60+dfsg-1.3build2      |
| libmagickcore-6.q16-6-extra:am  | aclocal BSD-with-FSF-change-public-domain GNU-All-Permissive-License GPL-2+ GPL2+-with-Autoconf-Macros-exception GPL3+-with-Autoconf-Macros-exception GPL3+-with-Autoconf-Macros-exception-GNU Imagemagick ImageMagick ImageMagickLicensePartEZXML ImageMagickLicensePartFIG ImageMagickLicensePartGsview ImageMagickLicensePartOpenSSH ImageMagickPartGraphicsMagick ImageMagickPartlibjpeg ImageMagickPartlibsquish LGPL-3+ Magick++ Makefile-in Perllikelicense TatcherUlrichPublicDomain | low-level image manipulation library - extra codecs (Q16)     | 8:6.9.11.60+dfsg-1.3build2      |
| libmagickwand-6.q16-6:amd64     | aclocal BSD-with-FSF-change-public-domain GNU-All-Permissive-License GPL-2+ GPL2+-with-Autoconf-Macros-exception GPL3+-with-Autoconf-Macros-exception GPL3+-with-Autoconf-Macros-exception-GNU Imagemagick ImageMagick ImageMagickLicensePartEZXML ImageMagickLicensePartFIG ImageMagickLicensePartGsview ImageMagickLicensePartOpenSSH ImageMagickPartGraphicsMagick ImageMagickPartlibjpeg ImageMagickPartlibsquish LGPL-3+ Magick++ Makefile-in Perllikelicense TatcherUlrichPublicDomain | image manipulation library -- quantum depth Q16               | 8:6.9.11.60+dfsg-1.3build2      |
| libmailtools-perl               | Artistic Artistic or GPL-1+ GPL-1+ | modules to manipulate email in perl programs                  | 2.21-1                          |
| libmanette-0.2-0:amd64          | LGPL-2.1+ | Simple GObject game controller library                        | 0.2.6-3build1                   |
| libmaxminddb0:amd64             | Apache-2.0 BSD-2-clause BSD-3-clause BSD-4-clause CC-BY-SA-3.0 GPL-2+ | IP geolocation database library                               | 1.5.2-1build2                   |
| libmbim-glib4:amd64             | GFDL-NIV-1.3+ GPL-2+ LGPL-2+ | Support library to use the MBIM protocol                      | 1.26.2-1build1                  |
| libmbim-proxy                   | GFDL-NIV-1.3+ GPL-2+ LGPL-2+ | Proxy to communicate with MBIM ports                          | 1.26.2-1build1                  |
| libmd0:amd64                    | Beerware BSD-2-clause BSD-2-clause-NetBSD BSD-3-clause BSD-3-clause-Aaron-D-Gifford ISC public-domain-md4 public-domain-md5 public-domain-sha1 | message digest functions from BSD systems - shared library    | 1.0.4-1build1                   |
| libmediaart-2.0-0:amd64         | GPL-2.0+ LGPL-2.1+ | media art extraction and cache management library             | 1.9.5-2build1                   |
| libmm-glib0:amd64               | GPL-2.0 GPL-2.0+ GPL-3.0+ LGPL-2.0+ | D-Bus service for managing modems - shared libraries          | 1.18.6-1                        |
| libmnl0:amd64                   | GPL-2+ LGPL-2.1 | minimalistic Netlink communication library                    | 1.0.4-3build2                   |
| libmount1:amd64                 | BSD-2-clause BSD-3-clause BSD-4-clause GPL-2 GPL-2+ GPL-3+ LGPL LGPL-2+ LGPL-2.1+ LGPL-3+ MIT public-domain | device mounting library                                       | 2.37.2-4ubuntu3                 |
| libmozjs-91-0:amd64             | aclocal-public-domain Apache-2.0 Beerware BSD-2-clause BSD-3-clause BSD-3-clause-ARM BSD-3-clause-ECMA BSD-3-clause-Google BSD-3-clause-Intel BSD-3-clause or GPL-2 BSD-3-clause-psutil BSD-3-clause-SwapOff BSD-3-clause-UC BSD-3-clause-UC or ISC BSD-3-clause-Voidspace BSL-1.0 CC0-1.0 Expat GPL-2 GPL-2+ GPL-2 or GPL-3 GPL-3 GPL-3+ GPL-3+ with Bison exception GPL-some-version ICU-IBM ICU-Unicode ISC LGPL-2.1 MIT-Lucent MPL-2.0 nspr-public-domain NTP Python SunPro Unlicense Zlib | SpiderMonkey JavaScript library                               | 91.10.0-0ubuntu1                |
| libmp3lame0:amd64               | BSD-3-clause GPL-1+ LGPL-2+ LGPL-2.1+ zlib/libpng | MP3 encoding library                                          | 3.100-3build2                   |
| libmpc3:amd64                   | LGPL-3 | multiple precision complex floating-point library             | 1.2.1-2build1                   |
| libmpdec3:amd64                 | BSD GPL-2+ | library for decimal floating point arithmetic (runtime libra  | 2.5.1-2build2                   |
| libmpfr6:amd64                  | GFDL-1.2 LGPL-3 | multiple precision floating-point computation                 | 4.1.0-3build3                   |
| libmpg123-0:amd64               | LGPL-2.1 | MPEG layer 1/2/3 audio decoder (shared library)               | 1.29.3-1build1                  |
| libmsgpackc2:amd64              | Boost-1 GPL-3 | binary-based efficient object serialization library           | 3.3.0-4                         |
| libmtdev1:amd64                 | Expat | Multitouch Protocol Translation Library - shared library      | 1.1.6-1build4                   |
| libmtp-common                   | GPL-2+ LGPL-2+ LGPL-2.1+ | Media Transfer Protocol (MTP) common files                    | 1.1.19-1build1                  |
| libmtp-runtime                  | GPL-2+ LGPL-2+ LGPL-2.1+ | Media Transfer Protocol (MTP) runtime tools                   | 1.1.19-1build1                  |
| libmtp9:amd64                   | GPL-2+ LGPL-2+ LGPL-2.1+ | Media Transfer Protocol (MTP) library                         | 1.1.19-1build1                  |
| libmutter-10-0:amd64            | DEC-BSD-variant Expat GPL-2+ GPL-3+ LGPL-2+ LGPL-2.1+ NTP-BSD-variant OpenGroup-BSD-variant SGI-B-2.0 WRF-BSD-variant | window manager library from the Mutter window manager         | 42.5-0ubuntu1                   |
| libnautilus-extension1a:amd64   | GPL-3 | libraries for nautilus components - runtime version           | 1:42.2-0ubuntu1                 |
| libncurses6:amd64               | BSD-3-clause MIT/X11 X11 | shared libraries for terminal handling                        | 6.3-2                           |
| libncursesw6:amd64              | BSD-3-clause MIT/X11 X11 | shared libraries for terminal handling (wide character suppo  | 6.3-2                           |
| libndp0:amd64                   | LGPL-2.1+ | Library for Neighbor Discovery Protocol                       | 1.8-0ubuntu3                    |
| libnet-dbus-perl                | Artistic Artistic or GPL-1+ GPL-1+ | Perl extension for the DBus bindings                          | 1.2.0-1build3                   |
| libnet-http-perl                | Artistic Artistic or GPL-1+ GPL-1+ | module providing low-level HTTP connection client             | 6.22-1                          |
| libnet-smtp-ssl-perl            | Artistic Artistic or GPL-1+ GPL-1+ | Perl module providing SSL support to Net::SMTP                | 1.04-1                          |
| libnet-ssleay-perl:amd64        | Artistic Artistic-2.0 Artistic or GPL-1+ GPL-1+ | Perl module for Secure Sockets Layer (SSL)                    | 1.92-1build2                    |
| libnetfilter-conntrack3:amd64   | GPL-2+ | Netfilter netlink-conntrack library                           | 1.0.9-1                         |
| libnetpbm10                     | BSD GPL | Graphics conversion tools shared libraries                    | 2:10.0-15.4                     |
| libnetplan0:amd64               | GPL-3 | YAML network configuration abstraction runtime library        | 0.104-0ubuntu2.1                |
| libnettle8:amd64                | Expat GAP GPL-2 GPL-2+ GPL-3+ GPL-3+ with Autoconf exception LGPL-2+ LGPL-3+ LGPL-3+ or GPL-2+ public-domain | low level cryptographic library (symmetric and one-way crypt  | 3.7.3-1build2                   |
| libnewt0.52:amd64               | LGPL-2 | Not Erik's Windowing Toolkit - text mode windowing with slan  | 0.52.21-5ubuntu2                |
| libnfnetlink0:amd64             | GPL | Netfilter netlink library                                     | 1.0.1-3build3                   |
| libnfs13:amd64                  | BSD-2-clause BSD-3-Clause GPL-3+ LGPL-2.1+ | NFS client library (shared library)                           | 4.0.0-1build2                   |
| libnftables1:amd64              | CC-BY-SA-4.0 GPL-2 GPL-2+ | Netfilter nftables high level userspace API library           | 1.0.2-1ubuntu3                  |
| libnftnl11:amd64                | GPL-2 GPL-2+ | Netfilter nftables userspace API library                      | 1.2.1-1build1                   |
| libnghttp2-14:amd64             | all-permissive BSD-2-clause Expat GPL-3+ with autoconf exception MIT SIL-OFL-1.1 | library implementing HTTP/2 protocol (shared library)         | 1.43.0-1build3                  |
| libnl-3-200:amd64               | GPL-2 LGPL-2.1 | library for dealing with netlink sockets                      | 3.5.0-0.1                       |
| libnl-genl-3-200:amd64          | GPL-2 LGPL-2.1 | library for dealing with netlink sockets - generic netlink    | 3.5.0-0.1                       |
| libnl-route-3-200:amd64         | GPL-2 LGPL-2.1 | library for dealing with netlink sockets - route interface    | 3.5.0-0.1                       |
| libnm0:amd64                    | GFDL-NIV-1.1+ GPL-2+ LGPL-2.1+ | GObject-based client library for NetworkManager               | 1.36.6-0ubuntu2                 |
| libnma-common                   | GPL-2+ LGPL-2+ LGPL-2.1+ | NetworkManager GUI library - translations                     | 1.8.34-1ubuntu1                 |
| libnma0:amd64                   | GPL-2+ LGPL-2+ LGPL-2.1+ | NetworkManager GUI library                                    | 1.8.34-1ubuntu1                 |
| libnotify-bin                   | LGPL-2.1 | sends desktop notifications to a notification daemon (Utilit  | 0.7.9-3ubuntu5.22.04.1          |
| libnotify4:amd64                | LGPL-2.1 | sends desktop notifications to a notification daemon          | 0.7.9-3ubuntu5.22.04.1          |
| libnpth0:amd64                  | LGPL-2.1+ | replacement for GNU Pth using system threads                  | 1.6-3build2                     |
| libnsl-dev:amd64                | BSD-3-clause GPL-2+-autoconf-exception GPL-2+-libtool-exception GPL-3+-autoconf-exception LGPL-2.1 LGPL-2.1+ MIT permissive-autoconf-m4 permissive-autoconf-m4-no-warranty permissive-configure permissive-fsf permissive-makefile-in | libnsl development files                                      | 1.3.0-2build2                   |
| libnsl2:amd64                   | BSD-3-clause GPL-2+-autoconf-exception GPL-2+-libtool-exception GPL-3+-autoconf-exception LGPL-2.1 LGPL-2.1+ MIT permissive-autoconf-m4 permissive-autoconf-m4-no-warranty permissive-configure permissive-fsf permissive-makefile-in | Public client interface for NIS(YP) and NIS+                  | 1.3.0-2build2                   |
| libnspr4:amd64                  | MPL-2.0 | NetScape Portable Runtime Library                             | 2:4.32-3build1                  |
| libnss-mdns:amd64               | LGPL-2+ | NSS module for Multicast DNS name resolution                  | 0.15.1-1ubuntu1                 |
| libnss-systemd:amd64            | CC0-1.0 Expat GPL-2 GPL-2+ GPL-2 with Linux-syscall-note exception LGPL-2.1+ public-domain | nss module providing dynamic user and group name resolution   | 249.11-0ubuntu3.6               |
| libnss3:amd64                   | BSD-3 MIT MPL-2.0 public-domain Zlib | Network Security Service libraries                            | 2:3.68.2-0ubuntu1.1             |
| libntfs-3g89                    | GPL-2+ LGPL-2+ | read/write NTFS driver for FUSE (runtime library)             | 1:2021.8.22-3ubuntu1.2          |
| libnuma1:amd64                  | GPL LGPL | Libraries for controlling NUMA policy                         | 2.0.14-3ubuntu2                 |
| libogg0:amd64                   | unknown | Ogg bitstream library                                         | 1.3.5-0ubuntu3                  |
| libonig5:amd64                  | BSD-2-clause GPL-2+ | regular expressions library                                   | 6.9.7.1-2build1                 |
| libopenexr25:amd64              | BSD-3-clause openexr | runtime files for the OpenEXR image library                   | 2.5.7-1                         |
| libopengl0:amd64                | 	Apache-2.0 Apache-2.0 	BSD-1-clause BSD-1-clause 	GPL GPL 	GPL-3+ GPL-3+ 	MIT MIT 	public-domain | Vendor neutral GL dispatch library -- OpenGL support          | 1.4.0-1                         |
| libopenjp2-7:amd64              | BSD-2 BSD-3 LIBPNG LIBTIFF LIBTIFF-GLARSON LIBTIFF-PIXAR MIT public-domain ZLIB | JPEG 2000 image compression/decompression library             | 2.4.0-6                         |
| libopus0:amd64                  | unknown | Opus codec runtime library                                    | 1.3.1-0.1build2                 |
| liborc-0.4-0:amd64              | unknown | Library of Optimized Inner Loops Runtime Compiler             | 1:0.4.32-2                      |
| libotf1:amd64                   | GPL-3 LGPL-2.1 | Library for handling OpenType Font - runtime                  | 0.9.16-3build1                  |
| libp11-kit0:amd64               | Apache-2.0 BSD-3-Clause ISC ISC+IBM LGPL-2.1+ permissive-like-automake-output same-as-rest-of-p11kit | library for loading and coordinating access to PKCS#11 modul  | 0.24.0-6build1                  |
| libpackagekit-glib2-18:amd64    | GPL-2+ LGPL-2.1+ | Library for accessing PackageKit using GLib                   | 1.2.5-2ubuntu2                  |
| libpam-cap:amd64                | BSD-3-clause BSD-3-clause or GPL-2 BSD-3-clause or GPL-2+ GPL-2 GPL-2+ | POSIX 1003.1e capabilities (PAM module)                       | 1:2.44-1build3                  |
| libpam-fprintd:amd64            | GPL-2+ | PAM module for fingerprint authentication through fprintd     | 1.94.2-1ubuntu0.22.04.1         |
| libpam-gnome-keyring:amd64      | BSD-3-clause custom-license Expat FSFAP FSFUL FSFULLR FSFULLR or GPL-2+ with AutoConf exception FSFULLR or GPL-2+ with LibTool exception GPL-2+ GPL-2+ or LGPL-3+ GPL-2+ with AutoConf exception GPL-2+ with LibTool exception GPL-3+ with AutoConf exception LGPL-2+ LGPL-2.1+ LGPL-3+ MPL-1.1 MPL-1.1 or GPL-2+ or LGPL-2.1+ | PAM module to unlock the GNOME keyring upon login             | 40.0-3ubuntu3                   |
| libpam-modules:amd64            | GPL | Pluggable Authentication Modules for PAM                      | 1.4.0-11ubuntu2                 |
| libpam-modules-bin              | GPL | Pluggable Authentication Modules for PAM - helper binaries    | 1.4.0-11ubuntu2                 |
| libpam-pwquality:amd64          | GPL-2+ GPL-3+ libpwquality libpwquality or GPL-2+ | PAM module to check password strength                         | 1.4.4-1build2                   |
| libpam-runtime                  | GPL | Runtime support for the PAM library                           | 1.4.0-11ubuntu2                 |
| libpam-sss:amd64                | GPL-3 LGPL-3 | Pam module for the System Security Services Daemon            | 2.6.3-1ubuntu3.2                |
| libpam-systemd:amd64            | CC0-1.0 Expat GPL-2 GPL-2+ GPL-2 with Linux-syscall-note exception LGPL-2.1+ public-domain | system and service manager - PAM module                       | 249.11-0ubuntu3.6               |
| libpam0g:amd64                  | GPL | Pluggable Authentication Modules library                      | 1.4.0-11ubuntu2                 |
| libpango-1.0-0:amd64            | Apache-2 Apache-2.0 Apache-2.0 or GPL-3.0+, Bitstream-Vera CC0-1.0 CC0-1.0, CC-BY-SA-3.0 CC-BY-SA-3.0, Chromium-BSD-style Example Expat GPL-2+ GPL-2+, GPL-3.0+ ICU LGPL-2+ LGPL-2+, LGPL-2.1+ LGPL-2+ or MPL-1.1 MPL-1.1 OFL-1.1 TCL Unicode | Layout and rendering of internationalized text                | 1.50.6+ds-2                     |
| libpangocairo-1.0-0:amd64       | Apache-2 Apache-2.0 Apache-2.0 or GPL-3.0+, Bitstream-Vera CC0-1.0 CC0-1.0, CC-BY-SA-3.0 CC-BY-SA-3.0, Chromium-BSD-style Example Expat GPL-2+ GPL-2+, GPL-3.0+ ICU LGPL-2+ LGPL-2+, LGPL-2.1+ LGPL-2+ or MPL-1.1 MPL-1.1 OFL-1.1 TCL Unicode | Layout and rendering of internationalized text                | 1.50.6+ds-2                     |
| libpangoft2-1.0-0:amd64         | Apache-2 Apache-2.0 Apache-2.0 or GPL-3.0+, Bitstream-Vera CC0-1.0 CC0-1.0, CC-BY-SA-3.0 CC-BY-SA-3.0, Chromium-BSD-style Example Expat GPL-2+ GPL-2+, GPL-3.0+ ICU LGPL-2+ LGPL-2+, LGPL-2.1+ LGPL-2+ or MPL-1.1 MPL-1.1 OFL-1.1 TCL Unicode | Layout and rendering of internationalized text                | 1.50.6+ds-2                     |
| libpangomm-1.4-1v5:amd64        | GPL-2+ LGPL-2.1+ | C++ Wrapper for pango (shared libraries)                      | 2.46.2-1                        |
| libpangoxft-1.0-0:amd64         | Apache-2 Apache-2.0 Apache-2.0 or GPL-3.0+, Bitstream-Vera CC0-1.0 CC0-1.0, CC-BY-SA-3.0 CC-BY-SA-3.0, Chromium-BSD-style Example Expat GPL-2+ GPL-2+, GPL-3.0+ ICU LGPL-2+ LGPL-2+, LGPL-2.1+ LGPL-2+ or MPL-1.1 MPL-1.1 OFL-1.1 TCL Unicode | Layout and rendering of internationalized text                | 1.50.6+ds-2                     |
| libpaper-utils                  | GPL-2 | library for handling paper characteristics (utilities)        | 1.1.28build2                    |
| libpaper1:amd64                 | GPL-2 | library for handling paper characteristics                    | 1.1.28build2                    |
| libparted-fs-resize0:amd64      | GPL-3 | disk partition manipulator - shared FS resizing library       | 3.4-2build1                     |
| libparted2:amd64                | GPL-3 | disk partition manipulator - shared library                   | 3.4-2build1                     |
| libpcap0.8:amd64                | BSD license | system interface for user-level packet capture                | 1.10.1-4build1                  |
| libpcaudio0:amd64               | BSD-3-clause GPL-2+ GPL-3+ | C API to different audio devices - shared library             | 1.1-6build2                     |
| libpci3:amd64                   | GPL-2+ | PCI utilities (shared library)                                | 1:3.7.0-6                       |
| libpciaccess0:amd64             | GPL | Generic PCI access library for X                              | 0.16-3                          |
| libpcre2-32-0:amd64             | "BSD" LICENCE "BSD" License public domain | New Perl Compatible Regular Expression Library - 32 bit runt  | 10.39-3ubuntu0.1                |
| libpcre2-8-0:amd64              | "BSD" LICENCE "BSD" License public domain | New Perl Compatible Regular Expression Library- 8 bit runtim  | 10.39-3ubuntu0.1                |
| libpcre3:amd64                  | "BSD" LICENCE "BSD" License | Old Perl 5 Compatible Regular Expression Library - runtime f  | 2:8.39-13ubuntu0.22.04.1        |
| libpcsclite1:amd64              | BSD-3-clause GPL-3+ ISC | Middleware to access a smart card using PC/SC (library)       | 1.9.5-3                         |
| libpeas-1.0-0:amd64             | GPL-3 LGPL-2 | Application plugin library                                    | 1.32.0-1                        |
| libpeas-common                  | GPL-3 LGPL-2 | Application plugin library (common files)                     | 1.32.0-1                        |
| libperl5.34:amd64               | Artistic Artistic-2 Artistic-dist Artistic or GPL-1+ or Artistic-dist BSD-3-clause BSD-3-clause-GENERIC BSD-3-clause-with-weird-numbering BSD-4-clause-POWERDOG BZIP CC0-1.0 DONT-CHANGE-THE-GPL Expat Expat or GPL-1+ or Artistic GPL-1+ GPL-1+ or Artistic GPL-1+ or Artistic, GPL-1+ or Artistic or Artistic-dist GPL-2+ GPL-2+ or Artistic GPL-3+-WITH-BISON-EXCEPTION HSIEH-BSD HSIEH-DERIVATIVE LGPL-2.1 REGCOMP REGCOMP, RRA-KEEP-THIS-NOTICE SDBM-PUBLIC-DOMAIN TEXT-TABS Unicode ZLIB | shared Perl library                                           | 5.34.0-3ubuntu1.1               |
| libphonenumber8:amd64           | Apache-2.0 BSD-3-clause MIT | parsing/formatting/validating phone numbers                   | 8.12.44-1                       |
| libpipeline1:amd64              | GPL-2+ GPL-3+ | Unix process pipeline manipulation library                    | 1.5.5-1                         |
| libpipewire-0.3-0:amd64         | Expat LGPL-2+ LGPL-2.1+ | libraries for the PipeWire multimedia server                  | 0.3.48-1ubuntu2                 |
| libpipewire-0.3-common          | Expat LGPL-2+ LGPL-2.1+ | libraries for the PipeWire multimedia server - common files   | 0.3.48-1ubuntu2                 |
| libpipewire-0.3-modules:amd64   | Expat LGPL-2+ LGPL-2.1+ | libraries for the PipeWire multimedia server - modules        | 0.3.48-1ubuntu2                 |
| libpixman-1-0:amd64             | MIT license | pixel-manipulation library for X and cairo                    | 0.40.0-1build4                  |
| libpkcs11-helper1:amd64         | BSD-3-clause BSD-3-clause or GPL-2 GPL-2 permissive | library that simplifies the interaction with PKCS#11          | 1.28-1ubuntu0.22.04.1           |
| libplist3:amd64                 | Expat GPL-3+ LGPL-2.1+ | Library for handling Apple binary and XML property lists      | 2.2.0-6build2                   |
| libplymouth5:amd64              | GPL-2+ other | graphical boot animation and logger - shared libraries        | 0.9.5+git20211018-1ubuntu3      |
| libpng16-16:amd64               | Apache-2.0 BSD-3-clause BSD-like-with-advertising-clause expat GPL-2+ GPL-2+ or BSD-like-with-advertising-clause libpng libpng OR Apache-2.0 OR BSD-3-clause | PNG library - runtime (version 1.6)                           | 1.6.37-3build5                  |
| libpolkit-agent-1-0:amd64       | Apache-2.0 LGPL-2.0+ | PolicyKit Authentication Agent API                            | 0.105-33                        |
| libpolkit-gobject-1-0:amd64     | Apache-2.0 LGPL-2.0+ | PolicyKit Authorization API                                   | 0.105-33                        |
| libpoppler-glib8:amd64          | Apache-2.0 GPL-2 GPL-2 or GPL-3 GPL-3 | PDF rendering library (GLib-based shared library)             | 22.02.0-2ubuntu0.1              |
| libpoppler118:amd64             | Apache-2.0 GPL-2 GPL-2 or GPL-3 GPL-3 | PDF rendering library                                         | 22.02.0-2ubuntu0.1              |
| libpopt0:amd64                  | GPL-2+ X-Consortium | lib for parsing cmdline parameters                            | 1.18-3build1                    |
| libprocps8:amd64                | GPL-2.0+ LGPL-2.0+ LGPL-2.1+ | library for accessing process information from /proc          | 2:3.3.17-6ubuntu2               |
| libprotobuf23:amd64             | Apache-2.0 BSD-3-Clause~Google Expat GPL-3 GPLWithACException Public-Domain Public-Domain or Expat | protocol buffers C++ library                                  | 3.12.4-1ubuntu7                 |
| libproxy1-plugin-gsettings:amd  | GPL LGPL-2.1 | automatic proxy configuration management library (GSettings   | 0.4.17-2                        |
| libproxy1-plugin-networkmanage  | GPL LGPL-2.1 | automatic proxy configuration management library (Network Ma  | 0.4.17-2                        |
| libproxy1v5:amd64               | GPL LGPL-2.1 | automatic proxy configuration management library (shared)     | 0.4.17-2                        |
| libpsl5:amd64                   | Chromium MIT | Library for Public Suffix List (shared libraries)             | 0.21.0-1.2build2                |
| libpthread-stubs0-dev:amd64     | unknown | pthread stubs not provided by native libc, development files  | 0.4-1build2                     |
| libpulse-mainloop-glib0:amd64   | GPL-2+ LGPL-2+ LGPL-2.1+ other | PulseAudio client libraries (glib support)                    | 1:15.99.1+dfsg1-1ubuntu2        |
| libpulse0:amd64                 | GPL-2+ LGPL-2+ LGPL-2.1+ other | PulseAudio client libraries                                   | 1:15.99.1+dfsg1-1ubuntu2        |
| libpulsedsp:amd64               | GPL-2+ LGPL-2+ LGPL-2.1+ other | PulseAudio OSS pre-load library                               | 1:15.99.1+dfsg1-1ubuntu2        |
| libpwquality-common             | GPL-2+ GPL-3+ libpwquality libpwquality or GPL-2+ | library for password quality checking and generation (data f  | 1.4.4-1build2                   |
| libpwquality1:amd64             | GPL-2+ GPL-3+ libpwquality libpwquality or GPL-2+ | library for password quality checking and generation          | 1.4.4-1build2                   |
| libpython3-stdlib:amd64         | GPL-compatible GPL-compatible licenses | interactive high-level object-oriented language (default pyt  | 3.10.6-1~22.04                  |
| libpython3.10:amd64             | GPL-2 | Shared Python runtime library (version 3.10)                  | 3.10.6-1~22.04                  |
| libpython3.10-minimal:amd64     | GPL-2 | Minimal subset of the Python language (version 3.10)          | 3.10.6-1~22.04                  |
| libpython3.10-stdlib:amd64      | GPL-2 | Interactive high-level object-oriented language (standard li  | 3.10.6-1~22.04                  |
| libqmi-glib5:amd64              | GFDL-NIV-1.3+ GPL-2+ LGPL-2.0+ | Support library to use the Qualcomm MSM Interface (QMI) prot  | 1.30.4-1                        |
| libqmi-proxy                    | GFDL-NIV-1.3+ GPL-2+ LGPL-2.0+ | Proxy to communicate with QMI ports                           | 1.30.4-1                        |
| libquadmath0:amd64              | Artistic GFDL-1.2 GPL GPL-2 GPL-3 LGPL | GCC Quad-Precision Math Library                               | 12.1.0-2ubuntu1~22.04           |
| libraqm0:amd64                  | MIT | Library for complex text layout                               | 0.7.0-4ubuntu1                  |
| libraw1394-11:amd64             | GPL LGPL | library for direct access to IEEE 1394 bus (aka FireWire)     | 2.1.2-2build2                   |
| libreadline8:amd64              | GFDL GPL-3 | GNU readline and history libraries, run-time libraries        | 8.1.2-1                         |
| librest-0.7-0:amd64             | FSF-aclocal FSF-INSTALL GPL-2+ LGPL-2.1 MIT with XConsortium exception | REST service access library                                   | 0.8.1-1.1build2                 |
| librsvg2-2:amd64                | 0BSD Apache-2.0 Apache-2.0, Apache-2.0 or Boost-1.0, Apache-2.0 or Expat, Apache-2.0 or Expat or 0BSD, Boost-1.0 BSD-2-clause BSD-2-clause, BSD-3-clause BSD-3-clause, CC-BY-3.0 CC-zero-waive-1.0-us Expat Expat, Expat or Unlicense, FSFAP LGPL-2+ MPL-2.0 MPL-2.0, OFL-1.1 Sun-permissive Sun-permissive, Unlicense zlib | SAX-based renderer library for SVG files (runtime)            | 2.52.5+dfsg-3                   |
| librsvg2-common:amd64           | 0BSD Apache-2.0 Apache-2.0, Apache-2.0 or Boost-1.0, Apache-2.0 or Expat, Apache-2.0 or Expat or 0BSD, Boost-1.0 BSD-2-clause BSD-2-clause, BSD-3-clause BSD-3-clause, CC-BY-3.0 CC-zero-waive-1.0-us Expat Expat, Expat or Unlicense, FSFAP LGPL-2+ MPL-2.0 MPL-2.0, OFL-1.1 Sun-permissive Sun-permissive, Unlicense zlib | SAX-based renderer library for SVG files (extra runtime)      | 2.52.5+dfsg-3                   |
| librtmp1:amd64                  | GPL-2 LGPL-2.1 | toolkit for RTMP streams (shared library)                     | 2.4+20151223.gitfa8646d.1-2bui  |
| librygel-core-2.6-2:amd64       | CC-BY-SA-3.0 LGPL-2+ | GNOME UPnP/DLNA services - core library                       | 0.40.3-1ubuntu2                 |
| librygel-db-2.6-2:amd64         | CC-BY-SA-3.0 LGPL-2+ | GNOME UPnP/DLNA services - db library                         | 0.40.3-1ubuntu2                 |
| librygel-renderer-2.6-2:amd64   | CC-BY-SA-3.0 LGPL-2+ | GNOME UPnP/DLNA services - renderer library                   | 0.40.3-1ubuntu2                 |
| librygel-server-2.6-2:amd64     | CC-BY-SA-3.0 LGPL-2+ | GNOME UPnP/DLNA services - server library                     | 0.40.3-1ubuntu2                 |
| libsamplerate0:amd64            | BSD-2-clause FSFAP GPL-2+ GPL-3+ | Audio sample rate conversion library                          | 0.2.2-1build1                   |
| libsane-common                  | Artistic GFDL-1.1 GPL-2 GPL-2+ GPL-2+ with sane exception GPL-3+ LGPL-2.1+ | API library for scanners -- documentation and support files   | 1.1.1-5                         |
| libsane1:amd64                  | Artistic GFDL-1.1 GPL-2 GPL-2+ GPL-2+ with sane exception GPL-3+ LGPL-2.1+ | API library for scanners                                      | 1.1.1-5                         |
| libsasl2-2:amd64                | BSD-2.2-clause BSD-2-clause BSD-3-clause BSD-3-clause-JANET BSD-3-clause-PADL BSD-4-clause BSD-4-clause-UC FSFULLR GPL-3+ IBM-as-is MIT-CMU MIT-Export MIT-OpenVision OpenLDAP OpenSSL RSA-MD SSLeay | Cyrus SASL - authentication abstraction library               | 2.1.27+dfsg2-3ubuntu1           |
| libsasl2-modules:amd64          | BSD-2.2-clause BSD-2-clause BSD-3-clause BSD-3-clause-JANET BSD-3-clause-PADL BSD-4-clause BSD-4-clause-UC FSFULLR GPL-3+ IBM-as-is MIT-CMU MIT-Export MIT-OpenVision OpenLDAP OpenSSL RSA-MD SSLeay | Cyrus SASL - pluggable authentication modules                 | 2.1.27+dfsg2-3ubuntu1           |
| libsasl2-modules-db:amd64       | BSD-2.2-clause BSD-2-clause BSD-3-clause BSD-3-clause-JANET BSD-3-clause-PADL BSD-4-clause BSD-4-clause-UC FSFULLR GPL-3+ IBM-as-is MIT-CMU MIT-Export MIT-OpenVision OpenLDAP OpenSSL RSA-MD SSLeay | Cyrus SASL - pluggable authentication modules (DB)            | 2.1.27+dfsg2-3ubuntu1           |
| libsasl2-modules-gssapi-mit:am  | BSD-2.2-clause BSD-2-clause BSD-3-clause BSD-3-clause-JANET BSD-3-clause-PADL BSD-4-clause BSD-4-clause-UC FSFULLR GPL-3+ IBM-as-is MIT-CMU MIT-Export MIT-OpenVision OpenLDAP OpenSSL RSA-MD SSLeay | Cyrus SASL - pluggable authentication modules (GSSAPI)        | 2.1.27+dfsg2-3ubuntu1           |
| libsbc1:amd64                   | GPL-2+ LGPL-2+ | Sub Band CODEC library - runtime                              | 1.5-3build2                     |
| libseccomp2:amd64               | LGPL-2.1 | high level interface to Linux seccomp filter                  | 2.5.3-2ubuntu2                  |
| libsecret-1-0:amd64             | Apache-2.0 BSD-3-clause GPL-2+ LGPL-2.1+ | Secret store                                                  | 0.20.5-2                        |
| libsecret-common                | Apache-2.0 BSD-3-clause GPL-2+ LGPL-2.1+ | Secret store (common files)                                   | 0.20.5-2                        |
| libselinux1:amd64               | GPL-2 LGPL-2.1 | SELinux runtime shared libraries                              | 3.3-1build2                     |
| libsemanage-common              | GPL LGPL | Common files for SELinux policy management libraries          | 3.3-1build2                     |
| libsemanage2:amd64              | GPL LGPL | SELinux policy management library                             | 3.3-1build2                     |
| libsensors-config               | GPL-2 LGPL-2.1 | lm-sensors configuration files                                | 1:3.6.0-7ubuntu1                |
| libsensors5:amd64               | GPL-2 LGPL-2.1 | library to read temperature/voltage/fan sensors               | 1:3.6.0-7ubuntu1                |
| libsepol2:amd64                 | GPL LGPL | SELinux library for manipulating binary security policies     | 3.3-1build1                     |
| libshout3:amd64                 | FSFUL FSFULLR FSFULLR~Makefile.in GPL-2+ GPL-2+ with Autoconf exception GPL-2+ with Libtool exception GPL-3+ GPL-3+~file GPL-3+~file with Autoconf exception LGPL-2+ NTP~Rushing X11 | MP3/Ogg Vorbis broadcast streaming library                    | 2.4.5-1build3                   |
| libsigc++-2.0-0v5:amd64         | GPL-2+ LGPL-2.1+ Public-Domain | type-safe Signal Framework for C++ - runtime                  | 2.10.4-2ubuntu3                 |
| libsixel-bin                    | Expat public-domain | DEC SIXEL graphics codec implementation (binary)              | 1.10.3-3                        |
| libsixel1:amd64                 | Expat public-domain | DEC SIXEL graphics codec implementation (runtime)             | 1.10.3-3                        |
| libslang2:amd64                 | GPL-2+ | S-Lang programming library - runtime version                  | 2.3.2-5build4                   |
| libsm-dev:amd64                 | unknown | X11 Session Management library (development headers)          | 2:1.2.3-1build2                 |
| libsm6:amd64                    | unknown | X11 Session Management library                                | 2:1.2.3-1build2                 |
| libsmartcols1:amd64             | BSD-2-clause BSD-3-clause BSD-4-clause GPL-2 GPL-2+ GPL-3+ LGPL LGPL-2+ LGPL-2.1+ LGPL-3+ MIT public-domain | smart column output alignment library                         | 2.37.2-4ubuntu3                 |
| libsmbclient:amd64              | BSD-3 GPL-3 GPL-3.0+ ISC LGPL-3.0+ MS-ADSL PostgreSQL | shared library for communication with SMB/CIFS servers        | 2:4.15.9+dfsg-0ubuntu0.2        |
| libsmbios-c2                    | GPL | Provide access to (SM)BIOS information -- dynamic library     | 2.4.3-1build1                   |
| libsnapd-glib1:amd64            | GPL-3+ LGPL-2 LGPL-2 or LGPL-3 LGPL-3 | GLib snapd library                                            | 1.60-0ubuntu1                   |
| libsndfile1:amd64               | Apache-2.0 BSD-3-clause FSFAP GPL-2+ gsm LGPL-2+ LGPL-2.1+ sun | Library for reading/writing audio files                       | 1.0.31-2build1                  |
| libsnmp-base                    | Artistic-1.0 BSD-3-clause BSD-LIKE GPL-2.0+ GPL-2.0+ or Artistic-1.0 | SNMP configuration script, MIBs and documentation             | 5.9.1+dfsg-1ubuntu2.2           |
| libsnmp40:amd64                 | Artistic-1.0 BSD-3-clause BSD-LIKE GPL-2.0+ GPL-2.0+ or Artistic-1.0 | SNMP (Simple Network Management Protocol) library             | 5.9.1+dfsg-1ubuntu2.2           |
| libsodium23:amd64               | BSD-2-clause CC0 GPL-2+ ISC MIT public-domain | Network communication, cryptography and signaturing library   | 1.0.18-1build2                  |
| libsonic0:amd64                 | Apache-2.0 | Simple library to speed up or slow down speech                | 0.2.0-11build1                  |
| libsoup-gnome2.4-1:amd64        | Expat LGPL-2+ LGPL-2.1+ | HTTP library implementation in C -- GNOME support library     | 2.74.2-3                        |
| libsoup2.4-1:amd64              | Expat LGPL-2+ LGPL-2.1+ | HTTP library implementation in C -- Shared library            | 2.74.2-3                        |
| libsoup2.4-common               | Expat LGPL-2+ LGPL-2.1+ | HTTP library implementation in C -- Common files              | 2.74.2-3                        |
| libsource-highlight-common      | Expat GPL-3+ | architecture-independent files for source highlighting libra  | 3.1.9-4.1build2                 |
| libsource-highlight4v5          | Expat GPL-3+ | source highlighting library                                   | 3.1.9-4.1build2                 |
| libsoxr0:amd64                  | LGPL-2.1+ permissive1 permissive2 Spherepack | High quality 1D sample-rate conversion library                | 0.1.3-4build2                   |
| libspa-0.2-modules:amd64        | Expat LGPL-2+ LGPL-2.1+ | libraries for the PipeWire multimedia server Simple Plugin A  | 0.3.48-1ubuntu2                 |
| libspectre1:amd64               | GPL-2+ | Library for rendering PostScript documents                    | 0.2.10-1                        |
| libspeechd2:amd64               | GFDL-NIV-1.2+ GFDL-NIV-1.2+ or GPL-2+ GPL-2+ GPL-3+ with tex exception LGPL-2.1+ other public-domain | Speech Dispatcher: Shared libraries                           | 0.11.1-1ubuntu2                 |
| libspeex1:amd64                 | unknown | The Speex codec runtime library                               | 1.2~rc1.2-1.1ubuntu3            |
| libspeexdsp1:amd64              | unknown | The Speex extended runtime library                            | 1.2~rc1.2-1.1ubuntu3            |
| libsqlite3-0:amd64              | GPL-2+ public-domain | SQLite 3 shared library                                       | 3.37.2-2                        |
| libss2:amd64                    | unknown | command-line interface parsing library                        | 1.46.5-2ubuntu1.1               |
| libssh-4:amd64                  | BSD-2-clause BSD-3-clause LGPL-2.1 LGPL-2.1+~OpenSSL LGPL-2.1+~OpenSSL or BSD-2-clause or BSD-3-clause public-domain | tiny C SSH library (OpenSSL flavor)                           | 0.9.6-2build1                   |
| libssl3:amd64                   | Apache-2.0 Artistic Artistic or GPL-1+ GPL-1+ | Secure Sockets Layer toolkit - shared libraries               | 3.0.2-0ubuntu1.7                |
| libstartup-notification0:amd64  | LGPL-2 | library for program launch feedback (shared library)          | 0.12-6build2                    |
| libstdc++-11-dev:amd64          | Artistic GFDL-1.2 GPL GPL-2 GPL-3 LGPL | GNU Standard C++ Library v3 (development files)               | 11.3.0-1ubuntu1~22.04           |
| libstdc++6:amd64                | Artistic GFDL-1.2 GPL GPL-2 GPL-3 LGPL | GNU Standard C++ Library v3                                   | 12.1.0-2ubuntu1~22.04           |
| libstemmer0d:amd64              | BSD-3-snowball | Snowball stemming algorithms for use in Information Retrieva  | 2.2.0-1build1                   |
| libsynctex2:amd64               | BSD license MIT license | TeX Live: SyncTeX parser library                              | 2021.20210626.59705-1build1     |
| libsystemd0:amd64               | CC0-1.0 Expat GPL-2 GPL-2+ GPL-2 with Linux-syscall-note exception LGPL-2.1+ public-domain | systemd utility library                                       | 249.11-0ubuntu3.6               |
| libtag1v5:amd64                 | BSD-2-clause LGPL-2.1 LGPL-2.1 or MPL-1.1 MPL-1.1 | audio meta-data library                                       | 1.11.1+dfsg.1-3ubuntu3          |
| libtag1v5-vanilla:amd64         | BSD-2-clause LGPL-2.1 LGPL-2.1 or MPL-1.1 MPL-1.1 | audio meta-data library - vanilla flavour                     | 1.11.1+dfsg.1-3ubuntu3          |
| libtalloc2:amd64                | BSD-3 GPL-3.0+ ISC LGPL-3.0+ PostgreSQL | hierarchical pool based memory allocator                      | 2.3.3-2build1                   |
| libtasn1-6:amd64                | GFDL-1.3 GPL-3 LGPL LGPL-2.1 | Manage ASN.1 structures (runtime)                             | 4.18.0-4build1                  |
| libtdb1:amd64                   | BSD-3 GPL-3.0+ ISC LGPL-3.0+ PostgreSQL | Trivial Database - shared library                             | 1.4.5-2build1                   |
| libteamdctl0:amd64              | BSD-3-clause BSD-3-clause or GPL-2 GPL-2 LGPL-2.1+ | library for communication with `teamd` process                | 1.31-1build2                    |
| libtermkey1:amd64               | MIT | library for processing keyboard input                         | 0.22-1                          |
| libtevent0:amd64                | BSD-3 GPL-3.0+ ISC LGPL-3.0+ PostgreSQL | talloc-based event loop library - shared library              | 0.11.0-1build1                  |
| libtext-charwidth-perl          | Artistic Artistic or GPL-1+ GPL-1+ | get display widths of characters on the terminal              | 0.04-10build3                   |
| libtext-iconv-perl              | Artistic Artistic or GPL-1+ GPL-1+ | module to convert between character sets in Perl              | 1.7-7build3                     |
| libtext-wrapi18n-perl           | Artistic Artistic or GPL-1+ GPL-1+ | internationalized substitute of Text::Wrap                    | 0.06-9                          |
| libthai-data                    | GPL-2+ LGPL-2.1+ | Data files for Thai language support library                  | 0.1.29-1build1                  |
| libthai0:amd64                  | GPL-2+ LGPL-2.1+ | Thai language support library                                 | 0.1.29-1build1                  |
| libtheora0:amd64                | BSD-3-Clause | Theora Video Compression Codec                                | 1.1.1+dfsg.1-15ubuntu4          |
| libtie-ixhash-perl              | Artistic Artistic or GPL-1+ GPL-1+ | Perl module to order associative arrays                       | 1.23-2.1                        |
| libtiff5:amd64                  | Hylafax | Tag Image File Format (TIFF) library                          | 4.3.0-6ubuntu0.1                |
| libtimedate-perl                | Artistic Artistic or GPL-1+ GPL-1+ | collection of modules to manipulate date/time information     | 2.3300-2                        |
| libtinfo6:amd64                 | BSD-3-clause MIT/X11 X11 | shared low-level terminfo library for terminal handling       | 6.3-2                           |
| libtirpc-common                 | __AUTO_PERMISSIVE__ BSD-2-Clause BSD-3-Clause BSD-4-Clause GPL-2 LGPL-2.1+ PERMISSIVE | transport-independent RPC library - common files              | 1.3.2-2ubuntu0.1                |
| libtirpc-dev:amd64              | __AUTO_PERMISSIVE__ BSD-2-Clause BSD-3-Clause BSD-4-Clause GPL-2 LGPL-2.1+ PERMISSIVE | transport-independent RPC library - development files         | 1.3.2-2ubuntu0.1                |
| libtirpc3:amd64                 | __AUTO_PERMISSIVE__ BSD-2-Clause BSD-3-Clause BSD-4-Clause GPL-2 LGPL-2.1+ PERMISSIVE | transport-independent RPC library                             | 1.3.2-2ubuntu0.1                |
| libtotem-plparser-common        | GPL LGPL | Totem Playlist Parser library - common files                  | 3.26.6-1build1                  |
| libtotem-plparser18:amd64       | GPL LGPL | Totem Playlist Parser library - runtime files                 | 3.26.6-1build1                  |
| libtracker-sparql-3.0-0:amd64   | GPL-2.0+ LGPL-2.0+ LGPL-2.1+ | metadata database, indexer and search tool - library          | 3.3.0-1                         |
| libtree-sitter0:amd64           | Expat Unicode | incremental parsing system for programming tools              | 0.20.3-1                        |
| libtry-tiny-perl                | Expat | module providing minimalistic try/catch                       | 0.31-1                          |
| libtsan0:amd64                  | Artistic GFDL-1.2 GPL GPL-2 GPL-3 LGPL | ThreadSanitizer -- a Valgrind-based detector of data races (  | 11.3.0-1ubuntu1~22.04           |
| libtss2-esys-3.0.2-0:amd64      | BSD-2-clause GPL-3 TCG | TPM2 Software stack library - TSS and TCTI libraries          | 3.2.0-1ubuntu1                  |
| libtss2-mu0:amd64               | BSD-2-clause GPL-3 TCG | TPM2 Software stack library - TSS and TCTI libraries          | 3.2.0-1ubuntu1                  |
| libtss2-sys1:amd64              | BSD-2-clause GPL-3 TCG | TPM2 Software stack library - TSS and TCTI libraries          | 3.2.0-1ubuntu1                  |
| libtss2-tcti-cmd0:amd64         | BSD-2-clause GPL-3 TCG | TPM2 Software stack library - TSS and TCTI libraries          | 3.2.0-1ubuntu1                  |
| libtss2-tcti-device0:amd64      | BSD-2-clause GPL-3 TCG | TPM2 Software stack library - TSS and TCTI libraries          | 3.2.0-1ubuntu1                  |
| libtss2-tcti-mssim0:amd64       | BSD-2-clause GPL-3 TCG | TPM2 Software stack library - TSS and TCTI libraries          | 3.2.0-1ubuntu1                  |
| libtss2-tcti-swtpm0:amd64       | BSD-2-clause GPL-3 TCG | TPM2 Software stack library - TSS and TCTI libraries          | 3.2.0-1ubuntu1                  |
| libtwolame0:amd64               | LGPL-2+ | MPEG Audio Layer 2 encoding library                           | 0.4.0-2build2                   |
| libtypes-serialiser-perl        | Artistic Artistic or GPL-1+ GPL-1+ | module providing simple data types for common serialisation   | 1.01-1                          |
| libu2f-udev                     | L-3+ LGPL-2.1+ | Universal 2nd Factor (U2F) — transitional package          GP | 1.1.10-3build2                  |
| libubsan1:amd64                 | Artistic GFDL-1.2 GPL GPL-2 GPL-3 LGPL | UBSan -- undefined behaviour sanitizer (runtime)              | 12.1.0-2ubuntu1~22.04           |
| libuchardet0:amd64              | GPL-2+ LGPL-2.1+ MPL-1.1 MPL-1.1 or GPL-2+ or LGPL-2.1+ | universal charset detection library - shared library          | 0.0.7-1build2                   |
| libudev1:amd64                  | CC0-1.0 Expat GPL-2 GPL-2+ GPL-2 with Linux-syscall-note exception LGPL-2.1+ public-domain | libudev shared library                                        | 249.11-0ubuntu3.6               |
| libudisks2-0:amd64              | GPL-2+ LGPL-2+ | GObject based library to access udisks2                       | 2.9.4-1ubuntu2                  |
| libunibilium4:amd64             | LGPL-3+ MIT | simple, self-contained terminfo library                       | 2.1.0-1                         |
| libunistring2:amd64             | FreeSoftware GFDL-1.2+ GPL-2+ GPL-2+ with distribution exception GPL-3+ GPL-3+ or GFDL-1.2+ LGPL-3+ LGPL-3+ or GPL-2+ MIT | Unicode string library for C                                  | 1.0-1                           |
| libunity-protocol-private0:amd  | GPL-3 LGPL-3 | binding to get places into the launcher - private library     | 7.1.4+19.04.20190319-6build1    |
| libunity-scopes-json-def-deskt  | GPL-3 LGPL-3 | binding to get places into the launcher - desktop def file    | 7.1.4+19.04.20190319-6build1    |
| libunity9:amd64                 | GPL-3 LGPL-3 | binding to get places into the launcher - shared library      | 7.1.4+19.04.20190319-6build1    |
| libunwind8:amd64                | Expat GPL-2+ | library to determine the call-chain of a program - runtime    | 1.3.2-2build2                   |
| libupower-glib3:amd64           | GFDL-1.1+ GPL-2+ | abstraction for power management - shared library             | 0.99.17-1                       |
| liburi-perl                     | Artistic Artistic or GPL-1+ GPL-1+ | module to manipulate and access URI strings                   | 5.10-1                          |
| libusb-1.0-0:amd64              | GPL-2 LGPL-2.1 | userspace USB programming library                             | 2:1.0.25-1ubuntu2               |
| libusbmuxd6:amd64               | GPL-2+ GPL-3+ LGPL-2.1+ | USB multiplexor daemon for iPhone and iPod Touch devices - l  | 2.0.2-3build2                   |
| libutempter0:amd64              | LGPL-2.1 | privileged helper for utmp/wtmp updates (runtime)             | 1.2.1-2build2                   |
| libuuid1:amd64                  | BSD-2-clause BSD-3-clause BSD-4-clause GPL-2 GPL-2+ GPL-3+ LGPL LGPL-2+ LGPL-2.1+ LGPL-3+ MIT public-domain | Universally Unique ID library                                 | 2.37.2-4ubuntu3                 |
| libuv1:amd64                    | Apache-2.0 BSD-1-clause BSD-2-clause BSD-3-clause CC-BY-4.0 Expat GPL3+ with autoconf exception ISC | asynchronous event notification library - runtime library     | 1.43.0-1                        |
| libv4l-0:amd64                  | GPL-2 LGPL | Collection of video4linux support libraries                   | 1.22.1-2build1                  |
| libv4lconvert0:amd64            | GPL-2 LGPL | Video4linux frame format conversion library                   | 1.22.1-2build1                  |
| libvisual-0.4-0:amd64           | LGPL-2+ LGPL-2.1+ | audio visualization framework                                 | 0.4.0-17build2                  |
| libvncserver1:amd64             | AT&T BSD-3 Expat GPL-2 GPL-2+ GPL-3 wxWin X-Consortium | API to write one's own VNC server                             | 0.9.13+dfsg-3build2             |
| libvolume-key1                  | GPL-2 GPL-2+ MPL-1.1 or GPL-2+ or LGPL-2.1+ | Library for manipulating storage encryption keys and passphr  | 0.3.12-3.1build3                |
| libvorbis0a:amd64               | BSD-3-Clause RFC-special | decoder library for Vorbis General Audio Compression Codec    | 1.3.7-1build2                   |
| libvorbisenc2:amd64             | BSD-3-Clause RFC-special | encoder library for Vorbis General Audio Compression Codec    | 1.3.7-1build2                   |
| libvorbisfile3:amd64            | BSD-3-Clause RFC-special | high-level API for Vorbis General Audio Compression Codec     | 1.3.7-1build2                   |
| libvpx7:amd64                   | BSD-3-Clause ISC public-domain | VP8 and VP9 video codec (shared library)                      | 1.11.0-2ubuntu2                 |
| libvte-2.91-0:amd64             | Expat GPL-3+ LGPL-3+ | Terminal emulator widget for GTK+ 3.0 - runtime files         | 0.68.0-1                        |
| libvte-2.91-common              | Expat GPL-3+ LGPL-3+ | Terminal emulator widget for GTK+ 3.0 - common files          | 0.68.0-1                        |
| libvterm0:amd64                 | MIT permissive | abstract terminal library                                     | 0.1.4-1                         |
| libvulkan1:amd64                | Apache-2.0 MIT MIT or Apache-2.0 | Vulkan loader library                                         | 1.3.204.1-2                     |
| libwacom-bin                    | MIT permissive | Wacom model feature query library -- binaries                 | 2.2.0-1                         |
| libwacom-common                 | MIT permissive | Wacom model feature query library (common files)              | 2.2.0-1                         |
| libwacom9:amd64                 | MIT permissive | Wacom model feature query library                             | 2.2.0-1                         |
| libwavpack1:amd64               | BSD-2-clause BSD-3-clause public-domain | audio codec (lossy and lossless) - library                    | 5.4.0-1build2                   |
| libwayland-client0:amd64        | X11 | wayland compositor infrastructure - client library            | 1.20.0-1ubuntu0.1               |
| libwayland-cursor0:amd64        | X11 | wayland compositor infrastructure - cursor library            | 1.20.0-1ubuntu0.1               |
| libwayland-egl1:amd64           | X11 | wayland compositor infrastructure - EGL library               | 1.20.0-1ubuntu0.1               |
| libwayland-server0:amd64        | X11 | wayland compositor infrastructure - server library            | 1.20.0-1ubuntu0.1               |
| libwbclient0:amd64              | BSD-3 GPL-3 GPL-3.0+ ISC LGPL-3.0+ MS-ADSL PostgreSQL | Samba winbind client library                                  | 2:4.15.9+dfsg-0ubuntu0.2        |
| libwebkit2gtk-4.0-37:amd64      | AFL-2.0 AFL-2.0 or LGPL-2+ Apache-2.0 BSD-2-clause BSD-2-clause or BSL-1.0 BSD-2-clause or Expat BSD-3-clause-adam-barth BSD-3-clause-apple BSD-3-clause-canon BSD-3-clause-code-aurora BSD-3-clause-copyright-holder BSD-3-clause-ericsson BSD-3-clause-google BSD-3-clause-jochen-kalmbach BSD-3-clause-microsoft BSD-3-clause-motorola BSD-3-clause-opera BSD-valgrind BSL-1.0 Expat GPL-2+ GPL-2+ or LGPL-2.1+ or MPL-1.1 GPL-2+ with Bison exception GPL-3+ GPL-3+ with Bison exception ISC LGPL-2 LGPL-2+ LGPL-2.1 LGPL-2.1+ LGPL-2.1+ or MPL-1.1 LGPL-2+ or MPL-1.1 MPL-1.1 MPL-2.0 public-domain | Web content engine library for GTK                            | 2.36.8-0ubuntu0.22.04.1         |
| libwebp7:amd64                  | Apache-2.0 | Lossy compression of digital photographic images              | 1.2.2-2                         |
| libwebpdemux2:amd64             | Apache-2.0 | Lossy compression of digital photographic images.             | 1.2.2-2                         |
| libwebpmux3:amd64               | Apache-2.0 | Lossy compression of digital photographic images              | 1.2.2-2                         |
| libwebrtc-audio-processing1:am  | BSD-3-clause | AudioProcessing module from the WebRTC project.               | 0.3.1-0ubuntu5                  |
| libwhoopsie-preferences0        | GPL-3 | Ubuntu error tracker submission settings - shared library     | 23                              |
| libwhoopsie0:amd64              | Apache-2.0 GPL-3 | Ubuntu error tracker submission - shared library              | 0.2.77                          |
| libwinpr2-2:amd64               | Apache-2.0 Apache-2.0 or BSD-2-clause or BSD-3-clause or BSL-1.0 or Expat or ISC or X11 or zlib/libpng or public-domain BSD-2-clause BSD-3-clause BSL-1.0 Expat FMILIB ISC LGPL-2.1+ NTP~legal-disclaimer public-domain X11 zlib/libpng | Windows Portable Runtime library                              | 2.6.1+dfsg1-3ubuntu2.2          |
| libwmf-0.2-7:amd64              | AGPL-3 AGPL-3 with Font exception GD ISC LGPL-2+ public-domain | Windows metafile conversion library                           | 0.2.12-5ubuntu1                 |
| libwmf-0.2-7-gtk                | AGPL-3 AGPL-3 with Font exception GD ISC LGPL-2+ public-domain | Windows metafile conversion GTK pixbuf plugin                 | 0.2.12-5ubuntu1                 |
| libwmf0.2-7-gtk:amd64           | AGPL-3 AGPL-3 with Font exception GD ISC LGPL-2+ public-domain | Windows metafile conversion GTK pixbuf plugin - transitional  | 0.2.12-5ubuntu1                 |
| libwmflite-0.2-7:amd64          | AGPL-3 AGPL-3 with Font exception GD ISC LGPL-2+ public-domain | Windows metafile conversion lite library                      | 0.2.12-5ubuntu1                 |
| libwnck-3-0:amd64               | LGPL-2+ | Window Navigator Construction Kit - runtime files             | 40.1-1                          |
| libwnck-3-common                | LGPL-2+ | Window Navigator Construction Kit - common files              | 40.1-1                          |
| libwoff1:amd64                  | Expat | library for converting fonts to WOFF 2.0                      | 1.0.2-1build4                   |
| libwrap0:amd64                  | unknown | Wietse Venema's TCP wrappers library                          | 7.6.q-31build2                  |
| libwww-perl                     | Artistic Artistic or GPL-1+ GPL-1+ | simple and consistent interface to the world-wide web         | 6.61-1                          |
| libwww-robotrules-perl          | Artistic Artistic or GPL-1+ GPL-1+ | database of robots.txt-derived permissions                    | 6.02-1                          |
| libx11-6:amd64                  | MIT license | X11 client-side library                                       | 2:1.7.5-1                       |
| libx11-data                     | MIT license | X11 client-side library                                       | 2:1.7.5-1                       |
| libx11-dev:amd64                | MIT license | X11 client-side library (development headers)                 | 2:1.7.5-1                       |
| libx11-protocol-perl            | Artistic Artistic or GPL-1+ Expat GPL-1+ | Perl module for the X Window System Protocol, version 11      | 0.56-7.1                        |
| libx11-xcb1:amd64               | MIT license | Xlib/XCB interface library                                    | 2:1.7.5-1                       |
| libx265-199:amd64               | Expat GPL-2+ ISC LGPL-2.1+ | H.265/HEVC video stream encoder (shared library)              | 3.5-2                           |
| libxau-dev:amd64                | unknown | X11 authorisation library (development headers)               | 1:1.0.9-1build5                 |
| libxau6:amd64                   | unknown | X11 authorisation library                                     | 1:1.0.9-1build5                 |
| libxaw7:amd64                   | unknown | X11 Athena Widget library                                     | 2:1.0.14-1                      |
| libxcb-cursor0:amd64            | MIT/X Consortium License | utility libraries for X C Binding -- cursor                   | 0.1.1-4ubuntu1                  |
| libxcb-dri2-0:amd64             | unknown | X C Binding, dri2 extension                                   | 1.14-3ubuntu3                   |
| libxcb-dri3-0:amd64             | unknown | X C Binding, dri3 extension                                   | 1.14-3ubuntu3                   |
| libxcb-glx0:amd64               | unknown | X C Binding, glx extension                                    | 1.14-3ubuntu3                   |
| libxcb-icccm4:amd64             | GPL-2+ MIT/X Consortium License | utility libraries for X C Binding -- icccm                    | 0.4.1-1.1build2                 |
| libxcb-image0:amd64             | GPL-2+ MIT/X11 | utility libraries for X C Binding -- image                    | 0.4.0-2                         |
| libxcb-keysyms1:amd64           | GPL-2+ MIT/X11 | utility libraries for X C Binding -- keysyms                  | 0.4.0-1build3                   |
| libxcb-present0:amd64           | unknown | X C Binding, present extension                                | 1.14-3ubuntu3                   |
| libxcb-randr0:amd64             | unknown | X C Binding, randr extension                                  | 1.14-3ubuntu3                   |
| libxcb-render-util0:amd64       | GPL-2+ MIT/X Consortium License | utility libraries for X C Binding -- render-util              | 0.3.9-1build3                   |
| libxcb-render0:amd64            | unknown | X C Binding, render extension                                 | 1.14-3ubuntu3                   |
| libxcb-res0:amd64               | unknown | X C Binding, res extension                                    | 1.14-3ubuntu3                   |
| libxcb-shape0:amd64             | unknown | X C Binding, shape extension                                  | 1.14-3ubuntu3                   |
| libxcb-shm0:amd64               | unknown | X C Binding, shm extension                                    | 1.14-3ubuntu3                   |
| libxcb-sync1:amd64              | unknown | X C Binding, sync extension                                   | 1.14-3ubuntu3                   |
| libxcb-util1:amd64              | GPL-2+ MIT | utility libraries for X C Binding -- atom, aux and event      | 0.4.0-1build2                   |
| libxcb-xfixes0:amd64            | unknown | X C Binding, xfixes extension                                 | 1.14-3ubuntu3                   |
| libxcb-xinerama0:amd64          | unknown | X C Binding, xinerama extension                               | 1.14-3ubuntu3                   |
| libxcb-xkb1:amd64               | unknown | X C Binding, XKEYBOARD extension                              | 1.14-3ubuntu3                   |
| libxcb-xrm0:amd64               | MIT/X | utility functions for the X resource manager                  | 1.0-3                           |
| libxcb-xv0:amd64                | unknown | X C Binding, xv extension                                     | 1.14-3ubuntu3                   |
| libxcb1:amd64                   | unknown | X C Binding                                                   | 1.14-3ubuntu3                   |
| libxcb1-dev:amd64               | unknown | X C Binding, development files                                | 1.14-3ubuntu3                   |
| libxcomposite1:amd64            | unknown | X11 Composite extension library                               | 1:0.4.5-1build2                 |
| libxcursor1:amd64               | unknown | X cursor management library                                   | 1:1.2.0-2build4                 |
| libxcvt0:amd64                  | MIT NTP | VESA CVT standard timing modelines generator -- shared libra  | 0.1.1-3                         |
| libxdamage1:amd64               | unknown | X11 damaged region extension library                          | 1:1.1.5-2build2                 |
| libxdmcp-dev:amd64              | unknown | X11 authorisation library (development headers)               | 1:1.1.3-0ubuntu5                |
| libxdmcp6:amd64                 | unknown | X11 Display Manager Control Protocol library                  | 1:1.1.3-0ubuntu5                |
| libxext6:amd64                  | unknown | X11 miscellaneous extension library                           | 2:1.3.4-1build1                 |
| libxfixes3:amd64                | unknown | X11 miscellaneous 'fixes' extension library                   | 1:6.0.0-1                       |
| libxfont2:amd64                 | unknown | X11 font rasterisation library                                | 1:2.0.5-1build1                 |
| libxft2:amd64                   | unknown | FreeType-based font drawing library for X                     | 2.3.4-1                         |
| libxi6:amd64                    | unknown | X11 Input extension library                                   | 2:1.8-1build1                   |
| libxinerama1:amd64              | unknown | X11 Xinerama extension library                                | 2:1.1.4-3                       |
| libxkbcommon-x11-0:amd64        | unknown | library to create keymaps with the XKB X11 protocol           | 1.4.0-1                         |
| libxkbcommon0:amd64             | unknown | library interface to the XKB compiler - shared library        | 1.4.0-1                         |
| libxkbfile1:amd64               | unknown | X11 keyboard file manipulation library                        | 1:1.1.0-1build3                 |
| libxkbregistry0:amd64           | unknown | library to query available RMLVO                              | 1.4.0-1                         |
| libxklavier16:amd64             | LGPL-2 | X Keyboard Extension high-level API                           | 5.4-4build2                     |
| libxml-parser-perl:amd64        | Artistic Artistic or GPL-1+ GPL-1+ | Perl module for parsing XML files                             | 2.46-3build1                    |
| libxml-twig-perl                | Artistic Artistic or GPL-1+ Artistic or GPL-2+ GPL-1+ GPL-2 GPL-2+ | Perl module for processing huge XML documents in tree mode    | 1:3.52-1                        |
| libxml-xpathengine-perl         | Artistic Artistic or GPL-1+ GPL-1+ | re-usable XPath engine for DOM-like trees                     | 0.14-1                          |
| libxml2:amd64                   | ISC MIT-1 | GNOME XML library                                             | 2.9.13+dfsg-1ubuntu0.1          |
| libxmlb2:amd64                  | CC0-1.0 LGPL-2.1+ | Binary XML library                                            | 0.3.6-2build1                   |
| libxmu6:amd64                   | unknown | X11 miscellaneous utility library                             | 2:1.1.3-3                       |
| libxmuu1:amd64                  | unknown | X11 miscellaneous micro-utility library                       | 2:1.1.3-3                       |
| libxpm4:amd64                   | unknown | X11 pixmap library                                            | 1:3.5.12-1build2                |
| libxrandr2:amd64                | unknown | X11 RandR extension library                                   | 2:1.5.2-1build1                 |
| libxrender1:amd64               | unknown | X Rendering Extension client library                          | 1:0.9.10-1build4                |
| libxres1:amd64                  | unknown | X11 Resource extension library                                | 2:1.2.1-1                       |
| libxshmfence1:amd64             | unknown | X shared memory fences - shared library                       | 1.3-1build4                     |
| libxslt1.1:amd64                | unknown | XSLT 1.0 processing library - runtime library                 | 1.1.34-4ubuntu0.22.04.1         |
| libxss1:amd64                   | unknown | X11 Screen Saver extension library                            | 1:1.2.3-1build2                 |
| libxt-dev:amd64                 | unknown | X11 toolkit intrinsics library (development headers)          | 1:1.2.1-1                       |
| libxt6:amd64                    | unknown | X11 toolkit intrinsics library                                | 1:1.2.1-1                       |
| libxtables12:amd64              | Artistic custom GPL-2 GPL-2+ | netfilter xtables library                                     | 1.8.7-1ubuntu5                  |
| libxtst6:amd64                  | unknown | X11 Testing -- Record extension library                       | 2:1.2.3-1build4                 |
| libxv1:amd64                    | unknown | X11 Video extension library                                   | 2:1.0.11-1build2                |
| libxxf86dga1:amd64              | unknown | X11 Direct Graphics Access extension library                  | 2:1.1.5-0ubuntu3                |
| libxxf86vm1:amd64               | unknown | X11 XFree86 video mode extension library                      | 1:1.1.4-1build3                 |
| libxxhash0:amd64                | BSD-2-clause GPL-2 | shared library for xxhash                                     | 0.8.1-1                         |
| libyajl2:amd64                  | ISC | Yet Another JSON Library                                      | 2.1.0-3build2                   |
| libyaml-0-2:amd64               | Expat permissive | Fast YAML 1.1 parser and emitter library                      | 0.2.2-1build2                   |
| libyelp0:amd64                  | Apache-2.0 GPL-2+ | Library for the GNOME help browser                            | 42.1-1                          |
| libzstd1:amd64                  | BSD-3-clause Expat GPL-2 zlib | fast lossless compression algorithm                           | 1.4.8+dfsg-3build1              |
| linux-base                      | GPL-2 | Linux image base package                                      | 4.5ubuntu9                      |
| linux-headers-5.15.0-52         | GPL-2 | Header files related to Linux kernel version 5.15.0           | 5.15.0-52.58                    |
| linux-headers-5.15.0-52-generi  | GPL-2 | Linux kernel headers for version 5.15.0 on 64 bit x86 SMP     | 5.15.0-52.58                    |
| linux-headers-generic           | GPL | Generic Linux kernel headers                                  | 5.15.0.52.52                    |
| linux-image-5.15.0-52-generic   | GPL-2 | Signed kernel image generic                                   | 5.15.0-52.58                    |
| linux-libc-dev:amd64            | GPL-2 | Linux Kernel Headers for development                          | 5.15.0-52.58                    |
| linux-modules-5.15.0-52-generi  | GPL-2 | Linux kernel extra modules for version 5.15.0 on 64 bit x86   | 5.15.0-52.58                    |
| linux-modules-extra-5.15.0-52-  | GPL-2 | Linux kernel extra modules for version 5.15.0 on 64 bit x86   | 5.15.0-52.58                    |
| linux-sound-base                | GPL-2 | base package for ALSA and OSS sound systems                   | 1.0.25+dfsg-0ubuntu7            |
| locales                         | GFDL-1.3 GPL-2 LGPL-2.1 | GNU C Library: National Language (locale) data [support]      | 2.35-0ubuntu3.1                 |
| login                           | GPL-2 | system login tools                                            | 1:4.8.1-2ubuntu2                |
| logrotate                       | BSD-3-Clause GPL-2 GPL-3+ | Log rotation utility                                          | 3.19.0-1ubuntu1.1               |
| logsave                         | GPL-2 LGPL-2 | save the output of a command in a log file                    | 1.46.5-2ubuntu1.1               |
| lsb-base                        | BSD-3-clause GPL-2 | Linux Standard Base init script functionality                 | 11.1.0ubuntu4                   |
| lsb-release                     | BSD-3-clause GPL-2 | Linux Standard Base version reporting utility                 | 11.1.0ubuntu4                   |
| lshw                            | GPL | information about hardware configuration                      | 02.19.git.2021.06.19.996aaad9c  |
| lsof                            | BSD-4-clause GPL-2+ LGPL-2+ Purdue sendmail | utility to list open files                                    | 4.93.2+dfsg-1.1build2           |
| lto-disabled-list               | GPL-2+ | list of packages not to build with LTO                        | 24                              |
| lua-luv:amd64                   | Apache-2.0 BSD-3-clause Expat | libuv bindings for Lua                                        | 1.36.0-0-1                      |
| lynx                            | GPL-2 | classic non-graphical (text-mode) web browser                 | 2.9.0dev.10-1                   |
| lynx-common                     | GPL-2 | shared files for lynx package                                 | 2.9.0dev.10-1                   |
| m17n-db                         | GPL-3 LGPL-2.1 | multilingual text processing library - database               | 1.8.0-3                         |
| mailcap                         | ad-hoc Bellcore | Debian's mailcap system, and support programs                 | 3.70+nmu1ubuntu1                |
| make                            | GPL-3+ | utility for directing compilation                             | 4.3-4.1build1                   |
| man-db                          | GPL-2+ GPL-3+ | tools for reading manual pages                                | 2.10.2-1                        |
| manpages                        | BSD-3-clause BSD-4-clause Expat freely-redistributable GPL-2 GPL-2+ henry-spencer-regex LDPv1 public-domain verbatim | Manual pages about using a GNU/Linux system                   | 5.10-1ubuntu1                   |
| manpages-dev                    | BSD-3-clause BSD-4-clause Expat freely-redistributable GPL-2 GPL-2+ henry-spencer-regex LDPv1 public-domain verbatim | Manual pages about using GNU/Linux for development            | 5.10-1ubuntu1                   |
| manpages-posix                  | Other public-domain | Manual pages about using POSIX system                         | 2017a-2                         |
| manpages-posix-dev              | Other public-domain | Manual pages about using a POSIX system for development       | 2017a-2                         |
| mawk                            | GPL-2 | Pattern scanning and text processing language                 | 1.3.4.20200120-3                |
| media-types                     | ad-hoc | List of standard media types and their usual file extension   | 7.0.0                           |
| memtest86+                      | Expat GPL-2 | thorough real-mode memory tester                              | 5.31b+dfsg-4                    |
| mime-support                    | ad-hoc | transitional package                                          | 3.66                            |
| mobile-broadband-provider-info  | public-domain | database of mobile broadband service providers                | 20220315-1                      |
| modemmanager                    | GPL-2.0 GPL-2.0+ GPL-3.0+ LGPL-2.0+ | D-Bus service for managing modems                             | 1.18.6-1                        |
| mokutil                         | GPL-3+ with OpenSSL exception | tools for manipulating machine owner keys                     | 0.4.0-1ubuntu2                  |
| mosh                            | GPL-2+ GPL-3+ with Autoconf exception GPL-3+ with OpenSSL exception ISC public-domain | Mobile shell that supports roaming and intelligent local ech  | 1.3.2-2.1ubuntu1                |
| mount                           | BSD-2-clause BSD-3-clause BSD-4-clause GPL-2 GPL-2+ GPL-3+ LGPL LGPL-2+ LGPL-2.1+ LGPL-3+ MIT public-domain | tools for mounting and manipulating filesystems               | 2.37.2-4ubuntu3                 |
| mousetweaks                     | GFDL-1.3 GPL-3 | mouse accessibility enhancements for the GNOME desktop        | 3.32.0-3build2                  |
| mtr-tiny                        | GPL-2+ | Full screen ncurses traceroute tool                           | 0.95-1                          |
| mutter-common                   | DEC-BSD-variant Expat GPL-2+ GPL-3+ LGPL-2+ LGPL-2.1+ NTP-BSD-variant OpenGroup-BSD-variant SGI-B-2.0 WRF-BSD-variant | shared files for the Mutter window manager                    | 42.5-0ubuntu1                   |
| nano                            | GFDL-NIV+ GFDL-NIV+ or GPL-3+ GPL-3+ | small, friendly text editor inspired by Pico                  | 6.2-1                           |
| nautilus                        | GPL-3 | file manager and graphical shell for GNOME                    | 1:42.2-0ubuntu1                 |
| nautilus-data                   | GPL-3 | data files for nautilus                                       | 1:42.2-0ubuntu1                 |
| nautilus-extension-gnome-termi  | GFDL-NIV-1.3 GFDL-NIV-1.3 or GPL-3+ GPL-3+ LGPL-3+ | GNOME terminal emulator application - Nautilus extension      | 3.44.0-1ubuntu1                 |
| nautilus-sendto                 | GPL-2 LGPL-2.1 | easily send files via email from within Nautilus              | 3.8.6-4                         |
| ncurses-base                    | BSD-3-clause MIT/X11 X11 | basic terminal type definitions                               | 6.3-2                           |
| ncurses-bin                     | BSD-3-clause MIT/X11 X11 | terminal-related programs and man pages                       | 6.3-2                           |
| ncurses-term                    | BSD-3-clause MIT/X11 X11 | additional terminal type definitions                          | 6.3-2                           |
| neofetch                        | Expat | Shows Linux System Information with Distribution Logo         | 7.1.0-3                         |
| neovim                          | Apache Apache-with-Exception BSD-2-clause BSD-3-clause clint EDL-1 Expat GPL-2 GPL-2+ GPL-3+ ISC LGPL-2.1+ Unicode Vim Vim-Regexp | heavily refactored vim fork                                   | 0.6.1-3                         |
| neovim-runtime                  | Apache Apache-with-Exception BSD-2-clause BSD-3-clause clint EDL-1 Expat GPL-2 GPL-2+ GPL-3+ ISC LGPL-2.1+ Unicode Vim Vim-Regexp | heavily refactored vim fork (runtime files)                   | 0.6.1-3                         |
| net-tools                       | GPL-2+ | NET-3 networking toolkit                                      | 1.60+git20181103.0eebece-1ubun  |
| netbase                         | GPL-2 | Basic TCP/IP networking system                                | 6.3                             |
| netcat-openbsd                  | BSD-2-Clause BSD-3-Clause | TCP/IP swiss army knife                                       | 1.218-4ubuntu1                  |
| nethogs                         | GPL-2+ | Net top tool grouping bandwidth per process                   | 0.8.6-3                         |
| netpbm                          | BSD GPL | Graphics conversion tools between image formats               | 2:10.0-15.4                     |
| netplan.io                      | GPL-3 | YAML network configuration abstraction for various backends   | 0.104-0ubuntu2.1                |
| network-manager                 | GFDL-NIV-1.1+ GPL-2+ LGPL-2.1+ | network management framework (daemon and userspace tools)     | 1.36.6-0ubuntu2                 |
| network-manager-config-connect  | GFDL-NIV-1.1+ GPL-2+ LGPL-2.1+ | NetworkManager configuration to enable connectivity checking  | 1.36.6-0ubuntu2                 |
| network-manager-gnome           | GPL-2+ LGPL-2+ LGPL-2.1+ | network management framework (GNOME frontend)                 | 1.24.0-1ubuntu3                 |
| network-manager-openvpn         | GPL-2+ LGPL-2+ | network management framework (OpenVPN plugin core)            | 1.8.18-1                        |
| network-manager-openvpn-gnome   | GPL-2+ LGPL-2+ | network management framework (OpenVPN plugin GNOME GUI)       | 1.8.18-1                        |
| network-manager-pptp            | GPL-2+ LGPL-2+ | network management framework (PPTP plugin core)               | 1.2.10-1                        |
| network-manager-pptp-gnome      | GPL-2+ LGPL-2+ | network management framework (PPTP plugin GNOME GUI)          | 1.2.10-1                        |
| networkd-dispatcher             | GPL-3+ | Dispatcher service for systemd-networkd connection status ch  | 2.1-2ubuntu0.22.04.2            |
| nftables                        | CC-BY-SA-4.0 GPL-2 GPL-2+ | Program to control packet filtering rules by Netfilter proje  | 1.0.2-1ubuntu3                  |
| nload                           | GPL-2+ MIT | realtime console network usage monitor                        | 0.7.4-2build3                   |
| ntfs-3g                         | GPL-2+ LGPL-2+ | read/write NTFS driver for FUSE                               | 1:2021.8.22-3ubuntu1.2          |
| openjdk-11-jdk:amd64            | Apache-2.0 GPL-2 LGPL-2 LGPL-2-1 | OpenJDK Development Kit (JDK)                                 | 11.0.16+8-0ubuntu1~22.04        |
| openjdk-11-jdk-headless:amd64   | Apache-2.0 GPL-2 LGPL-2 LGPL-2-1 | OpenJDK Development Kit (JDK) (headless)                      | 11.0.16+8-0ubuntu1~22.04        |
| openjdk-11-jre:amd64            | Apache-2.0 GPL-2 LGPL-2 LGPL-2-1 | OpenJDK Java runtime, using Hotspot JIT                       | 11.0.16+8-0ubuntu1~22.04        |
| openjdk-11-jre-headless:amd64   | Apache-2.0 GPL-2 LGPL-2 LGPL-2-1 | OpenJDK Java runtime, using Hotspot JIT (headless)            | 11.0.16+8-0ubuntu1~22.04        |
| openprinting-ppds               | BSD~unspecified Expat FSFUL GPL-2 GPL-2+ GPL-2.0+OKI | OpenPrinting printer support - PostScript PPD files           | 20220223-0ubuntu1               |
| openssh-client                  | BSD-2-clause BSD-3-clause Expat-with-advertising-restriction Mazieres-BSD-style OpenSSH Powell-BSD-style public-domain | secure shell (SSH) client, for secure access to remote machi  | 1:8.9p1-3                       |
| openssh-server                  | BSD-2-clause BSD-3-clause Expat-with-advertising-restriction Mazieres-BSD-style OpenSSH Powell-BSD-style public-domain | secure shell (SSH) server, for secure access from remote mac  | 1:8.9p1-3                       |
| openssh-sftp-server             | BSD-2-clause BSD-3-clause Expat-with-advertising-restriction Mazieres-BSD-style OpenSSH Powell-BSD-style public-domain | secure shell (SSH) sftp server module, for SFTP access from   | 1:8.9p1-3                       |
| openssl                         | Apache-2.0 Artistic Artistic or GPL-1+ GPL-1+ | Secure Sockets Layer toolkit - cryptographic utility          | 3.0.2-0ubuntu1.7                |
| openvpn                         | BSD-2 BSD-3 GPL-2 GPL-2+ GPL-2 with OpenSSL exception GPL-3+ MIT other | virtual private network daemon                                | 2.5.5-1ubuntu3                  |
| orca                            | LGPL-2.1+ | Scriptable screen reader                                      | 42.0-1ubuntu2                   |
| os-prober                       | GPL-2 | utility to detect other OSes on a set of drives               | 1.79ubuntu2                     |
| p11-kit                         | Apache-2.0 BSD-3-Clause ISC ISC+IBM LGPL-2.1+ permissive-like-automake-output same-as-rest-of-p11kit | p11-glue utilities                                            | 0.24.0-6build1                  |
| p11-kit-modules:amd64           | Apache-2.0 BSD-3-Clause ISC ISC+IBM LGPL-2.1+ permissive-like-automake-output same-as-rest-of-p11kit | p11-glue proxy and trust modules                              | 0.24.0-6build1                  |
| packagekit                      | GPL-2+ LGPL-2.1+ | Provides a package management service                         | 1.2.5-2ubuntu2                  |
| packagekit-tools                | GPL-2+ LGPL-2.1+ | Provides PackageKit command-line tools                        | 1.2.5-2ubuntu2                  |
| pandoc                          | BSD-3-Clause~Glasgow BSD-3-clause~JohnMacFarlane BSD-3-clause~JohnMacFarlane or GPL-2+ CC-BY-SA-3.0 GPL-2 GPL-2+ GPL-3+ ISC MS-RL public-domain WTFPL-2 | general markup converter                                      | 2.9.2.1-3ubuntu2                |
| pandoc-data                     | BSD-3-Clause~Glasgow BSD-3-clause~JohnMacFarlane BSD-3-clause~JohnMacFarlane or GPL-2+ CC-BY-SA-3.0 GPL-2 GPL-2+ GPL-3+ ISC MS-RL public-domain WTFPL-2 | general markup converter - data files                         | 2.9.2.1-3ubuntu2                |
| parted                          | GPL-3 | disk partition manipulator                                    | 3.4-2build1                     |
| passwd                          | GPL-2 | change and administer password and group data                 | 1:4.8.1-2ubuntu2                |
| patch                           | GPL | Apply a diff file to an original                              | 2.7.6-7build2                   |
| pci.ids                         | BSD-3-clause GPL-2+ GPL-2+ or BSD-3-clause | PCI ID Repository                                             | 0.0~2022.01.22-1                |
| pciutils                        | GPL-2+ | PCI utilities                                                 | 1:3.7.0-6                       |
| pcmciautils                     | GPL-2 | PCMCIA utilities for Linux 2.6                                | 018-13build1                    |
| perl                            | Artistic Artistic-2 Artistic-dist Artistic or GPL-1+ or Artistic-dist BSD-3-clause BSD-3-clause-GENERIC BSD-3-clause-with-weird-numbering BSD-4-clause-POWERDOG BZIP CC0-1.0 DONT-CHANGE-THE-GPL Expat Expat or GPL-1+ or Artistic GPL-1+ GPL-1+ or Artistic GPL-1+ or Artistic, GPL-1+ or Artistic or Artistic-dist GPL-2+ GPL-2+ or Artistic GPL-3+-WITH-BISON-EXCEPTION HSIEH-BSD HSIEH-DERIVATIVE LGPL-2.1 REGCOMP REGCOMP, RRA-KEEP-THIS-NOTICE SDBM-PUBLIC-DOMAIN TEXT-TABS Unicode ZLIB | Larry Wall's Practical Extraction and Report Language         | 5.34.0-3ubuntu1.1               |
| perl-base                       | Artistic Artistic-2 Artistic-dist Artistic or GPL-1+ or Artistic-dist BSD-3-clause BSD-3-clause-GENERIC BSD-3-clause-with-weird-numbering BSD-4-clause-POWERDOG BZIP CC0-1.0 DONT-CHANGE-THE-GPL Expat Expat or GPL-1+ or Artistic GPL-1+ GPL-1+ or Artistic GPL-1+ or Artistic, GPL-1+ or Artistic or Artistic-dist GPL-2+ GPL-2+ or Artistic GPL-3+-WITH-BISON-EXCEPTION HSIEH-BSD HSIEH-DERIVATIVE LGPL-2.1 REGCOMP REGCOMP, RRA-KEEP-THIS-NOTICE SDBM-PUBLIC-DOMAIN TEXT-TABS Unicode ZLIB | minimal Perl system                                           | 5.34.0-3ubuntu1.1               |
| perl-modules-5.34               | Artistic Artistic-2 Artistic-dist Artistic or GPL-1+ or Artistic-dist BSD-3-clause BSD-3-clause-GENERIC BSD-3-clause-with-weird-numbering BSD-4-clause-POWERDOG BZIP CC0-1.0 DONT-CHANGE-THE-GPL Expat Expat or GPL-1+ or Artistic GPL-1+ GPL-1+ or Artistic GPL-1+ or Artistic, GPL-1+ or Artistic or Artistic-dist GPL-2+ GPL-2+ or Artistic GPL-3+-WITH-BISON-EXCEPTION HSIEH-BSD HSIEH-DERIVATIVE LGPL-2.1 REGCOMP REGCOMP, RRA-KEEP-THIS-NOTICE SDBM-PUBLIC-DOMAIN TEXT-TABS Unicode ZLIB | Core Perl modules                                             | 5.34.0-3ubuntu1.1               |
| perl-openssl-defaults:amd64     | Artistic Artistic or GPL-1+ GPL-1+ | version compatibility baseline for Perl OpenSSL packages      | 5build2                         |
| pinentry-curses                 | GPL-2 GPL-2+ LGPL-3+ LGPL-3+ or GPL-2+ X11 | curses-based PIN or pass-phrase entry dialog for GnuPG        | 1.1.1-1build2                   |
| pinentry-gnome3                 | GPL-2 GPL-2+ LGPL-3+ LGPL-3+ or GPL-2+ X11 | GNOME 3 PIN or pass-phrase entry dialog for GnuPG             | 1.1.1-1build2                   |
| pipewire:amd64                  | Expat LGPL-2+ LGPL-2.1+ | audio and video processing engine multimedia server           | 0.3.48-1ubuntu2                 |
| pipewire-bin                    | Expat LGPL-2+ LGPL-2.1+ | PipeWire multimedia server - programs                         | 0.3.48-1ubuntu2                 |
| pipewire-media-session          | Expat | example session manager for PipeWire                          | 0.4.1-2ubuntu1                  |
| pkexec                          | Apache-2.0 LGPL-2.0+ | run commands as another user with polkit authorization        | 0.105-33                        |
| plymouth                        | GPL-2+ other | boot animation, logger and I/O multiplexer                    | 0.9.5+git20211018-1ubuntu3      |
| plymouth-label                  | GPL-2+ other | boot animation, logger and I/O multiplexer - label control    | 0.9.5+git20211018-1ubuntu3      |
| plymouth-theme-spinner          | GPL-2+ other | boot animation, logger and I/O multiplexer - spinner theme    | 0.9.5+git20211018-1ubuntu3      |
| plymouth-theme-ubuntu-text      | GPL-2+ other | boot animation, logger and I/O multiplexer - ubuntu text the  | 0.9.5+git20211018-1ubuntu3      |
| policykit-1                     | Apache-2.0 LGPL-2.0+ | transitional package for polkitd and pkexec                   | 0.105-33                        |
| policykit-1-gnome               | LGPL-2.0 | authentication agent for PolicyKit                            | 0.105-7ubuntu3                  |
| policykit-desktop-privileges    | GPL-2+ | run common desktop actions without password                   | 0.21                            |
| polkitd                         | Apache-2.0 LGPL-2.0+ | framework for managing administrative policies and privilege  | 0.105-33                        |
| poppler-data                    | AGPL-3+ BSD-3-cluase GPL-2 MIT | encoding data for the poppler PDF rendering library           | 0.4.11-1                        |
| poppler-utils                   | Apache-2.0 GPL-2 GPL-2 or GPL-3 GPL-3 | PDF utilities (based on Poppler)                              | 22.02.0-2ubuntu0.1              |
| power-profiles-daemon           | GFDL-NIV-1.1+ GPL-2+ GPL-3 | Makes power profiles handling available over D-Bus.           | 0.10.1-3                        |
| powermgmt-base                  | GPL-2+ | common utils for power management                             | 1.36                            |
| ppp                             | GPL-2 | Point-to-Point Protocol (PPP) - daemon                        | 2.4.9-1+1ubuntu3                |
| pptp-linux                      | GPL-2.0+ | Point-to-Point Tunneling Protocol (PPTP) Client               | 1.10.0-1build3                  |
| procps                          | GPL-2.0+ LGPL-2.0+ LGPL-2.1+ | /proc file system utilities                                   | 2:3.3.17-6ubuntu2               |
| psmisc                          | GPL-2+ | utilities that use the proc file system                       | 23.4-2build3                    |
| publicsuffix                    | CC0 MPL-2.0 | accurate, machine-readable list of domain name suffixes       | 20211207.1025-1                 |
| pulseaudio                      | GPL-2+ LGPL-2+ LGPL-2.1+ other | PulseAudio sound server                                       | 1:15.99.1+dfsg1-1ubuntu2        |
| pulseaudio-module-bluetooth     | GPL-2+ LGPL-2+ LGPL-2.1+ other | Bluetooth module for PulseAudio sound server                  | 1:15.99.1+dfsg1-1ubuntu2        |
| pulseaudio-utils                | GPL-2+ LGPL-2+ LGPL-2.1+ other | Command line tools for the PulseAudio sound server            | 1:15.99.1+dfsg1-1ubuntu2        |
| python-apt-common               | GPL-2+ Permissive | Python interface to libapt-pkg (locales)                      | 2.3.0ubuntu2.1                  |
| python-babel-localedata         | BSD-3-clause GPL-2+ Unicode | tools for internationalizing Python applications - locale da  | 2.8.0+dfsg.1-7                  |
| python3                         | GPL-compatible GPL-compatible licenses | interactive high-level object-oriented language (default pyt  | 3.10.6-1~22.04                  |
| python3-apport                  | GPL-2+ | Python 3 library for Apport crash report handling             | 2.20.11-0ubuntu82.1             |
| python3-apt                     | GPL-2+ Permissive | Python 3 interface to libapt-pkg                              | 2.3.0ubuntu2.1                  |
| python3-aptdaemon               | GPL-2 | Python 3 module for the server and client of aptdaemon        | 1.1.1+bzr982-0ubuntu39          |
| python3-aptdaemon.gtk3widgets   | GPL-2 | Python 3 GTK+ 3 widgets to run an aptdaemon client            | 1.1.1+bzr982-0ubuntu39          |
| python3-argcomplete             | Apache-2.0 | bash tab completion for argparse (for Python 3)               | 1.8.1-1.5                       |
| python3-babel                   | BSD-3-clause GPL-2+ Unicode | tools for internationalizing Python applications - Python 3.  | 2.8.0+dfsg.1-7                  |
| python3-blinker                 | BSD-2-clause BSD-3-clause Expat | fast, simple object-to-object and broadcast signaling librar  | 1.4+dfsg1-0.4                   |
| python3-brlapi:amd64            | LGPL-2.1 | Braille display access via BRLTTY - Python3 bindings          | 6.4-4ubuntu3                    |
| python3-cairo:amd64             | LGPL-2.1 | Python3 bindings for the Cairo vector graphics library        | 1.20.1-3build1                  |
| python3-certifi                 | GPL-2 MPL-2 | root certificates for validating SSL certs and verifying TLS  | 2020.6.20-1                     |
| python3-cffi-backend:amd64      | Expat | Foreign Function Interface for Python 3 calling C code - run  | 1.15.0-1build2                  |
| python3-chardet                 | LGPL-2.1+ | universal character encoding detector for Python3             | 4.0.0-1                         |
| python3-commandnotfound         | GPL GPL-3 | Python 3 bindings for command-not-found.                      | 22.04.0                         |
| python3-cryptography            | Apache Expat | Python library exposing cryptographic recipes and primitives  | 3.4.8-1ubuntu2                  |
| python3-cups:amd64              | GPL-2+ | Python3 bindings for CUPS                                     | 2.0.1-5build1                   |
| python3-cupshelpers             | GPL-2+ | Python utility modules around the CUPS printing system        | 1.5.16-0ubuntu3                 |
| python3-dateutil                | BSD-3-clause | powerful extensions to the standard Python 3 datetime module  | 2.8.1-6                         |
| python3-dbus                    | AFL-2.1 Expat GPL-2+ GPL-2+ or AFL-2.1 | simple interprocess messaging system (Python 3 interface)     | 1.2.18-3build1                  |
| python3-debconf                 | BSD-2-clause | interact with debconf from Python 3                           | 1.5.79ubuntu1                   |
| python3-debian                  | GPL-2+ GPL-3+ | Python 3 modules to work with Debian-related data formats     | 0.1.43ubuntu1                   |
| python3-defer                   | GPL-2 | Small framework for asynchronous programming (Python 3)       | 1.0.6-2.1ubuntu1                |
| python3-distro                  | Apache-2.0 | Linux OS platform information API                             | 1.7.0-1                         |
| python3-distutils               | GPL-compatible GPL-compatible licenses | distutils package for Python 3.x                              | 3.10.6-1~22.04                  |
| python3-dnspython               | unknown | DNS toolkit for Python 3                                      | 2.1.0-1ubuntu1                  |
| python3-gdbm:amd64              | GPL-compatible GPL-compatible licenses | GNU dbm database support for Python 3.x                       | 3.10.6-1~22.04                  |
| python3-gi                      | Expat LGPL-2.1+ | Python 3 bindings for gobject-introspection libraries         | 3.42.1-0ubuntu1                 |
| python3-gi-cairo                | Expat LGPL-2.1+ | Python 3 Cairo bindings for the GObject library               | 3.42.1-0ubuntu1                 |
| python3-greenlet                | CC0-1.0-Universal GPL-3 MIT PSFL-2 | Lightweight in-process concurrent programming (python3)       | 1.1.2-3build1                   |
| python3-httplib2                | BSD-3 Expat GPL-2+ GPL-3+ LGPL-2.1+ MPL-1.1 MPL-1.1 or GPL-2+ or LGPL-2.1+ | comprehensive HTTP client library written for Python3         | 0.20.2-2                        |
| python3-ibus-1.0                | GPL-2.0+ with autoconf exception GPL-3.0+ with autoconf exception ISC-Fujitsu ISC-Intel ISC-NCR ISC-Sun LGPL-2.0+ LGPL-2.1+ MIT permissive permissive-author-grant-attribution permissive-autoconf-m4 permissive-fsf-grant permissive-fsf-grant-attribution permissive-makefile-in | Intelligent Input Bus - introspection overrides for Python (  | 1.5.26-4                        |
| python3-idna                    | BSD-3-clause PSF-2 Unicode | Python IDNA2008 (RFC 5891) handling (Python 3)                | 3.3-1                           |
| python3-importlib-metadata      | Apache-2 | library to access the metadata for a Python package - Python  | 4.6.4-1                         |
| python3-jeepney                 | Expat | pure Python D-Bus interface                                   | 0.7.1-3                         |
| python3-jinja2                  | GPL | small but fast and easy to use stand-alone template engine    | 3.0.3-1                         |
| python3-jmespath                | MIT | JSON Matching Expressions (Python 3)                          | 0.10.0-1                        |
| python3-jwt                     | Expat | Python 3 implementation of JSON Web Token                     | 2.3.0-1ubuntu0.2                |
| python3-kerberos                | Apache-2 | GSSAPI interface module - Python 3.x                          | 1.1.14-3.1build5                |
| python3-keyring                 | Expat GPL-3 PSF-2 PSF-2 or Expat | store and access your passwords safely                        | 23.5.0-1                        |
| python3-launchpadlib            | LGPL-3.0 LGPL-3.0+ | Launchpad web services client library (Python 3)              | 1.10.16-1                       |
| python3-lazr.restfulclient      | LGPL-3.0 LGPL-3.0+ | client for lazr.restful-based web services (Python 3)         | 0.14.4-1                        |
| python3-lazr.uri                | LGPL-3.0 | library for parsing, manipulating, and generating URIs        | 1.0.6-2                         |
| python3-ldb                     | BSD-3 GPL-3.0+ ISC LGPL-3.0+ PostgreSQL | Python 3 bindings for LDB                                     | 2:2.4.4-0ubuntu0.1              |
| python3-lib2to3                 | GPL-compatible GPL-compatible licenses | Interactive high-level object-oriented language (lib2to3)     | 3.10.6-1~22.04                  |
| python3-libcloud                | Apache-2.0 | unified Python interface into the cloud (Python3 version)     | 3.2.0-2                         |
| python3-lockfile                | pat GPL-3+ | file locking library for Python — Python 3 library         Ex | 1:0.12.2-2.2                    |
| python3-louis                   | GPL LGPL | Python bindings for liblouis                                  | 3.20.0-2ubuntu0.1               |
| python3-macaroonbakery          | LGPL-3 | Higher-level macaroon operations for Python 3                 | 1.3.1-2                         |
| python3-markupsafe              | unknown | HTML/XHTML/XML string library                                 | 2.0.1-2build1                   |
| python3-minimal                 | GPL-compatible GPL-compatible licenses | minimal subset of the Python language (default python3 versi  | 3.10.6-1~22.04                  |
| python3-more-itertools          | MIT-style | library with routines for operating on iterables, beyond ite  | 8.10.0-2                        |
| python3-msgpack                 | Apache-2.0 GPL-2+ | Python 3 implementation of MessagePack format                 | 1.0.3-1build1                   |
| python3-nacl                    | Apache-2.0 Expat | Python bindings to libsodium (Python 3)                       | 1.5.0-2                         |
| python3-neovim                  | Apache-2.0 | transitional dummy package                                    | 0.4.2-1                         |
| python3-netaddr                 | BSD license | manipulation of various common network address notations (Py  | 0.8.0-2                         |
| python3-netifaces:amd64         | MIT-style | portable network interface information - Python 3.x           | 0.11.0-1build2                  |
| python3-ntlm-auth               | Expat | NTLM low-level Python library                                 | 1.4.0-1                         |
| python3-oauthlib                | BSD-3-clause | generic, spec-compliant implementation of OAuth for Python3   | 3.2.0-1ubuntu0.1                |
| python3-olefile                 | BSD-3-clause | Python module to read/write MS OLE2 files                     | 0.46-3                          |
| python3-packaging               | Apache-2.0 BSD-3-clause | core utilities for python3 packages                           | 21.3-1                          |
| python3-pexpect                 | ISC | Python 3 module for automating interactive applications       | 4.8.0-2ubuntu1                  |
| python3-pil:amd64               | Public domain | Python Imaging Library (Python3)                              | 9.0.1-1build1                   |
| python3-pkg-resources           | Apache-2.0 | Package Discovery and Resource Access using pkg_resources     | 59.6.0-1.2                      |
| python3-problem-report          | GPL-2+ | Python 3 library to handle problem reports                    | 2.20.11-0ubuntu82.1             |
| python3-protobuf                | Apache-2.0 BSD-3-Clause~Google Expat GPL-3 GPLWithACException Public-Domain Public-Domain or Expat | Python 3 bindings for protocol buffers                        | 3.12.4-1ubuntu7                 |
| python3-ptyprocess              | ISC | Run a subprocess in a pseudo terminal from Python 3           | 0.7.0-3                         |
| python3-pyatspi                 | GPL-2 GPL-2+ LGPL-2 LGPL-2+ LGPL-2.1+ PSF | Assistive Technology Service Provider Interface - Python3 bi  | 2.38.2-1                        |
| python3-pycryptodome            | BSD-2-clause public-domain | cryptographic Python library (Python 3)                       | 3.11.0+dfsg1-3build1            |
| python3-pymacaroons             | Expat | Macaroon library for Python 3                                 | 0.13.0-4                        |
| python3-pynvim                  | Apache-2.0 | Python3 library for scripting Neovim processes through its m  | 0.4.2-1                         |
| python3-pyparsing               | BSD-2-clause BSD-3-clause ellis-and-grant Expat GPL-2 GPL-3 salvolainen | alternative to creating and executing simple grammars - Pyth  | 2.4.7-1                         |
| python3-renderpm:amd64          | GPL LGPL | python low level render interface                             | 3.6.8-1                         |
| python3-reportlab               | GPL LGPL | ReportLab library to create PDF documents using Python3       | 3.6.8-1                         |
| python3-reportlab-accel:amd64   | GPL LGPL | C coded extension accelerator for the ReportLab Toolkit       | 3.6.8-1                         |
| python3-requests                | Apache other | elegant and simple HTTP library for Python3, built for human  | 2.25.1+dfsg-2                   |
| python3-requests-kerberos       | BSD-style | Kerberos/GSSAPI authentication handler for python-requests -  | 0.12.0-2                        |
| python3-requests-ntlm           | ISC | Adds support for NTLM authentication to the requests library  | 1.1.0-1.1                       |
| python3-requests-toolbelt       | Apache-2.0 | Utility belt for advanced users of python3-requests           | 0.9.1-1                         |
| python3-rfc3339                 | MIT | parser and generator of RFC 3339-compliant timestamps (Pytho  | 1.1-3                           |
| python3-secretstorage           | BSD-3-clause | Python module for storing secrets - Python 3.x version        | 3.3.1-1                         |
| python3-selinux                 | GPL-2 LGPL-2.1 | Python3 bindings to SELinux shared libraries                  | 3.3-1build2                     |
| python3-simplejson              | MIT | simple, fast, extensible JSON encoder/decoder for Python 3.x  | 3.17.6-1build1                  |
| python3-six                     | Expat | Python 2 and 3 compatibility library (Python 3 interface)     | 1.16.0-3ubuntu1                 |
| python3-software-properties     | GPL-2+ GPL-3 LGPL-3 | manage the repositories that you install software from        | 0.99.22.3                       |
| python3-speechd                 | GFDL-NIV-1.2+ GFDL-NIV-1.2+ or GPL-2+ GPL-2+ GPL-3+ with tex exception LGPL-2.1+ other public-domain | Python interface to Speech Dispatcher                         | 0.11.1-1ubuntu2                 |
| python3-systemd                 | LGPL-2.1+ | Python 3 bindings for systemd                                 | 234-3ubuntu2                    |
| python3-talloc:amd64            | BSD-3 GPL-3.0+ ISC LGPL-3.0+ PostgreSQL | hierarchical pool based memory allocator - Python3 bindings   | 2.3.3-2build1                   |
| python3-tz                      | Expat | Python3 version of the Olson timezone database                | 2022.1-1                        |
| python3-urllib3                 | Expat PSF-2 | HTTP library with thread-safe connection pooling for Python3  | 1.26.5-1~exp1                   |
| python3-wadllib                 | LGPL-3.0 | Python 3 library for navigating WADL files                    | 1.3.6-1                         |
| python3-winrm                   | MIT | Python 3 library for Windows Remote Management                | 0.3.0-2                         |
| python3-xdg                     | LGPL-2 | Python 3 library to access freedesktop.org standards          | 0.27-2                          |
| python3-xmltodict               | MIT | Makes working with XML feel like you are working with JSON (  | 0.12.0-2                        |
| python3-yaml                    | unknown | YAML parser and emitter for Python3                           | 5.4.1-1ubuntu1                  |
| python3-zipp                    | Expat | pathlib-compatible Zipfile object wrapper - Python 3.x        | 1.0.0-3                         |
| python3.10                      | GPL-2 | Interactive high-level object-oriented language (version 3.1  | 3.10.6-1~22.04                  |
| python3.10-minimal              | GPL-2 | Minimal subset of the Python language (version 3.10)          | 3.10.6-1~22.04                  |
| readline-common                 | GFDL GPL-3 | GNU readline and history libraries, common files              | 8.1.2-1                         |
| rfkill                          | BSD-2-clause BSD-3-clause BSD-4-clause GPL-2 GPL-2+ GPL-3+ LGPL LGPL-2+ LGPL-2.1+ LGPL-3+ MIT public-domain | tool for enabling and disabling wireless devices              | 2.37.2-4ubuntu3                 |
| rpcsvc-proto                    | BSD-3-clause GPL-2+-autoconf-exception GPL-3+-autoconf-exception MIT permissive-autoconf-m4 permissive-autoconf-m4-no-warranty permissive-configure permissive-fsf permissive-makefile-in | RPC protocol compiler and definitions                         | 1.4.2-0ubuntu6                  |
| rsync                           | GPL-3 | fast, versatile, remote (and local) file-copying tool         | 3.2.3-8ubuntu3                  |
| rsyslog                         | Apache-2.0 BSD-3-clause GPL-3.0+ LGPL-3.0+ | reliable system and kernel logging daemon                     | 8.2112.0-2ubuntu2.2             |
| rtkit                           | Expat GPL-3+ | Realtime Policy and Watchdog Daemon                           | 0.13-4build2                    |
| rygel                           | CC-BY-SA-3.0 LGPL-2+ | GNOME UPnP/DLNA services                                      | 0.40.3-1ubuntu2                 |
| samba-libs:amd64                | BSD-3 GPL-3 GPL-3.0+ ISC LGPL-3.0+ MS-ADSL PostgreSQL | Samba core libraries                                          | 2:4.15.9+dfsg-0ubuntu0.2        |
| sane-airscan                    | Expat GPL-2+ | SANE backend for AirScan (eSCL) and WSD document scanner      | 0.99.27-1build1                 |
| sane-utils                      | Artistic GFDL-1.1 GPL-2 GPL-2+ GPL-2+ with sane exception GPL-3+ LGPL-2.1+ | API library for scanners -- utilities                         | 1.1.1-5                         |
| sbsigntool                      | CC0 GPL-3+ GPL-3+ with OpenSSL exception LGPL-2+ LGPL-2.1+ LGPL-3+ MIT | Tools to manipulate signatures on UEFI binaries and drivers   | 0.9.4-2ubuntu2                  |
| screen                          | GPL-3+ | terminal multiplexer with VT100/ANSI terminal emulation       | 4.9.0-1                         |
| seahorse                        | GFDL-1.1 GPL-2 | GNOME front end for GnuPG                                     | 41.0-2                          |
| secureboot-db                   | GPL-3+ | Secure Boot updates for DB and DBX                            | 1.8                             |
| sed                             | GPL-3 | GNU stream editor for filtering/transforming text             | 4.8-1ubuntu2                    |
| sensible-utils                  | All-permissive configure GPL-2+ installsh | Utilities for sensible alternative selection                  | 0.0.17                          |
| session-migration               | LGPL-3+ | Tool to migrate in user session settings                      | 0.3.6                           |
| sgml-base                       | GPL-2+ | SGML infrastructure and SGML catalog file support             | 1.30                            |
| sgml-data                       | GPL-2 | common SGML and XML data                                      | 2.0.11+nmu1                     |
| shared-mime-info                | GPL | FreeDesktop.org shared MIME database and spec                 | 2.1-2                           |
| shim-signed                     | BSD-2-Clause | Secure Boot chain-loading bootloader (Microsoft-signed binar  | 1.51+15.4-0ubuntu9              |
| sl                              | unknown | Correct you if you type `sl' by mistake                       | 5.02-1                          |
| software-properties-common      | GPL-2+ GPL-3 LGPL-3 | manage the repositories that you install software from (comm  | 0.99.22.3                       |
| sound-icons                     | GPL-2+ | Sounds for speech enabled applications                        | 0.1-8                           |
| sound-theme-freedesktop         | CC-BY-3.0 CC-BY-SA-3.0 GPL-2 GPL-2+ | freedesktop.org sound theme                                   | 0.8-2ubuntu1                    |
| speech-dispatcher               | GFDL-NIV-1.2+ GFDL-NIV-1.2+ or GPL-2+ GPL-2+ GPL-3+ with tex exception LGPL-2.1+ other public-domain | Common interface to speech synthesizers                       | 0.11.1-1ubuntu2                 |
| speech-dispatcher-audio-plugin  | GFDL-NIV-1.2+ GFDL-NIV-1.2+ or GPL-2+ GPL-2+ GPL-3+ with tex exception LGPL-2.1+ other public-domain | Speech Dispatcher: Audio output plugins                       | 0.11.1-1ubuntu2                 |
| speech-dispatcher-espeak-ng     | GFDL-NIV-1.2+ GFDL-NIV-1.2+ or GPL-2+ GPL-2+ GPL-3+ with tex exception LGPL-2.1+ other public-domain | Speech Dispatcher: Espeak-ng output module                    | 0.11.1-1ubuntu2                 |
| speedtest-cli                   | Apache-2.0 Expat | Command line interface for testing internet bandwidth using   | 2.1.3-2                         |
| spice-vdagent                   | GPL-3+ | Spice agent for Linux                                         | 0.22.1-1                        |
| squashfs-tools                  | GPL-2+ | Tool to create and append to squashfs filesystems             | 1:4.5-3build1                   |
| ssh-import-id                   | GPL-3 | securely retrieve an SSH public key and install it locally    | 5.11-0ubuntu1                   |
| sshfs                           | GPL-2 GPL-2+ | filesystem client based on SSH File Transfer Protocol         | 3.7.1+repack-2                  |
| ssl-cert                        | BSD-3-clause | simple debconf wrapper for OpenSSL                            | 1.1.2                           |
| strace                          | unknown | System call tracer                                            | 5.16-0ubuntu3                   |
| suckless-tools                  | Expat public-domain | simple commands for minimalistic window managers              | 46-1                            |
| sudo                            | BSD-2-Clause BSD-3-Clause ISC other Zlib | Provide limited super user privileges to specific users       | 1.9.9-1ubuntu2.1                |
| switcheroo-control              | GPL-3+ | D-Bus service to check the availability of dual-GPU           | 2.4-3build2                     |
| system-config-printer           | GPL-2+ | graphical interface to configure the printing system          | 1.5.16-0ubuntu3                 |
| system-config-printer-common    | GPL-2+ | backend and the translation files for system-config-printer   | 1.5.16-0ubuntu3                 |
| system-config-printer-udev      | GPL-2+ | Utilities to detect and configure printers automatically      | 1.5.16-0ubuntu3                 |
| systemd                         | CC0-1.0 Expat GPL-2 GPL-2+ GPL-2 with Linux-syscall-note exception LGPL-2.1+ public-domain | system and service manager                                    | 249.11-0ubuntu3.6               |
| systemd-hwe-hwdb                | GPL-2.0+ | udev rules for hardware enablement (HWE)                      | 249.11.1                        |
| systemd-oomd                    | CC0-1.0 Expat GPL-2 GPL-2+ GPL-2 with Linux-syscall-note exception LGPL-2.1+ public-domain | Userspace out-of-memory (OOM) killer                          | 249.11-0ubuntu3.6               |
| systemd-sysv                    | CC0-1.0 Expat GPL-2 GPL-2+ GPL-2 with Linux-syscall-note exception LGPL-2.1+ public-domain | system and service manager - SysV links                       | 249.11-0ubuntu3.6               |
| systemd-timesyncd               | CC0-1.0 Expat GPL-2 GPL-2+ GPL-2 with Linux-syscall-note exception LGPL-2.1+ public-domain | minimalistic service to synchronize local time with NTP serv  | 249.11-0ubuntu3.6               |
| sysvinit-utils                  | GPL-2+ | System-V-like utilities                                       | 3.01-1ubuntu1                   |
| tar                             | GPL-2 GPL-3 | GNU version of the tar archiving utility                      | 1.34+dfsg-1build3               |
| tcpdump                         | 4-clause BSD license BSD license | command-line network traffic analyzer                         | 4.99.1-3build2                  |
| telnet                          | BSD-4-clause | basic telnet client                                           | 0.17-44build1                   |
| time                            | freely-redistributable GPL-2+ GPL-3+ | GNU time program for measuring CPU resource usage             | 1.9-0.1build2                   |
| tmux                            | BSD-2 BSD-2. BSD-3 bsd-poll.c bsd-poll.h | terminal multiplexer                                          | 3.2a-4build1                    |
| tnftp                           | BSD-2-clause BSD-2-Clause BSD-3-clause BSD-3-Clause BSD-4-Clause Expat FSFAP FSFULLR GPL-2+ GPL-2+ with Autoconf-data exception GPL-2+ with libtool exception GPL-3+ with Autoconf-data exception ISC Unlicense | enhanced ftp client                                           | 20210827-4build1                |
| toilet                          | unknown | display large colourful characters in text mode               | 0.3-1.4                         |
| toilet-fonts                    | unknown | collection of TOIlet fonts                                    | 0.3-1.4                         |
| tpm-udev                        | BSD-2-clause | udev rules for TPM modules                                    | 0.6                             |
| tracker                         | GPL-2.0+ LGPL-2.0+ LGPL-2.1+ | metadata database, indexer and search tool                    | 3.3.0-1                         |
| tracker-extract                 | Expat GPL-2+ LGPL-2+ LGPL-2.1+ | metadata database, indexer and search tool - metadata extrac  | 3.3.0-1                         |
| tracker-miner-fs                | Expat GPL-2+ LGPL-2+ LGPL-2.1+ | metadata database, indexer and search tool - filesystem inde  | 3.3.0-1                         |
| tree                            | GPL GPL-2 | displays an indented directory tree, in color                 | 2.0.2-1                         |
| tzdata                          | public domain | time zone and daylight-saving time data                       | 2022e-0ubuntu0.22.04.0          |
| ubuntu-advantage-desktop-daemo  | GPL-3.0 | Daemon to allow access to ubuntu-advantage via D-Bus          | 1.9~22.04.1                     |
| ubuntu-advantage-tools          | GPL-3.0 | management tools for Ubuntu Advantage                         | 27.11.2~22.04.1                 |
| ubuntu-docs                     | CC-BY-SA-3.0 GPL-3+ | Ubuntu Desktop Guide                                          | 22.04.5                         |
| ubuntu-keyring                  | GPL | GnuPG keys of the Ubuntu archive                              | 2021.03.26                      |
| ubuntu-minimal                  | GPL-2 | Minimal core of Ubuntu                                        | 1.481                           |
| ubuntu-mono                     | CC-BY-SA-3.0 GPL-3 | Ubuntu Mono Icon theme                                        | 20.10-0ubuntu2                  |
| ubuntu-report                   | Apache BSD-2.0-clause BSD-3.0-clause Expat GPL-3 | Report hardware and other collected metrics                   | 1.7.1                           |
| ubuntu-session                  | GPL-2 | Ubuntu session with GNOME Shell                               | 42.0-1ubuntu2                   |
| ubuntu-settings                 | GPL-2+ | default settings for the Ubuntu desktop                       | 22.04.6                         |
| ubuntu-wallpapers               | CC0 CC-BY-2.0 CC-BY-3.0 CC-BY-SA-2.0 CC-BY-SA-3.0 CC-BY-SA-4.0 Unsplash | Ubuntu Wallpapers                                             | 22.04.4-0ubuntu1                |
| ubuntu-wallpapers-jammy         | CC0 CC-BY-2.0 CC-BY-3.0 CC-BY-SA-2.0 CC-BY-SA-3.0 CC-BY-SA-4.0 Unsplash | Ubuntu 22.04 Wallpapers                                       | 22.04.4-0ubuntu1                |
| ucf                             | GPL-2 | Update Configuration File(s): preserve user changes to confi  | 3.0043                          |
| udev                            | CC0-1.0 Expat GPL-2 GPL-2+ GPL-2 with Linux-syscall-note exception LGPL-2.1+ public-domain | /dev/ and hotplug management daemon                           | 249.11-0ubuntu3.6               |
| udisks2                         | GPL-2+ LGPL-2+ | D-Bus service to access and manipulate storage devices        | 2.9.4-1ubuntu2                  |
| ufw                             | BSD-3-clause GPL-3 | program for managing a Netfilter firewall                     | 0.36.1-4build1                  |
| unrar-free                      | GPL-2+ Info-ZIP | Unarchiver for .rar files                                     | 1:0.0.2-0.1                     |
| unzip                           | unknown | De-archiver for .zip files                                    | 6.0-26ubuntu3.1                 |
| update-inetd                    | GPL-2+ | inetd configuration file updater                              | 4.51                            |
| upower                          | GFDL-1.1+ GPL-2+ | abstraction for power management                              | 0.99.17-1                       |
| usb.ids                         | GPL-2 | USB ID Repository                                             | 2022.04.02-1                    |
| usrmerge                        | GPL-2 | Convert the system to the merged /usr directories scheme      | 25ubuntu2                       |
| util-linux                      | BSD-2-clause BSD-3-clause BSD-4-clause GPL-2 GPL-2+ GPL-3+ LGPL LGPL-2+ LGPL-2.1+ LGPL-3+ MIT public-domain | miscellaneous system utilities                                | 2.37.2-4ubuntu3                 |
| uuid-runtime                    | BSD-2-clause BSD-3-clause BSD-4-clause GPL-2 GPL-2+ GPL-3+ LGPL LGPL-2+ LGPL-2.1+ LGPL-3+ MIT public-domain | runtime components for the Universally Unique ID library      | 2.37.2-4ubuntu3                 |
| vim                             | Apache Apache or Expat Artistic-1 BSD-2-clause BSD-3-clause Compaq EDL-1 Expat Expat or GPL-2 Expat or Vim GPL-1+ GPL-1+ or Artistic-1 GPL-2 GPL-2+ GPL-3+ LGPL-2.1+ OPL-1+ public-domain SRA UC Vim Vim-Regexp X11 XPM | Vi IMproved - enhanced vi editor                              | 2:8.2.3995-1ubuntu2.1           |
| vim-common                      | Apache Apache or Expat Artistic-1 BSD-2-clause BSD-3-clause Compaq EDL-1 Expat Expat or GPL-2 Expat or Vim GPL-1+ GPL-1+ or Artistic-1 GPL-2 GPL-2+ GPL-3+ LGPL-2.1+ OPL-1+ public-domain SRA UC Vim Vim-Regexp X11 XPM | Vi IMproved - Common files                                    | 2:8.2.3995-1ubuntu2.1           |
| vim-runtime                     | Apache Apache or Expat Artistic-1 BSD-2-clause BSD-3-clause Compaq EDL-1 Expat Expat or GPL-2 Expat or Vim GPL-1+ GPL-1+ or Artistic-1 GPL-2 GPL-2+ GPL-3+ LGPL-2.1+ OPL-1+ public-domain SRA UC Vim Vim-Regexp X11 XPM | Vi IMproved - Runtime files                                   | 2:8.2.3995-1ubuntu2.1           |
| vim-tiny                        | Apache Apache or Expat Artistic-1 BSD-2-clause BSD-3-clause Compaq EDL-1 Expat Expat or GPL-2 Expat or Vim GPL-1+ GPL-1+ or Artistic-1 GPL-2 GPL-2+ GPL-3+ LGPL-2.1+ OPL-1+ public-domain SRA UC Vim Vim-Regexp X11 XPM | Vi IMproved - enhanced vi editor - compact version            | 2:8.2.3995-1ubuntu2.1           |
| w3m                             | copyleft-matrix-c GPL-3+ with Autoconf exception permissive-aclocal permissive-configure permissive-debian permissive-gitlog2changelog permissive-matrix-h permissive-po permissive-Str public-domain Ruby Unicode-Data-Files w3m X11 X11-install-sh | WWW browsable pager with excellent tables/frames support      | 0.5.3+git20210102-6build3       |
| w3m-img                         | copyleft-matrix-c GPL-3+ with Autoconf exception permissive-aclocal permissive-configure permissive-debian permissive-gitlog2changelog permissive-matrix-h permissive-po permissive-Str public-domain Ruby Unicode-Data-Files w3m X11 X11-install-sh | inline image extension support utilities for w3m              | 0.5.3+git20210102-6build3       |
| wamerican                       | public domain Public Domain | American English dictionary words for /usr/share/dict         | 2020.12.07-2                    |
| wbritish                        | public domain Public Domain | British English dictionary words for /usr/share/dict          | 2020.12.07-2                    |
| wget                            | GFDL-1.2 GPL-3 | retrieves files from the web                                  | 1.21.2-2ubuntu1                 |
| whiptail                        | LGPL-2 | Displays user-friendly dialog boxes from shell scripts        | 0.52.21-5ubuntu2                |
| whoopsie                        | Apache-2.0 GPL-3 | Ubuntu error tracker submission                               | 0.2.77                          |
| whoopsie-preferences            | GPL-3 | System preferences for error reporting                        | 23                              |
| wireless-regdb                  | ISC | wireless regulatory database                                  | 2022.06.06-0ubuntu1~22.04.1     |
| wpasupplicant                   | BSD-3-clause BSD-3-clause or GPL-2 GPL-2 ISC public-domain | client support for WPA and WPA2 (IEEE 802.11i)                | 2:2.10-6ubuntu2                 |
| x11-apps                        | unknown | X applications                                                | 7.7+8build2                     |
| x11-common                      | GPL | X Window System (X.Org) infrastructure                        | 1:7.7+23ubuntu2                 |
| x11-session-utils               | unknown | X session utilities                                           | 7.7+4build2                     |
| x11-utils                       | unknown | X11 utilities                                                 | 7.7+5build2                     |
| x11-xkb-utils                   | unknown | X11 XKB utilities                                             | 7.7+5build4                     |
| x11-xserver-utils               | unknown | X server utilities                                            | 7.7+9build1                     |
| x11proto-dev                    | MIT SGI | X11 extension protocols and auxiliary headers                 | 2021.5-1                        |
| xauth                           | unknown | X authentication utility                                      | 1:1.1-1build2                   |
| xbitmaps                        | unknown | Base X bitmaps                                                | 1.1.1-2.1ubuntu1                |
| xbrlapi                         | LGPL-2.1 | Access software for a blind person using a braille display -  | 6.4-4ubuntu3                    |
| xclip                           | GPL-2.0+ | command line interface to X selections                        | 0.13-2                          |
| xcursor-themes                  | unknown | Base X cursor themes                                          | 1.0.6-0ubuntu1                  |
| xcvt                            | MIT NTP | VESA CVT standard timing modelines generator                  | 0.1.1-3                         |
| xdg-dbus-proxy                  | LGPL-2+ LGPL-2.1+ | filtering D-Bus proxy                                         | 0.1.3-1                         |
| xdg-desktop-portal              | LGPL-2+ LGPL-2.1+ | desktop integration portal for Flatpak and Snap               | 1.14.4-1ubuntu2~22.04.1         |
| xdg-desktop-portal-gnome        | GPL-2+ LGPL-2+ LGPL-2.1+ | GNOME portal backend for xdg-desktop-portal                   | 42.1-0ubuntu1                   |
| xdg-desktop-portal-gtk          | GPL-2+ LGPL-2+ | GTK+/GNOME portal backend for xdg-desktop-portal              | 1.14.0-1build1                  |
| xdg-user-dirs                   | GPL-2 | tool to manage well known user directories                    | 0.17-2ubuntu4                   |
| xdg-user-dirs-gtk               | GPL | tool to manage well known user directories (Gtk extension)    | 0.10-3build2                    |
| xdg-utils                       | Expat | desktop integration utilities from freedesktop.org            | 1.1.3-4.1ubuntu3~22.04.1        |
| xfonts-base                     | Public domain | standard fonts for X                                          | 1:1.0.5                         |
| xfonts-encodings                | public domain | Encodings for X.Org fonts                                     | 1:1.0.5-0ubuntu2                |
| xfonts-scalable                 | unknown | scalable fonts for X                                          | 1:1.0.3-1.2ubuntu1              |
| xfonts-utils                    | unknown | X Window System font utility programs                         | 1:7.7+6build2                   |
| xinit                           | unknown | X server initialisation tool                                  | 1.4.1-0ubuntu4                  |
| xinput                          | unknown | Runtime configuration and test of XInput devices              | 1.6.3-1build2                   |
| xkb-data                        | unknown | X Keyboard Extension (XKB) configuration data                 | 2.33-1                          |
| xml-core                        | GPL-2+ | XML infrastructure and XML catalog file support               | 0.18+nmu1                       |
| xorg                            | GPL | X.Org X Window System                                         | 1:7.7+23ubuntu2                 |
| xorg-docs-core                  | MIT license MIT License | Core documentation for the X.org X Window System              | 1:1.7.1-1.2                     |
| xorg-sgml-doctools              | unknown | Common tools for building X.Org SGML documentation            | 1:1.11-1.1                      |
| xsel                            | X11 | command-line tool to access X clipboard and selection buffer  | 1.2.0+git9bfc13d.20180109-3     |
| xserver-common                  | BSD-like MIT license | common files used by various X servers                        | 2:21.1.3-2ubuntu2.2             |
| xserver-xephyr                  | BSD-like MIT license | nested X server                                               | 2:21.1.3-2ubuntu2.2             |
| xserver-xorg                    | GPL | X.Org X server                                                | 1:7.7+23ubuntu2                 |
| xserver-xorg-core               | BSD-like MIT license | Xorg X server - core server                                   | 2:21.1.3-2ubuntu2.2             |
| xserver-xorg-input-all          | GPL | X.Org X server -- input driver metapackage                    | 1:7.7+23ubuntu2                 |
| xserver-xorg-input-libinput     | MIT xorg | X.Org X server -- libinput input driver                       | 1.2.1-1                         |
| xserver-xorg-input-wacom        | unknown | X.Org X server -- Wacom input driver                          | 1:1.0.0-3ubuntu1                |
| xserver-xorg-legacy             | BSD-like MIT license | setuid root Xorg server wrapper                               | 2:21.1.3-2ubuntu2.2             |
| xtrans-dev                      | unknown | X transport library (development files)                       | 1.4.0-1                         |
| xul-ext-ubufox                  | GPL-2 | Ubuntu modifications for Firefox                              | 3.4-0ubuntu1.17.10.1            |
| xwayland                        | BSD-like MIT license | X server for running X clients under Wayland                  | 2:22.1.1-1ubuntu0.2             |
| xxd                             | Apache Apache or Expat Artistic-1 BSD-2-clause BSD-3-clause Compaq EDL-1 Expat Expat or GPL-2 Expat or Vim GPL-1+ GPL-1+ or Artistic-1 GPL-2 GPL-2+ GPL-3+ LGPL-2.1+ OPL-1+ public-domain SRA UC Vim Vim-Regexp X11 XPM | tool to make (or reverse) a hex dump                          | 2:8.2.3995-1ubuntu2.1           |
| xz-utils                        | Autoconf config-h GPL-2 GPL-2+ LGPL-2.1+ noderivs none PD PD-debian permissive-fsf permissive-nowarranty probably-PD | XZ-format compression utilities                               | 5.2.5-2ubuntu1                  |
| yaru-theme-gnome-shell          | CC-BY-SA GPL-3.0+ LGPL-2.1 LGPL-3.0 | Yaru GNOME Shell desktop theme from the Ubuntu Community      | 22.04.4                         |
| yaru-theme-gtk                  | CC-BY-SA GPL-3.0+ LGPL-2.1 LGPL-3.0 | Yaru GTK theme from the Ubuntu Community                      | 22.04.4                         |
| yaru-theme-icon                 | CC-BY-SA GPL-3.0+ LGPL-2.1 LGPL-3.0 | Yaru icon theme from the Ubuntu Community                     | 22.04.4                         |
| yaru-theme-sound                | CC-BY-SA GPL-3.0+ LGPL-2.1 LGPL-3.0 | Yaru sound theme from the Ubuntu Community                    | 22.04.4                         |
| yelp                            | Apache-2.0 GPL-2+ | Help browser for GNOME                                        | 42.1-1                          |
| yelp-xsl                        | BSD-3-clause GPL-2+ LGPL-2+ | XSL stylesheets for the yelp help browser                     | 42.0-1                          |
| zenity                          | LGPL-2 | Display graphical dialog boxes from shell scripts             | 3.42.1-0ubuntu1                 |
| zenity-common                   | LGPL-2 | Display graphical dialog boxes from shell scripts (common fi  | 3.42.1-0ubuntu1                 |
| zip                             | unknown | Archiver for .zip files                                       | 3.0-12build2                    |
| zlib1g:amd64                    | Zlib | compression library - runtime                                 | 1:1.2.11.dfsg-2ubuntu9.2        |
| zsh                             | Artistic Artistic or GPL-1+ or Zsh BSD-3 Expat GPL-1+ GPL-2 GPL-2+ PWS-Zsh-FAQ Zsh | shell with lots of features                                   | 5.8.1-1                         |
| zsh-common                      | Artistic Artistic or GPL-1+ or Zsh BSD-3 Expat GPL-1+ GPL-2 GPL-2+ PWS-Zsh-FAQ Zsh | architecture independent files for Zsh                        | 5.8.1-1                         |
| zstd                            | BSD-3-clause Expat GPL-2 zlib | fast lossless compression algorithm -- CLI tool               | 1.4.8+dfsg-3build1              |

