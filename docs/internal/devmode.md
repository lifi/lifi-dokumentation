# Entwicklungsmodus

Um Lifi in den Entwicklungsmodus umzustellen muss man in der VM die folgenden Befehle ausführen:

```bash
$ cd /opt/lifi && sudo git switch dev  # In den Entwicklungsbranch wechseln (andere Branches sind auch möglich)
$ sudo touch /opt/lifi/.dev            # Entwicklungsmodus aktivieren
$ sudo touch /opt/lifi/.dev-update     # Automatisches 'git pull' im Entwicklungsmodus reaktivieren
$ update                               # Lifi updaten
```

Im Entwicklungsmodus verhält sich der `update` Befehl wie folgt anders:

1. `/opt/lifi` wird nicht automatisch zurückgesetzt.
1. In `/opt/lifi` wird nicht automatisch das Repo aktualisiert, außer `/opt/lifi/.dev-update` existiert.
