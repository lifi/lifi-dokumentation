# Interne Dokumentation für Lifi

!!! info
    Der interne Teil der Dokumentation ist für die Entwicklung, Instandhaltung und generelle Dokumentation von Lifi.

    Außerdem sind hier Informationen für Dozenten und Admins, welche Lifi für Prüfungen im E-Prüfungsraum verwenden wollen.

- [Entwicklungsmodus](devmode.md)
