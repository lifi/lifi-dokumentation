# Lifi Dokumentation

https://lifi.pages.gwdg.de/lifi-dokumentation/

Gebaut mit [MkDocs](https://www.mkdocs.org/).

## Lokal entwickeln

Abhängigkeiten installieren: `pip install -r requirements.txt`

Automatisch neubauen und hosten: `mkdocs serve`

## Logo & Images

- https://www.reshot.com/free-svg-icons/item/three-leaves-KD6UVGSNFP/
- https://www.reshot.com/free-svg-icons/item/bug-SV8H943YWA/


## Markdown Syntax Hinweise

- https://squidfunk.github.io/mkdocs-material/reference/admonitions/
- https://squidfunk.github.io/mkdocs-material/reference/admonitions/#supported-types
